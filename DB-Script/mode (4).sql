-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2020 at 04:01 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mode`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(155) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `text_password`) VALUES
(1, 'Admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `foreignName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `oldMobile` bigint(20) NOT NULL,
  `customerCode` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `fcm_token` text,
  `deviceId` text NOT NULL,
  `otp` int(11) NOT NULL,
  `valid_token` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `foreignName`, `mobile`, `oldMobile`, `customerCode`, `status`, `fcm_token`, `deviceId`, `otp`, `valid_token`, `created_at`, `modified_at`) VALUES
(1, 'Ali Faisal AL Bugami', 'علي فيصل ال', 9474565257, 9434620796, 'IX12-000185', 'Active', NULL, '', 544329, '', '2020-07-17 14:57:17', '2020-07-17 14:57:17'),
(10, 'Ali Faisal AL', 'علي فيصل ال', 8389023404, 8389023404, 'IX12-000186', 'Active', NULL, 'abcdefgh11122345', 646602, 'MiAlQ0bU27avVOpr', '2020-07-21 18:26:13', '2020-07-21 18:26:13');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_men`
--

CREATE TABLE `delivery_men` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_code` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_men`
--

INSERT INTO `delivery_men` (`id`, `name`, `mobile`, `password`, `text_password`, `email`, `user_code`, `designation`, `status`, `created_at`, `modified_at`) VALUES
(1, 'Shantanu Mandal', 9874533333, '827ccb0eea8a706c4c34a16891f84e7b', '12345', 'swagat@kazmatechnology.com', 'dl05', 'deliveryman', 'Active', '0000-00-00 00:00:00', '2020-07-24 11:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `employee_code` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `mobile`, `password`, `text_password`, `email`, `employee_code`, `designation`, `status`, `created_at`, `modified_at`) VALUES
(4, 'Shantanu Mandal', 9831130399, '827ccb0eea8a706c4c34a16891f84e7b', '12345', 'swagat@kazmatechnology.com', 'kazma02', 'Developer', 'Active', '0000-00-00 00:00:00', '2020-07-24 11:43:17');

-- --------------------------------------------------------

--
-- Table structure for table `fitters`
--

CREATE TABLE `fitters` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_code` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fitters`
--

INSERT INTO `fitters` (`id`, `name`, `mobile`, `password`, `text_password`, `email`, `user_code`, `designation`, `status`, `created_at`, `modified_at`) VALUES
(1, 'Shantanu Mandal', 9475875421, '827ccb0eea8a706c4c34a16891f84e7b', '12345', 'shantanu@kazmatechnology.com', 'FT04', 'Fitterman', 'Active', '0000-00-00 00:00:00', '2020-07-24 11:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `itemCode` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `itemCode`, `status`) VALUES
(3, 'NOB-0010', 'Active'),
(4, 'NOB-0011', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `salesOrderId` int(11) NOT NULL,
  `orderNumber` int(11) NOT NULL,
  `itemCode` varchar(100) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `foreignDescription` text CHARACTER SET utf8 NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `salesOrderId`, `orderNumber`, `itemCode`, `description`, `foreignDescription`, `quantity`, `created_at`, `modified_at`) VALUES
(1, 151939, 220265, 'NOB-0014', '', 'مودا', 1, '2020-07-17 16:08:28', '2020-07-24 15:15:34'),
(2, 151939, 220265, 'NOB-0015', '', 'مودا', 1, '2020-07-24 11:15:58', '2020-07-24 15:15:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Employee'),
(2, 'Sales Man'),
(3, 'Delivery Man'),
(4, 'Fitter');

-- --------------------------------------------------------

--
-- Table structure for table `sales_men`
--

CREATE TABLE `sales_men` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_code` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_orders`
--

CREATE TABLE `sales_orders` (
  `id` int(11) NOT NULL,
  `salesOrderId` int(11) NOT NULL,
  `orderNumber` int(11) NOT NULL,
  `orderDate` date NOT NULL,
  `orderDueDate` date NOT NULL,
  `customerCode` varchar(100) NOT NULL,
  `customerName` varchar(100) NOT NULL,
  `stageId` int(11) NOT NULL,
  `vatAmount` decimal(10,2) NOT NULL,
  `orderTotal` decimal(12,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_orders`
--

INSERT INTO `sales_orders` (`id`, `salesOrderId`, `orderNumber`, `orderDate`, `orderDueDate`, `customerCode`, `customerName`, `stageId`, `vatAmount`, `orderTotal`, `created_at`, `modified_at`) VALUES
(1, 151939, 220265, '2019-10-31', '2019-03-04', 'IX12-000185', 'Yaser Mohammed', 9, '1857.15', '39000.01', '2020-07-17 15:42:35', '2020-07-27 06:54:19'),
(3, 151938, 220266, '2019-10-31', '2019-03-04', 'IX12-000185', 'Yaser Mohammed', 8, '1857.15', '39000.50', '2020-07-22 14:06:20', '2020-07-24 11:05:17');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_stages`
--

CREATE TABLE `sales_order_stages` (
  `id` int(11) NOT NULL,
  `stageId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order_stages`
--

INSERT INTO `sales_order_stages` (`id`, `stageId`, `name`) VALUES
(1, 0, 'Sales Offer'),
(2, 1, 'Sales Contract'),
(3, 2, 'Sent for Technical Review'),
(4, 3, 'Returned from Technical for Amendment'),
(5, 4, 'Approved by Technical'),
(6, 5, 'Request for Production'),
(7, 6, 'Purchase Order Raised'),
(8, 7, 'Inventory Received'),
(9, 8, 'Request for Delivery'),
(10, 9, 'Delivered'),
(11, 10, 'Partial Delivery');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_men`
--
ALTER TABLE `delivery_men`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fitters`
--
ALTER TABLE `fitters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_men`
--
ALTER TABLE `sales_men`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_orders`
--
ALTER TABLE `sales_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_stages`
--
ALTER TABLE `sales_order_stages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `delivery_men`
--
ALTER TABLE `delivery_men`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fitters`
--
ALTER TABLE `fitters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sales_men`
--
ALTER TABLE `sales_men`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_orders`
--
ALTER TABLE `sales_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sales_order_stages`
--
ALTER TABLE `sales_order_stages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
