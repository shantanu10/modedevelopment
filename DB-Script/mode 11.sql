-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2020 at 12:45 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mode`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(155) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `text_password`) VALUES
(1, 'Admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `contract` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`id`, `order_id`, `contract`, `created_at`, `modified_at`) VALUES
(8, 1, '1596179579SalesOrder.pdf', '2020-07-30 19:29:51', '2020-07-31 07:12:59'),
(13, 7, '1596865172SalesOrder.pdf', '2020-08-08 11:09:32', '2020-08-08 05:39:32'),
(14, 3, '1596865227SalesOrder.pdf', '2020-08-08 11:10:27', '2020-08-08 05:40:27'),
(15, 4, '1596865252SalesOrder.pdf', '2020-08-08 11:10:52', '2020-08-08 05:40:52'),
(16, 5, '1596865296SalesOrder.pdf', '2020-08-08 11:11:36', '2020-08-08 05:41:36'),
(17, 6, '1596866878SalesOrder.pdf', '2020-08-08 11:37:58', '2020-08-08 06:07:58'),
(18, 8, '1596866893SalesOrder.pdf', '2020-08-08 11:38:13', '2020-08-08 06:08:13'),
(19, 9, '1596866931SalesOrder.pdf', '2020-08-08 11:38:51', '2020-08-08 06:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `country_codes`
--

CREATE TABLE `country_codes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `countryCode` varchar(20) NOT NULL,
  `callingCode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country_codes`
--

INSERT INTO `country_codes` (`id`, `name`, `countryCode`, `callingCode`) VALUES
(1, 'Bahrain', 'BH', 973),
(2, 'Egypt', 'EG', 20),
(3, 'Iran', 'IR', 98),
(4, 'Iraq', 'IQ', 964),
(5, 'Israel', 'IL', 972),
(6, 'Jordan', 'JO', 962),
(7, 'Kuwait', 'KW', 965),
(8, 'Lebanon', 'LB', 961),
(9, 'Oman', 'OM', 968),
(10, 'Palestinian Territory', 'PS', 970),
(11, 'Qatar', 'QA', 974),
(12, 'Saudi Arabia', 'SA', 966),
(13, 'Syria', 'SY', 963),
(14, 'Turkey', 'TR', 90),
(15, 'United Arab Emirates', 'AE', 971),
(16, 'Yemen', 'YE', 967),
(17, 'India', 'IN', 91);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `foreignName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `callingCode` int(11) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `oldMobile` bigint(20) NOT NULL,
  `customerCode` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `foreignAddress` text CHARACTER SET utf8 NOT NULL,
  `altMobile` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `birthDate` date NOT NULL,
  `anniversaryDate` date NOT NULL,
  `locationCoords` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `fcm_token` text,
  `otp` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `foreignName`, `callingCode`, `mobile`, `oldMobile`, `customerCode`, `address`, `foreignAddress`, `altMobile`, `email`, `birthDate`, `anniversaryDate`, `locationCoords`, `image`, `status`, `fcm_token`, `otp`, `created_at`, `modified_at`) VALUES
(1, 'Shantanu Mandal', 'علي فيصل ال', 91, 8389023404, 8389023404, 'IX12-000185', 'Arambagh,Hooghly', '', 9474565257, 'shantanu@kazmatechnology.com', '1986-06-20', '2018-06-20', '[\"22.8765\",\"87.7910\"]', '', 'Active', '', 458403, '2020-07-17 14:57:17', '2020-07-17 14:57:17'),
(10, 'Muzaffar Ahmed', 'علي فيصل ال', 91, 9052531507, 9052531507, 'IX12-000186', '', '', 0, '', '0000-00-00', '0000-00-00', '', '', 'Active', 'dgjhghjadgahdagddgahdga', 0, '2020-07-21 18:26:13', '2020-07-21 18:26:13'),
(11, 'Shantanu Mandala', 'علي فيصل ال', 91, 9876543210, 9876543210, 'IX12-000180', 'Arambagh,Hooghly', '', 9474565257, 'shantanu@kazmatechnology.com', '1986-06-20', '2018-06-20', '\"\"', '', 'Active', '', 123456, '2020-07-21 18:26:13', '2020-07-21 18:26:13'),
(12, 'Swagat Bose', 'علي فيصل ال', 91, 9831130399, 9831130399, 'IX12-000187', '', '', 0, '', '0000-00-00', '0000-00-00', '', '', 'Active', '', 0, '2020-07-21 18:26:13', '2020-07-21 18:26:13'),
(13, 'Ranajay Chakrabarty', 'علي فيصل ال', 91, 7980233910, 7980233910, 'IX12-000188', '', '', 0, '', '0000-00-00', '0000-00-00', '', '', 'Active', '', 373082, '2020-07-21 18:26:13', '2020-07-21 18:26:13'),
(14, 'Wasim Android', 'علي فيصل ال', 91, 7044055836, 7044055836, 'IX12-000189', 'Garia,Kolkata-700084', '', 7044055836, 'wasim@gmail.com', '1986-06-20', '2018-06-20', '[\"22.4660\",\"88.3928\"]', '', 'Active', '', 221744, '2020-07-21 18:26:13', '2020-07-21 18:26:13');

-- --------------------------------------------------------

--
-- Table structure for table `customer_tokens`
--

CREATE TABLE `customer_tokens` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `deviceId` varchar(100) NOT NULL,
  `token` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_tokens`
--

INSERT INTO `customer_tokens` (`id`, `customer_id`, `deviceId`, `token`) VALUES
(1, 11, 'goldfish_x86', 'QOoZXK3946LYIkq2'),
(2, 11, 'msm8953', 'g2xzD4LpsG3iJq6l'),
(3, 11, 'sdm845', 'ZRtduWmpfcF9YiUS'),
(4, 1, 'sdm636', 'tOifSYV3PlAT6X5I'),
(5, 14, 'exynos9611', 'C3WyvGNwB4dn71Oi'),
(6, 11, 'goldfish_x86', 'Wfc3URN0O7Sx2gXG'),
(7, 13, 'begoniain', 'xE0wbHplzrqLBGJi'),
(8, 11, 'exynos9611', 'bOuioCDh6FALZ8R4'),
(9, 11, 'exynos9611', 'xUFtDfQ3rYMJpETG'),
(10, 11, 'exynos9611', '691dD4GFItmSjXh2'),
(11, 11, 'exynos9611', 'AhyPYMvE3lbVfKFH'),
(12, 14, 'exynos9611', 'yHATe93FBCuvk4df'),
(13, 11, 'exynos9611', 'yLVJYMioO76AExkl'),
(14, 11, 'goldfish_x86', 'ZLAv9hP1e78frqgp');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_men`
--

CREATE TABLE `delivery_men` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `callingCode` int(11) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_code` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `fcm_token` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_men`
--

INSERT INTO `delivery_men` (`id`, `name`, `callingCode`, `mobile`, `password`, `text_password`, `email`, `user_code`, `designation`, `status`, `fcm_token`, `created_at`, `modified_at`) VALUES
(1, 'Shantanu Mandal', 966, 9874533333, '827ccb0eea8a706c4c34a16891f84e7b', '12345', 'swagat@kazmatechnology.com', 'dl05', 'deliveryman', 'Active', '', '0000-00-00 00:00:00', '2020-08-11 05:47:44'),
(2, 'Swagat Bose', 966, 9831130399, 'e10adc3949ba59abbe56e057f20f883e', '123456', 'swagat@kazmatechnology.com', 'dv20', 'Delivery Man', 'Active', '', '0000-00-00 00:00:00', '2020-08-11 05:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_schedules`
--

CREATE TABLE `delivery_schedules` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `delivery_man_id` int(11) NOT NULL,
  `assign_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_schedules`
--

INSERT INTO `delivery_schedules` (`id`, `delivery_id`, `delivery_man_id`, `assign_date`, `delivery_date`, `created_at`, `modified_at`) VALUES
(1, 15, 2, '2020-08-12', '2020-08-14', '2020-08-12 08:50:02', '2020-08-12 05:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `display_contents`
--

CREATE TABLE `display_contents` (
  `id` int(11) NOT NULL,
  `file_type` enum('image','video') NOT NULL,
  `content_type` enum('item','stage') NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `stageId` int(11) DEFAULT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `display_contents`
--

INSERT INTO `display_contents` (`id`, `file_type`, `content_type`, `itemId`, `stageId`, `status`, `created_at`, `modified_at`) VALUES
(1, 'video', 'stage', NULL, 7, 'Active', '2020-08-06 12:48:38', '2020-08-07 07:18:44'),
(2, 'image', 'stage', NULL, 9, 'Active', '2020-08-06 12:48:46', '2020-08-07 07:18:51'),
(3, 'image', 'item', 4, 0, 'Active', '2020-08-06 12:48:56', '2020-08-07 07:18:58'),
(4, 'image', 'stage', 0, 8, 'Active', '2020-08-07 12:48:52', '2020-08-07 07:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `callingCode` int(11) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `employee_code` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `callingCode`, `mobile`, `password`, `text_password`, `email`, `employee_code`, `designation`, `status`, `created_at`, `modified_at`) VALUES
(4, 'Shantanu Mandal', 966, 9831130399, '827ccb0eea8a706c4c34a16891f84e7b', '12345', 'swagat@kazmatechnology.com', 'kazma02', 'Developer', 'Active', '0000-00-00 00:00:00', '2020-08-11 05:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `fitters`
--

CREATE TABLE `fitters` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `callingCode` int(11) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `text_password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_code` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `fcm_token` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fitters`
--

INSERT INTO `fitters` (`id`, `name`, `callingCode`, `mobile`, `password`, `text_password`, `email`, `user_code`, `designation`, `status`, `fcm_token`, `created_at`, `modified_at`) VALUES
(1, 'Shantanu Mandal', 966, 9475875421, '827ccb0eea8a706c4c34a16891f84e7b', '12345', 'shantanu@kazmatechnology.com', 'FT04', 'Fitterman', 'Active', '', '0000-00-00 00:00:00', '2020-08-11 05:42:58');

-- --------------------------------------------------------

--
-- Table structure for table `fitting_schedules`
--

CREATE TABLE `fitting_schedules` (
  `id` int(11) NOT NULL,
  `order_fitting_id` int(11) NOT NULL,
  `fitter_id` int(11) NOT NULL,
  `assign_date` date NOT NULL,
  `fitting_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fitting_schedules`
--

INSERT INTO `fitting_schedules` (`id`, `order_fitting_id`, `fitter_id`, `assign_date`, `fitting_date`, `created_at`, `modified_at`) VALUES
(1, 15, 1, '2020-08-12', '2020-08-15', '2020-08-12 08:52:57', '2020-08-12 05:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `image_videos`
--

CREATE TABLE `image_videos` (
  `id` int(11) NOT NULL,
  `display_content_id` int(11) NOT NULL,
  `image_video` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_videos`
--

INSERT INTO `image_videos` (`id`, `display_content_id`, `image_video`) VALUES
(6, 3, '1596544141Jellyfish.jpg'),
(9, 4, '1596722519download(1).jpg'),
(10, 4, '1596722519download.jpg'),
(11, 2, '1596790996kitchen-ideas-calderone-kitchen-006-1583960334.jpg'),
(12, 2, '1596790996kitchen-ideas-hbx110119wholehome-015-1572549271.jpg'),
(13, 2, '15967909961524514638493.jpeg'),
(14, 1, '1596872284MOBILIA_2019.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `itemCode` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `itemCode`, `status`) VALUES
(3, 'NOB-0010', 'Active'),
(4, 'NOB-0011', 'Active'),
(5, 'NOB-0012', 'Active'),
(6, 'NOB-0013', 'Active'),
(7, 'NOB-0014', 'Active'),
(8, 'NOB-0015', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `item_receipts`
--

CREATE TABLE `item_receipts` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `itemCode` varchar(20) NOT NULL,
  `palletCode` varchar(100) NOT NULL,
  `consignmentNo` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_delivery`
--

CREATE TABLE `order_delivery` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `stageId` int(11) NOT NULL,
  `received_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_delivery`
--

INSERT INTO `order_delivery` (`id`, `order_id`, `stageId`, `received_at`, `created_at`, `modified_at`) VALUES
(14, 8, 7, '2020-08-11 14:57:36', '2020-08-11 14:57:36', '2020-08-12 04:32:49'),
(15, 4, 8, '2020-08-11 14:57:37', '2020-08-11 14:57:37', '2020-08-12 05:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `order_fitting`
--

CREATE TABLE `order_fitting` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `stageId` int(11) NOT NULL,
  `received_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_fitting`
--

INSERT INTO `order_fitting` (`id`, `order_id`, `stageId`, `received_at`, `created_at`, `modified_at`) VALUES
(14, 8, 7, '2020-08-11 14:57:36', '2020-08-11 14:57:36', '2020-08-12 04:32:37'),
(15, 4, 14, '2020-08-11 14:57:37', '2020-08-11 14:57:37', '2020-08-12 05:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `salesOrderId` int(11) NOT NULL,
  `orderNumber` int(11) NOT NULL,
  `itemCode` varchar(100) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `foreignDescription` text CHARACTER SET utf8 NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `salesOrderId`, `orderNumber`, `itemCode`, `description`, `foreignDescription`, `quantity`, `created_at`, `modified_at`) VALUES
(1, 1, 151939, 220265, 'NOB-0014', '', 'مودا', 1, '2020-07-17 16:08:28', '2020-07-28 08:25:08'),
(2, 1, 151939, 220265, 'NOB-0015', '', 'مودا', 1, '2020-07-24 11:15:58', '2020-07-28 08:25:12'),
(3, 7, 151943, 220273, 'NOB-0013', 'kitchen item', 'مودا', 1, '2020-08-08 07:18:28', '2020-08-08 04:18:28'),
(4, 7, 151943, 220273, 'NOB-0014', 'kitchen item', 'مودا', 1, '2020-08-08 07:18:47', '2020-08-08 04:18:47'),
(5, 6, 151942, 220272, 'NOB-0012', 'kitchen item', 'مودا', 1, '2020-08-08 07:19:19', '2020-08-08 04:19:19'),
(6, 6, 151942, 220272, 'NOB-0015', 'kitchen item', 'مودا', 1, '2020-08-08 07:19:39', '2020-08-08 04:19:39'),
(7, 5, 151941, 220271, 'NOB-0015', 'kitchen item', 'مودا', 1, '2020-08-08 07:20:32', '2020-08-08 04:20:32'),
(8, 5, 151941, 220271, 'NOB-0012', 'kitchen item', 'مودا', 1, '2020-08-08 07:20:47', '2020-08-08 04:20:47'),
(9, 4, 151940, 220270, 'NOB-0012', 'kitchen item', 'مودا', 1, '2020-08-08 07:21:15', '2020-08-08 04:21:15'),
(10, 4, 151940, 220270, 'NOB-0013', 'kitchen item', 'مودا', 1, '2020-08-08 07:21:19', '2020-08-08 04:21:19'),
(11, 3, 151938, 220266, 'NOB-0013', 'kitchen item', 'مودا', 1, '2020-08-08 07:21:58', '2020-08-08 04:21:58'),
(12, 3, 151938, 220266, 'NOB-0011', 'kitchen item', 'مودا', 1, '2020-08-08 07:22:03', '2020-08-08 04:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `order_type`
--

CREATE TABLE `order_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_type`
--

INSERT INTO `order_type` (`id`, `name`) VALUES
(1, 'Kitchen Replacements'),
(2, 'Kitchen Sets'),
(3, 'Accessories'),
(4, 'Appliances'),
(5, 'Worktops'),
(6, 'Services');

-- --------------------------------------------------------

--
-- Table structure for table `order_type_images`
--

CREATE TABLE `order_type_images` (
  `id` int(11) NOT NULL,
  `orderTypeId` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_type_images`
--

INSERT INTO `order_type_images` (`id`, `orderTypeId`, `image`) VALUES
(2, 2, '1596722085kitchen-ideas-calderone-kitchen-006-1583960334.jpg'),
(3, 2, '1596722085kitchen-ideas-hbx110119wholehome-015-1572549271.jpg'),
(4, 2, '15967220851524514638493.jpeg'),
(6, 4, '1596806585download.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sales_men`
--

CREATE TABLE `sales_men` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `countryCode` int(11) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_code` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_orders`
--

CREATE TABLE `sales_orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `orderTypeId` int(11) NOT NULL,
  `salesOrderId` int(11) NOT NULL,
  `orderNumber` int(11) NOT NULL,
  `orderDate` date NOT NULL,
  `orderDueDate` date NOT NULL,
  `customerCode` varchar(100) NOT NULL,
  `customerName` varchar(100) NOT NULL,
  `stageId` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `vatAmount` decimal(10,2) NOT NULL,
  `orderTotal` decimal(12,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_orders`
--

INSERT INTO `sales_orders` (`id`, `customer_id`, `orderTypeId`, `salesOrderId`, `orderNumber`, `orderDate`, `orderDueDate`, `customerCode`, `customerName`, `stageId`, `currency`, `vatAmount`, `orderTotal`, `created_at`, `modified_at`) VALUES
(1, 11, 2, 151939, 220265, '2019-10-31', '2019-03-04', 'IX12-000180', 'Wasim', 1, '', '1857.15', '39000.01', '2020-07-17 15:42:35', '2020-08-11 07:29:43'),
(3, 10, 2, 151938, 220266, '2019-10-31', '2019-03-04', 'IX12-000186', 'Muzaffar Ahmed', 1, '', '1857.15', '39000.50', '2020-07-22 14:06:20', '2020-08-11 07:29:43'),
(4, 1, 2, 151940, 220270, '2020-08-01', '2020-08-20', 'IX12-000185', 'Shantanu Mandal', 8, 'SAR', '2005.15', '40000.50', '2020-08-07 15:36:16', '2020-08-12 05:43:19'),
(5, 14, 4, 151941, 220271, '2020-08-01', '2020-08-20', 'IX12-000189', 'Wasim', 1, 'SAR', '2005.15', '40000.50', '2020-08-07 16:15:03', '2020-08-11 07:29:43'),
(6, 11, 4, 151942, 220272, '2020-08-01', '2020-08-20', 'IX12-000180', 'Wasim', 1, 'SAR', '2005.15', '40000.50', '2020-08-07 16:15:56', '2020-08-11 07:29:43'),
(7, 13, 2, 151943, 220273, '2020-08-01', '2020-08-20', 'IX12-000188', 'Ranajay Vhakrabarty', 1, 'SAR', '2005.15', '40000.50', '2020-08-08 07:10:43', '2020-08-11 07:29:43'),
(8, 1, 2, 151944, 220274, '2020-08-01', '2020-08-20', 'IX12-000185', 'Shantanu Mandal', 7, 'SAR', '2005.15', '40000.50', '2020-08-08 07:26:02', '2020-08-11 11:57:35'),
(9, 11, 2, 151945, 220275, '2020-08-01', '2020-08-20', 'IX12-000180', 'Wasim', 1, 'SAR', '2005.15', '40000.50', '2020-08-08 07:30:59', '2020-08-11 07:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_stages`
--

CREATE TABLE `sales_order_stages` (
  `stageId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `shortName` varchar(30) NOT NULL,
  `userGroup` varchar(100) NOT NULL,
  `stageMediaTitle` text NOT NULL,
  `stageMediaSubtitle` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order_stages`
--

INSERT INTO `sales_order_stages` (`stageId`, `name`, `shortName`, `userGroup`, `stageMediaTitle`, `stageMediaSubtitle`, `image`) VALUES
(1, 'Sales Contract', 'Contract', 'Customer', 'Media in Sales Contract stage', 'See how mode transfer your goods safely and securely', 'contract.png'),
(2, 'Sent to Technical', 'Tech Rev', 'Customer', '', 'See how mode transfer your goods safely and securely', 'processing.png'),
(3, 'Returned from Technical', 'Ret.Tech', 'Customer', '', 'See how mode transfer your goods safely and securely', 'processing.png'),
(4, 'Approved by Technical', 'Approved', 'Customer', '', 'See how mode transfer your goods safely and securely', 'processing.png'),
(5, 'Purchased Request Raised', 'Req.Raised', 'Customer', '', 'See how mode transfer your goods safely and securely', 'processing.png'),
(6, 'Purchase Order Raised', 'Order Raised', 'Customer', '', 'See how mode transfer your goods safely and securely', 'processing.png'),
(7, 'Inventory Received', 'Warehouse', 'Customer', 'Media in Inventory received stage', 'See how mode transfer your goods safely and securely', 'warehouse.png'),
(8, 'Delivery Man Assigned', 'Assign Delv.', 'Customer,Delivery', 'Media in request for Delivery Man Assigned stage', 'See how mode transfer your goods safely and securely', 'delivery.png'),
(9, 'Checklist Verified by Delivery Man', 'Checklist Verified', 'Delivery', 'Media in Checklist Verified by Delivery Man stage', 'See how mode transfer your goods safely and securely', 'delivery.png'),
(10, 'Accepted by Delivery Man', 'Accepted', 'Delivery', '', 'See how mode transfer your goods safely and securely', 'delivery.png'),
(11, 'Out for Delivery', 'Out for Delv.', 'Customer,Delivery', 'Media for Shipped stages', 'See how mode transfer your goods safely and securely', 'delivery.png'),
(12, 'Delivery Completed', 'Delivered', 'Customer,Delivery', 'Media for Delivered stages', 'See how mode transfer your goods safely and securely', 'delivery.png'),
(13, 'Customer Feedback received on Delivery', 'Customer Feedback', 'Customer,Delivery', 'Media for Customer Feedback stages', 'See how mode transfer your goods safely and securely', ''),
(14, 'Fitter Assigned', 'Fitter Assigned', 'Customer,Fitter', 'Media for Fitter Assigned stages', 'See how mode transfer your goods safely and securely', 'installations.png'),
(15, 'Items checklist verified by Fitter', 'Item Checklist Verified', 'Customer,Fitter', 'Media for Item Checklist Verified stages', 'See how mode transfer your goods safely and securely', 'installations.png'),
(16, 'Fitting completed', 'Fitting completed', 'Customer,Fitter', 'Media for Fitting completed stages', 'See how mode transfer your goods safely and securely', 'installations.png'),
(17, 'Customer Feedback received on Fitting', 'Feedback Received on Fitting', 'Customer,Fitter', 'Media for Feedback Received on Fitting stages', 'See how mode transfer your goods safely and securely', '');

-- --------------------------------------------------------

--
-- Table structure for table `stages_log`
--

CREATE TABLE `stages_log` (
  `id` int(11) NOT NULL,
  `stageId` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `isCompleted` tinyint(4) NOT NULL,
  `isCurrent` tinyint(4) NOT NULL,
  `creationDateTime` datetime NOT NULL,
  `modifiedDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stages_log`
--

INSERT INTO `stages_log` (`id`, `stageId`, `order_id`, `isCompleted`, `isCurrent`, `creationDateTime`, `modifiedDateTime`) VALUES
(1, 1, 4, 0, 0, '2020-08-11 00:00:00', '2020-08-11 09:33:55'),
(2, 1, 8, 0, 0, '2020-08-11 00:00:00', '2020-08-11 09:34:16'),
(6, 2, 4, 0, 0, '2020-08-11 14:42:39', '2020-08-11 11:42:39'),
(7, 2, 8, 0, 0, '2020-08-11 14:43:20', '2020-08-11 11:43:20'),
(8, 3, 8, 0, 0, '2020-08-11 14:44:07', '2020-08-11 11:44:07'),
(9, 3, 4, 0, 0, '2020-08-11 14:44:07', '2020-08-11 11:44:07'),
(10, 4, 8, 0, 0, '2020-08-11 14:44:25', '2020-08-11 11:44:25'),
(11, 4, 4, 0, 0, '2020-08-11 14:44:25', '2020-08-11 11:44:25'),
(12, 5, 8, 0, 0, '2020-08-11 14:44:38', '2020-08-11 11:44:38'),
(13, 5, 4, 0, 0, '2020-08-11 14:44:39', '2020-08-11 11:44:39'),
(18, 6, 8, 0, 0, '2020-08-11 14:55:20', '2020-08-11 11:55:20'),
(19, 6, 4, 0, 0, '2020-08-11 14:55:20', '2020-08-11 11:55:20'),
(22, 7, 8, 0, 0, '2020-08-11 14:57:36', '2020-08-11 11:57:36'),
(23, 7, 4, 0, 0, '2020-08-11 14:57:37', '2020-08-11 11:57:37'),
(26, 8, 4, 0, 0, '2020-08-12 08:50:02', '2020-08-12 05:50:02'),
(28, 14, 4, 0, 0, '2020-08-12 08:52:57', '2020-08-12 05:52:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `country_codes`
--
ALTER TABLE `country_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_tokens`
--
ALTER TABLE `customer_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_men`
--
ALTER TABLE `delivery_men`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_schedules`
--
ALTER TABLE `delivery_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_id` (`delivery_id`),
  ADD KEY `delivery_man_id` (`delivery_man_id`);

--
-- Indexes for table `display_contents`
--
ALTER TABLE `display_contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stageId` (`stageId`),
  ADD KEY `itemId` (`itemId`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fitters`
--
ALTER TABLE `fitters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fitting_schedules`
--
ALTER TABLE `fitting_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fitter_id` (`fitter_id`),
  ADD KEY `order_fitting_id` (`order_fitting_id`);

--
-- Indexes for table `image_videos`
--
ALTER TABLE `image_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `display_content_id` (`display_content_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_receipts`
--
ALTER TABLE `item_receipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `order_delivery`
--
ALTER TABLE `order_delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `stageId` (`stageId`);

--
-- Indexes for table `order_fitting`
--
ALTER TABLE `order_fitting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `stageId` (`stageId`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `order_type`
--
ALTER TABLE `order_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_type_images`
--
ALTER TABLE `order_type_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderTypeId` (`orderTypeId`);

--
-- Indexes for table `sales_men`
--
ALTER TABLE `sales_men`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_orders`
--
ALTER TABLE `sales_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `stageId` (`stageId`),
  ADD KEY `orderTypeId` (`orderTypeId`);

--
-- Indexes for table `sales_order_stages`
--
ALTER TABLE `sales_order_stages`
  ADD PRIMARY KEY (`stageId`);

--
-- Indexes for table `stages_log`
--
ALTER TABLE `stages_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stageId` (`stageId`),
  ADD KEY `order_id` (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `country_codes`
--
ALTER TABLE `country_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `customer_tokens`
--
ALTER TABLE `customer_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `delivery_men`
--
ALTER TABLE `delivery_men`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `delivery_schedules`
--
ALTER TABLE `delivery_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `display_contents`
--
ALTER TABLE `display_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fitters`
--
ALTER TABLE `fitters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fitting_schedules`
--
ALTER TABLE `fitting_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `image_videos`
--
ALTER TABLE `image_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `item_receipts`
--
ALTER TABLE `item_receipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_delivery`
--
ALTER TABLE `order_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order_fitting`
--
ALTER TABLE `order_fitting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `order_type`
--
ALTER TABLE `order_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order_type_images`
--
ALTER TABLE `order_type_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sales_men`
--
ALTER TABLE `sales_men`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_orders`
--
ALTER TABLE `sales_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sales_order_stages`
--
ALTER TABLE `sales_order_stages`
  MODIFY `stageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `stages_log`
--
ALTER TABLE `stages_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contracts`
--
ALTER TABLE `contracts`
  ADD CONSTRAINT `contracts_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `sales_orders` (`id`);

--
-- Constraints for table `delivery_schedules`
--
ALTER TABLE `delivery_schedules`
  ADD CONSTRAINT `delivery_schedules_ibfk_1` FOREIGN KEY (`delivery_id`) REFERENCES `order_delivery` (`id`),
  ADD CONSTRAINT `delivery_schedules_ibfk_2` FOREIGN KEY (`delivery_man_id`) REFERENCES `delivery_men` (`id`);

--
-- Constraints for table `fitting_schedules`
--
ALTER TABLE `fitting_schedules`
  ADD CONSTRAINT `fitting_schedules_ibfk_1` FOREIGN KEY (`fitter_id`) REFERENCES `fitters` (`id`),
  ADD CONSTRAINT `fitting_schedules_ibfk_2` FOREIGN KEY (`order_fitting_id`) REFERENCES `order_fitting` (`id`);

--
-- Constraints for table `image_videos`
--
ALTER TABLE `image_videos`
  ADD CONSTRAINT `image_videos_ibfk_1` FOREIGN KEY (`display_content_id`) REFERENCES `display_contents` (`id`);

--
-- Constraints for table `item_receipts`
--
ALTER TABLE `item_receipts`
  ADD CONSTRAINT `item_receipts_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `item_receipts_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `sales_orders` (`id`);

--
-- Constraints for table `order_delivery`
--
ALTER TABLE `order_delivery`
  ADD CONSTRAINT `order_delivery_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `sales_orders` (`id`),
  ADD CONSTRAINT `order_delivery_ibfk_2` FOREIGN KEY (`stageId`) REFERENCES `sales_order_stages` (`stageId`);

--
-- Constraints for table `order_fitting`
--
ALTER TABLE `order_fitting`
  ADD CONSTRAINT `order_fitting_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `sales_orders` (`id`),
  ADD CONSTRAINT `order_fitting_ibfk_2` FOREIGN KEY (`stageId`) REFERENCES `sales_order_stages` (`stageId`);

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `sales_orders` (`id`);

--
-- Constraints for table `order_type_images`
--
ALTER TABLE `order_type_images`
  ADD CONSTRAINT `order_type_images_ibfk_1` FOREIGN KEY (`orderTypeId`) REFERENCES `order_type` (`id`);

--
-- Constraints for table `sales_orders`
--
ALTER TABLE `sales_orders`
  ADD CONSTRAINT `sales_orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `sales_orders_ibfk_2` FOREIGN KEY (`stageId`) REFERENCES `sales_order_stages` (`stageId`),
  ADD CONSTRAINT `sales_orders_ibfk_3` FOREIGN KEY (`orderTypeId`) REFERENCES `order_type` (`id`);

--
-- Constraints for table `stages_log`
--
ALTER TABLE `stages_log`
  ADD CONSTRAINT `stages_log_ibfk_1` FOREIGN KEY (`stageId`) REFERENCES `sales_order_stages` (`stageId`),
  ADD CONSTRAINT `stages_log_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `sales_orders` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
