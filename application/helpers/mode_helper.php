<?php

function pre($data, $exit = false){
    echo "<pre>";
    print_r($data);
    if($exit) exit;
    echo "</pre>";
}
function getusername(){
	 $CI = & get_instance();
	$data['name'] = $CI->session->userdata('name');
	//print_r($data);
	return $data;
	}

function session_check()
{
    $CI = & get_instance();
   
    if($CI->session->has_userdata('user_id') != 1)
        redirect('login');
}
function to_db_date($time_stamp = '') {
    $time_stamp = ($time_stamp == '') ? time() : $time_stamp;
    return date('Y-m-d H:i:s', $time_stamp);
}

