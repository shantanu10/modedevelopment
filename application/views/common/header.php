<?php 
 error_reporting(0);
$link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
	$innerpage = $link_array[3];
     $page = end($link_array);
	 
	?>
<style></style>
<div class="sidebar-menu">
  <div class="logo1"> 
    <!-- <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span>  --> 
    <a href="<?php echo base_url()?>Dashboard/"> 
    <img id="logo" src="<?php echo base_url();?>assets/img/logo.png" alt="Logo"  width="50%" height="50%" style="width:75% !important"/></a> </div>
  <div id="myElement" class="simplebar menu">
    <header class="header" role="banner">
      <nav class="nav" role="navigation">
        <ul class="nav__list">
         <!--<li class="top <?php if($page=='Dashboard'){ echo 'active1';}?>" id="ID113"><a href="<?php echo base_url()?>Dashboard/"><i class="fa fa-dashboard"></i> Home</a> </li>-->
     <li class="top <?php if($page=='employees' || $page=='deliveryman' || $page=='salesman' || $page=='fitter' || $page=='order_type' || $page=='stages' || $page=='items' || $page=='images' || $page=='videos' || $page=='contracts'){ echo 'active1';}?>" id="ID18"> <a> <span class="fa fa-angle-right"></span> <i class="fa fa-cog" aria-hidden="true"></i> <b>Configure </b> </a>
            <ul class="group-list">
          
           <li class="top <?php if($page=='employees'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/employees"> <i class="fa fa-user" aria-hidden="true"></i> <b>Employees</b> </a> </li>
            <li class="top <?php if($page=='deliveryman'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/deliveryman"><i class="fa fa-user" aria-hidden="true"></i> <b>Delivery Man</b> </a> </li>
<!--<li class="top <?php if($page=='salesman'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/salesman"><i class="fa fa-user" aria-hidden="true"></i> <b>Sales Man</b> </a> </li>-->
              <li class="top <?php if($page=='fitter'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/fitter"><i class="fa fa-user" aria-hidden="true"></i><b> Fitter</b> </a> </li>
               <li class="top <?php if($page=='order_type'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/order_type"><i class="fa fa-step-forward" aria-hidden="true"></i><b> Order Type</b> </a> </li>
               <li class="top <?php if($page=='stages'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/stages"><i class="fa fa-step-forward" aria-hidden="true"></i><b> Stages</b> </a> </li>
               <li class="top <?php if($page=='items'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/items"><i class="fa fa-list-alt" aria-hidden="true"></i><b> Items</b> </a> </li>
                <li class="top <?php if($page=='images'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/images"><i class="fa fa-picture-o" aria-hidden="true"></i><b> Images</b> </a> </li>
                 <li class="top <?php if($page=='videos'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/videos"><i class="fa fa-picture-o" aria-hidden="true"></i><b> Videos</b> </a> </li>
             <li class="top <?php if($page=='contracts'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/contracts"><i class="fa fa-upload" aria-hidden="true"></i><b> Contracts</b> </a> </li>
                       
            </ul>
           </li>   
          
  <li class="top <?php if($page=='customer' || $page=='view_customer'){ echo 'active1';}?>" id="ID10"> <a href="<?php echo base_url();?>customer" onclick="MenuSelect(18, 18, 18, 10);"><i class="fa fa-user" aria-hidden="true"></i> Business Partner</a> </li>
  <li class="top <?php if($page=='order' || $page=='view_order' || $page=='receipt'){ echo 'active1';}?>" id="ID10"> <a><span class="fa fa-angle-right"></span><i class="fa fa-list" aria-hidden="true"></i> Sales Order</a>
  <ul class="group-list">
           <li class="top <?php if($page=='order' || $page=='view_order'){ echo 'active1';}?>" id="ID10"> <a href="<?php echo base_url();?>order" onclick="MenuSelect(18, 18, 18, 10);"><i class="fa fa-list" aria-hidden="true"></i> Orders</a></li>
           <li class="top <?php if($page=='receipt'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>order/receipt"> <i class="fa fa-file" aria-hidden="true"></i> <b>Material Receipt</b> </a> </li>
           </ul>
   </li>
  
  
  <li class="top <?php if($page=='delivery' || $page=='fitting' || $page=='schedules' || $page=='fittingschedules'){ echo 'active1';}?>" id="ID18"> <a> <span class="fa fa-angle-right"></span> <i class="fa fa-tasks" aria-hidden="true"></i> <b>Assignment </b> </a>
            <ul class="group-list">
          
           <li class="top <?php if($page=='delivery'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>delivery"> <i class="fa fa-truck" aria-hidden="true"></i> <b>Delivery</b> </a> </li>
           <li class="top <?php if($page=='schedules'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>delivery/schedules"> <i class="fa fa-calendar" aria-hidden="true"></i> <b>Delivery Schedules</b> </a> </li>
             <li class="top <?php if($page=='fitting'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>fitting"> <i class="fa fa-wrench" aria-hidden="true"></i> <b>Fitting</b> </a> </li>
           <li class="top <?php if($page=='fittingschedules'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>fitting/fittingschedules"> <i class="fa fa-calendar" aria-hidden="true"></i> <b>Fitting Schedules</b> </a> </li>                      
            </ul>
           </li>
  
  
  </ul>  
 
      </nav>
    </header>
  </div>
</div>
<div class="responsive-logo">
  <div class="logo1"> <a href="#"> <!--<img id="logo" src="<?php echo base_url()?>assets/img/logo.png" alt="Logo" width="100%" height="100%" style="width:24% !important">--> </a> </div>
</div>
<!-- Offcanvas Navigation Start --> 
