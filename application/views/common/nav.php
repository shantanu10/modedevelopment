<div class="nav-offcanvas">
    <button type="button" class="close" id="offCanvasClose" aria-label="Close">
        <i class="ti-close"></i>
    </button>
    <div class="logo1"> 
    <!-- <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span>  --> 
    <a href="<?php echo base_url()?>Dashboard/"> 
    <img id="logo" src="<?php echo base_url();?>assets/img/logo.png" alt="Logo"  width="50%" height="50%" style="width:75% !important"/></a> </div>
    <div class="nav-offcanvas-menu header">
<nav class="nav" role="navigation">
<ul class="nav__list">
        <!-- <li class="top <?php if($page=='Dashboard'){ echo 'active1';}?>" id="ID113"><a href="<?php echo base_url()?>Dashboard/"><i class="fa fa-dashboard"></i> Home</a> </li>-->
    <li class="top <?php if($page=='employees' || $page=='deliveryman' || $page=='salesman' || $page=='fitter' || $page=='order_type' || $page=='stages' || $page=='items' || $page=='images' || $page=='videos' || $page=='contracts'){ echo 'active1';}?>" id="ID18"> <a> <span class="fa fa-angle-right"></span> <i class="fa fa-cog" aria-hidden="true"></i> <b>Configure </b> </a>
            <ul class="group-list">
          
           <li class="top <?php if($page=='employees'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/employees"> <i class="fa fa-user" aria-hidden="true"></i> <b>Employees</b> </a> </li>
            <li class="top <?php if($page=='deliveryman'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/deliveryman"><i class="fa fa-user" aria-hidden="true"></i> <b>Delivery Man</b> </a> </li>
<!--<li class="top <?php if($page=='salesman'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/salesman"><i class="fa fa-user" aria-hidden="true"></i> <b>Sales Man</b> </a> </li>-->
              <li class="top <?php if($page=='fitter'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>user/fitter"><i class="fa fa-user" aria-hidden="true"></i><b> Fitter</b> </a> </li>
               <li class="top <?php if($page=='order_type'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/order_type"><i class="fa fa-step-forward" aria-hidden="true"></i><b> Order Type</b> </a> </li>
               <li class="top <?php if($page=='stages'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/stages"><i class="fa fa-step-forward" aria-hidden="true"></i><b> Stages</b> </a> </li>
               <li class="top <?php if($page=='items'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/items"><i class="fa fa-list-alt" aria-hidden="true"></i><b> Items</b> </a> </li>
                <li class="top <?php if($page=='images'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/images"><i class="fa fa-picture-o" aria-hidden="true"></i><b> Images</b> </a> </li>
                 <li class="top <?php if($page=='videos'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/videos"><i class="fa fa-picture-o" aria-hidden="true"></i><b> Videos</b> </a> </li>
             <li class="top <?php if($page=='contracts'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>Dashboard/contracts"><i class="fa fa-upload" aria-hidden="true"></i><b> Contracts</b> </a> </li>
                       
            </ul>
           </li>   
          
  <li class="top <?php if($page=='customer' || $page=='view_customer'){ echo 'active1';}?>" id="ID10"> <a href="<?php echo base_url();?>customer" onclick="MenuSelect(18, 18, 18, 10);"><i class="fa fa-user" aria-hidden="true"></i> Business Partner</a> </li>
  <li class="top <?php if($page=='order' || $page=='view_order' || $page=='receipt'){ echo 'active1';}?>" id="ID10"> <a><span class="fa fa-angle-right"></span><i class="fa fa-list" aria-hidden="true"></i> Sales Order</a>
  <ul class="group-list">
           <li class="top <?php if($page=='order' || $page=='view_order'){ echo 'active1';}?>" id="ID10"> <a href="<?php echo base_url();?>order" onclick="MenuSelect(18, 18, 18, 10);"><i class="fa fa-list" aria-hidden="true"></i> Orders</a></li>
           <li class="top <?php if($page=='receipt'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>order/receipt"> <i class="fa fa-file" aria-hidden="true"></i> <b>Material Receipt</b> </a> </li>
           </ul>
   </li>
  
  
  <li class="top <?php if($page=='delivery' || $page=='fitting' || $page=='schedules' || $page=='fittingschedules'){ echo 'active1';}?>" id="ID18"> <a> <span class="fa fa-angle-right"></span> <i class="fa fa-tasks" aria-hidden="true"></i> <b>Assignment </b> </a>
            <ul class="group-list">
          
           <li class="top <?php if($page=='delivery'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>delivery"> <i class="fa fa-truck" aria-hidden="true"></i> <b>Delivery</b> </a> </li>
           <li class="top <?php if($page=='schedules'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>delivery/schedules"> <i class="fa fa-calendar" aria-hidden="true"></i> <b>Delivery Schedules</b> </a> </li>
             <li class="top <?php if($page=='fitting'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>fitting"> <i class="fa fa-wrench" aria-hidden="true"></i> <b>Fitting</b> </a> </li>
           <li class="top <?php if($page=='fittingschedules'){ echo 'active1';}?>" id="ID1"> <a href="<?php echo base_url();?>fitting/fittingschedules"> <i class="fa fa-calendar" aria-hidden="true"></i> <b>Fitting Schedules</b> </a> </li>                      
            </ul>
           </li>
  
 
  </ul>
          <!--<ul class="nav__list">
        <li class="top" id="ID1">
            <a>
                <span class="fa fa-angle-right"></span>

                <img src="img/master.png" alt="">
                <b>Master Admin1</b>
            </a>
            <ul class="group-list">
                <li class="top" id="ID2">
                    <a><span class="fa fa-angle-right"></span>Master Setup</a>
                    <ul class="sub-group-list">
                        <li class="top" id="ID3"><a href="/Admin/KazmaAdmin/FinancialYear" onclick="MenuSelect(1, 1, 2, 3);"><span class="icon-left"></span>Location Management</a> </li>
                        <li class="top" id="ID4"><a href="/Admin/KazmaAdmin/Depatrment" onclick="MenuSelect(1, 1, 2, 4);"><span class="icon-left"></span>Department</a> </li>
                        <li class="top" id="ID5"><a href="/Admin/KazmaAdmin/Designation" onclick="MenuSelect(1, 1, 2, 5);"><span class="icon-left"></span>Designation</a> </li>
                        <li class="top" id="ID6"><a href="/Admin/KazmaAdmin/LocatuionSetup" onclick="MenuSelect(1, 1, 2, 6);"><span class="icon-left"></span>Location</a> </li>
                        <li class="top" id="ID7"><a href="/Admin/KazmaAdmin/ShiftSetup" onclick="MenuSelect(1, 1, 2, 7);"><span class="icon-left"></span>Shift</a> </li>
                    </ul>
                </li>

                <li class="top" id="ID1002">
                    <a><span class="fa fa-angle-right"></span>Task Management Setup</a>
                    <ul class="sub-group-list">
                        <li class="top" id="ID1003"><a href="/Admin/TaskManagement/TaskStatus" onclick="MenuSelect(1, 1, 1002, 1003);"><span class="icon-left"></span>Task Status</a> </li>
                    </ul>
                </li>



                <li class="top" id="ID113"><a href="/Admin/RoleMng/Roles" onclick="MenuSelect(1, 1, 113, 0);"><span class="icon-left"></span>Roles</a> </li>
                <li class="top" id="ID114"><a href="/Admin/RoleMng/Administrators" onclick="MenuSelect(1, 1, 114, 0);"><span class="icon-left"></span>User Management</a> </li>
                <li class="top" id="ID115"><a href="/Admin/RoleMng/Team" onclick="MenuSelect(1, 1, 115, 0);"><span class="icon-left"></span>Team Management</a> </li>
            </ul>

        </li>
        <li class="top" id="ID18">
            <a>
                <span class="fa fa-angle-right"></span>
                <img src="img/hr.png" alt="">
                <b>HR User  </b>
            </a>

            <ul class="group-list">
                <li class="top" id="ID8">
                    <a><span class="fa fa-angle-right"></span>Organisation</a>
                    <ul class=" sub-group-list">
                        <li class="top" id="ID9"><a href="/Admin/KazmaAdmin/CompanySetup" onclick="MenuSelect(18, 18, 8, 9);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Organisation data</a> </li>
                        <li class="upper top" id="ID10">
                            <a><span class="fa fa-angle-right"></span>Organisation Policy</a>
                            <ul class="sub-group-list sub-group-list2">
                                <li class="top" id="ID11"><a href="/Admin/KazmaAdmin/CompanyPolicy" onclick="MenuSelect(18, 8, 10, 11);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Policy</a> </li>
                                <li class="top" id="ID12"><a href="/Admin/KazmaAdmin/CompanyDocument" onclick="MenuSelect(18, 8, 10, 12);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Documents / forms</a> </li>
                            </ul>
                        </li>
                        <li class="upper top" id="ID13">
                            <a><span class="fa fa-angle-right"></span>Leave Management</a>
                            <ul class="sub-group-list sub-group-list2">
                                <li class="top" id="ID14"><a href="/Admin/KazmaAdmin/HolidaySet" onclick="MenuSelect(18, 8, 13, 14);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Holidays</a> </li>
                                <li class="top" id="ID15"><a href="/Admin/KazmaAdmin/WeekSetup" onclick="MenuSelect(18, 8, 13, 15);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Week Set-up </a> </li>
                                <li class="top" id="ID16"><a href="/Admin/KazmaAdmin/LeaveSetup" onclick="MenuSelect(18, 8, 13, 16);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Leave Set-up</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="top" id="ID17"><a href="/Admin/KazmaAdmin/Employee" onclick="MenuSelect(18, 18, 18, 17);">Employee</a></li>
                <li class="top" id="ID19"><a href="/Admin/KazmaAdmin/TrackAttendance" onclick="MenuSelect(18, 18, 18, 19);"><span class="icon-left"></span>Attendance</a></li>
                
                <li class="top" id="ID20">
                    <a><span class="fa fa-angle-right"></span>Leave </a>
                    <ul class="sub-group-list">
                        <li class="top" id="ID29"><a href="/Admin/KazmaAdmin/EmployeeLeaveManage" onclick="MenuSelect(18, 18, 20, 29);"><span class="icon-left"></span>Approvals/Requests</a> </li>
                        <li class="top" id="ID30"><a href="/Admin/KazmaAdmin/LeaveBalance" onclick="MenuSelect(18, 18, 20, 30);"><span class="icon-left"></span>Leave Balance</a> </li>
                    </ul>
                </li>
                <li class="top" id="ID21">
                    <a><span class="fa fa-angle-right"></span>Payroll management </a>
                    <ul class="sub-group-list">
                        <li class="top" id="ID22"><a href="/Admin/KazmaAdmin/PayRollMasterSetup" onclick="MenuSelect(18, 18, 21, 22);"><span class="icon-left"></span> Payroll Master </a> </li>
                        <li class="top" id="ID23"><a href="/Admin/KazmaAdmin/EmployeeList" onclick="MenuSelect(18, 18, 21, 23);"><span class="icon-left"></span> Payroll</a> </li>
                        <li class="" id="ID24">
                            <a>Report <span class="fa fa-angle-right"></span></a>
                            <ul class="sub-group-list">
                                <li class="top" id="ID25"><a href="/Admin/Report/ListPaySlip" onclick="MenuSelect(18, 18, 21, 24);"><span class="icon-left"></span>Pay Slip </a> </li>
                                <li class="top" id="ID26"><a href="/Admin/Report/BankStatement" onclick="MenuSelect(18, 18, 21, 24);"><span class="icon-left"></span>Bank Statement </a> </li>
                                <li class="top" id="ID27"><a href="/Admin/Report/MISStatement" onclick="MenuSelect(18, 18, 21, 24);"><span class="icon-left"></span>MIS Statement </a> </li>
                                <li class="top" id="ID28"><a href="/Admin/Report/DeductionStatement" onclick="MenuSelect(18, 18, 21, 24);"><span class="icon-left"></span>Deduction Statement </a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>

            </ul>
        </li>
        <li class="top" id="ID50">
            <a>
                <span class="fa fa-angle-right"></span>
                <img src="img/hr.png" alt="">
                <b>Management User  </b>
            </a>

            <ul class="group-list">
                <li class="top" id="ID40">
                    <a><span class="fa fa-angle-right"></span>Organisation</a>
                    <ul class=" sub-group-list">
                        <li class="top" id="ID41"><a href="/Admin/ManageUser/CompanySetup" onclick="MenuSelect(50, 50, 40, 41);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Organisation data</a> </li>
                        <li class="upper top" id="ID42">
                            <a><span class="fa fa-angle-right"></span>Organisation Policy</a>
                            <ul class="sub-group-list sub-group-list2">
                                <li class="top" id="ID43"><a href="/Admin/ManageUser/CompanyPolicy" onclick="MenuSelect(50, 40, 42, 43);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Policy</a> </li>
                                <li class="top" id="ID44"><a href="/Admin/ManageUser/CompanyDocument" onclick="MenuSelect(50, 40, 42, 44);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Documents / forms</a> </li>
                            </ul>
                        </li>
                        <li class="upper top" id="ID45">
                            <a><span class="fa fa-angle-right"></span>Leave Management</a>
                            <ul class="sub-group-list sub-group-list2">
                                <li class="top" id="ID46"><a href="/Admin/ManageUser/HolidaySet" onclick="MenuSelect(50, 40, 45, 46);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Holidays</a> </li>
                                <li class="top" id="ID47"><a href="/Admin/ManageUser/WeekSetup" onclick="MenuSelect(50, 40, 45, 47);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Week Set-up </a> </li>
                                <li class="top" id="ID48"><a href="/Admin/ManageUser/LeaveSetup" onclick="MenuSelect(50, 40, 45, 48);"><span class="icon-left"><i class="fa fa-envelope-open-o fa-fw" aria-hidden="true"></i></span>Leave Set-up</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="top" id="ID51"><a href="/Admin/ManageUser/Employee" onclick="MenuSelect(50, 50, 50, 51);">Employee</a></li>
                <li class="top" id="ID52"><a href="/Admin/ManageUser/TrackAttendance" onclick="MenuSelect(50, 50, 50, 52);"><span class="icon-left"></span>Attendance</a></li>

                <li class="top" id="ID64"><a href="/Admin/ManageUser/Task" onclick="MenuSelect(50, 50, 50, 64);">Tasks</a></li>
                <li class="top" id="ID53">
                    <a><span class="fa fa-angle-right"></span>Leave </a>
                    <ul class="sub-group-list">
                        <li class="top" id="ID54"><a href="/Admin/ManageUser/EmployeeLeaveManage" onclick="MenuSelect(50, 50, 53, 54);"><span class="icon-left"></span>Approvals/Requests</a> </li>
                        <li class="top" id="ID55"><a href="/Admin/ManageUser/LeaveBalance" onclick="MenuSelect(50, 50, 53, 55);"><span class="icon-left"></span>Leave Balance</a> </li>
                    </ul>
                </li>
                <li class="top" id="ID56">
                    <a><span class="fa fa-angle-right"></span>Payroll management </a>
                    <ul class="sub-group-list">
                        <li class="top" id="ID57"><a href="/Admin/ManageUser/PayRollMasterSetup" onclick="MenuSelect(50, 50, 56, 57);"><span class="icon-left"></span> Payroll Master </a> </li>
                        
                        <li class="" id="ID59">
                            <a>Report <span class="fa fa-angle-right"></span></a>
                            <ul class="sub-group-list">
                                <li class="top" id="ID60"><a href="/Admin/ManageUser/ListPaySlip" onclick="MenuSelect(50, 50, 56, 59);"><span class="icon-left"></span>Pay Slip </a> </li>
                                <li class="top" id="ID61"><a href="/Admin/ManageUser/BankStatement" onclick="MenuSelect(50, 50, 56, 59);"><span class="icon-left"></span>Bank Statement </a> </li>
                                <li class="top" id="ID62"><a href="/Admin/ManageUser/MISStatement" onclick="MenuSelect(50, 50, 56, 59);"><span class="icon-left"></span>MIS Statement </a> </li>
                                <li class="top" id="ID63"><a href="/Admin/ManageUser/DeductionStatement" onclick="MenuSelect(50, 50, 56, 59);"><span class="icon-left"></span>Deduction Statement </a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="top" id="ID65"><a href="/Admin/ManageUser/Transaction" onclick="MenuSelect(50, 50, 50, 65);"><span class="icon-left"></span>Transaction</a> </li>

            </ul>
        </li>

          </ul>-->
       </nav>
       </div></div>
       <script >
     $(document).ready(function() {
     $(".nav a").click(function() {
       var link = $(this);
       var closest_ul = link.closest("ul");
       var parallel_active_links = closest_ul.find(".active1");
       var closest_li = link.closest("li");
       var link_status = closest_li.hasClass("active1");
       var count = 0;

       closest_ul.find("ul").slideUp("fast", function() {
         if (++count == closest_ul.find("ul").length)
           parallel_active_links.removeClass("active1");
       });

       if (!link_status) {
         closest_li.children("ul").slideDown("fast");
         closest_li.addClass("active1");
       }
     });
     });

  </script>