<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
<?php include 'common/header.php';?>
<?php include 'common/nav.php';?>
<!-- Offcanvas Navigation End -->
<div class="offcanvas-overlay"></div>
<!-- -------end-responsive-header------ -->
<div class="left-content">
<div class="mother-grid-inner">
<!--header start here-->
<?php include 'common/navbar.php';?>
<!--heder end here--> 

<!--inner block start here--> 

<!--market updates updates-->
<div class="inner-block">
<ol class="breadcrumb">
  <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
  <li class="active">Employees</li>
</ol>
<h3>Employee List</h3>
<div class="page-header">
<div class="main">
<!--button-->
<div class="main-content">
<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
<div class="dropdown" style="float:right; padding-right:5px">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" title="Change Status"><i class="fa fa-check"></i> <span class="caret"></span></button>
  <ul class="dropdown-menu" style="min-width:75px">
    <li><a onClick="return validatestatus('Active')">Active</a></li>
    <li><a onClick="return validatestatus('Inactive')">Inactive</a></li>
  </ul>
</div>
&nbsp;
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" value="Add Employee" style="margin-right: 5px; float:right;"><i class="fa fa-plus"></i></button>
<?php if($this->session->flashdata('msg')): ?>
<span style="color:green; padding-top:10px">
<center>
  <?php echo $this->session->flashdata('msg'); ?>
</center>
</span>
<?php endif; ?>
<?php // echo '<pre>';print_r($customers);?>
<form id="frm-example"  method="POST">
<table id="example" class="table table-striped table-bordered display select" cellspacing="0">
  <thead>
    <tr>
      <th width="5%"><!--<input type="checkbox" id="master">--></th>
      <th width="5%">S.No</th>
      <th>Name</th>
      <th>Country Calling Code</th>
      <th>Mobile</th>
      <th>Password</th>
      <th>Email</th>
      <th>Emp Code</th>
      <th>Designation</th>
      <th>Status</th>
      <th width="8%">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($employees)){
   	$i=1;
   foreach ($employees as $employee){
   	$id = $employee['id'];
   	$name = $employee['name'];
	$mobile = $employee['mobile'];
	$password = $employee['text_password'];
	$email = $employee['email'];
	$user_code = $employee['user_code'];
	$designation = $employee['designation'];
	$status = $employee['status'];
	$callingCode = $employee['callingCode'];
    ?>
    <tr class="gradeX check">
      <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
      <td><?php echo $i;?></td>
      <td><?php echo $name;?></td>
      <td><?php echo '+'.$callingCode;?></td>
      <td><?php echo $mobile;?></td>
      <td><?php echo $password;?></td>
      <td><?php echo $email;?></td>
      <td><?php echo $user_code;?></td>
      <td><?php echo $designation;?></td>
      <td id="stat<?php echo $id;?>"><a  class="btn" id="boundOnPageLoaded<?php echo $id;?>"><?php echo $status;?></a></td>
      <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModaledit<?php echo $id;?>"  onClick="openmodal('<?php echo $id;?>')"><i class="fa fa-edit"></i></button>
        <a onClick="return deleteemp('<?php echo $id;?>')" title="Delete" class="btn btn-info "  direction="right"><i class="fa fa-trash"></i></a></td>
    </tr>
    <script>
                  $("body").delegate("#boundOnPageLoaded<?php echo $id;?>", "click", function(){
//alert("boundOnPageLoaded Button Clicked")

	var url = "<?php echo base_url()."user/empchangestatus"?>";
	/*var status = '<?php echo $changestatus;?>';*/
	var getstat = $("#boundOnPageLoaded<?php echo $id;?>").text();
	
	var id = '<?php echo $id;?>';
	if(getstat=='Active'){
		var changestat = 'Inactive';
		}else{
		var changestat = 'Active';	
		}
		
	$.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id,
						status : changestat
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Employee status updated successfully", "success");
							
							
							
	                        setTimeout(function() {
	                            $('#stat'+id).html('<a class="btn" id="boundOnPageLoaded'+id+'">'+changestat+'</a>');
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Employee is not been updated", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	

});</script>
    <?php $i++;}}?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Employee</h4>
      </div>
      <div class="modal-body">
        <table class="horizontal-table" width="100%">
          <tbody>
            <tr>
              <th>Name<span style="color:red">* </span></th>
              <td><input class="form-control" id="name" name="name" required type="text" value="" />
                <div class="text-danger" id="name_error"></div></td>
            </tr>
            <tr>
              <th>Country Calling Code<span style="color:red">* </span></th>
              <td><select name="callingCode" id="callingCode" class="form-control">
                  <option value="">Select Country Calling Code</option>
                  <?php foreach($countrydata as $country){?>
                  <option value="<?php echo $country['callingCode'];?>"><?php echo '+'.$country['callingCode'].' '.$country['name'];?></option>
                  <?php }?>
                </select>
                <div class="text-danger" id="callingCode_error"></div></td>
            </tr>
            <tr>
              <th>Mobile<span style="color:red">* </span></th>
              <td><input class="form-control" id="mobile" name="mobile" required type="text" value="" pattern="[0-9]{10}" maxlength="10"/>
                <div class="text-danger" id="mobile_error"></div></td>
            </tr>
            <tr>
              <th>Password<span style="color:red">* </span></th>
              <td><input class="form-control" id="password" name="password" required type="password" value="" />
                <div class="text-danger" id="password_error"></div></td>
            </tr>
            <tr>
              <th>Email<span style="color:red">* </span></th>
              <td><input class="form-control" id="email" name="email" required type="email" value="" />
                <div class="text-danger" id="email_error"></div></td>
            </tr>
            <tr>
              <th>Employee Code</th>
              <td><input class="form-control" id="user_code" name="user_code"  type="text" value="" /></td>
            </tr>
            <tr>
              <th>Designation</th>
              <td><input class="form-control" id="designation" name="designation"  type="text" value="" /></td>
            </tr>
          </tbody>
        </table>
        <div class="modal-backdrop fade in" id="loadDiv" style="display: none;">
          <center>
            <div class="loadingmessage" style="padding-top:200px; color:#fff"><img
    src="<?php echo base_url();?>assets/img/ajax-loader.gif" /><br>
              Please wait...</div>
          </center>
        </div>
        <br>
        <button type="button" value="active" name="SubBtn" class="btn" onClick="saveemployee()">Add</button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade editmodal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Employee</h4>
      </div>
      <div class="modal-backdrop fade in" id="loadDiv2" style="display: none;">
        <center>
          <div class="loadingmessage" style="padding-top:200px; color:#fff"><img
    src="<?php echo base_url();?>assets/img/ajax-loader.gif" /><br>
            Please wait...</div>
        </center>
      </div>
      <div id="itemdetails" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--inner block end here--> 
<!--copy rights start here-->
<?php include 'common/footer.php';?>
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
<div class="clearfix"> </div>
</div>
<script>
		 function openmodal(id){
	    $('.editmodal').removeAttr('id');
	   $('.editmodal').attr('id','myModaledit'+id);
	     var url = "<?php echo base_url()."user/ajaxeditemployee"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModaledit'+id).modal('show');
	                   
	                }
	            });
	  
	   }     
		
       function saveemployee(){
		   var name = $('#name').val();
		   var callingCode = $('#callingCode').val();
		   var mobile = $('#mobile').val();
		   var password = $('#password').val();
		   var email = $('#email').val();
		   var user_code = $('#user_code').val();
		   var designation = $('#designation').val();
		   var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  if(name.length==0){
			$("#name_error").html("Name is Required Field.");
			return false;  
		  }else{
			$("#name_error").html("");  
			 }
			 if(callingCode.length==0){
			$("#callingCode_error").html("Country Calling Code is Required Field.");
			return false;  
		  }else{
			$("#callingCode_error").html("");  
			 }
         if(mobile.length==0){
			$("#mobile_error").html("Mobile is Required Field.");
			return false;
		 }else if(isNaN(mobile)){
			$("#mobile_error").html("Mobile must be a number.");
			return false; 
			
		  }else{
			  $("#mobile_error").html("");
			  }
			if(password.length==0){
			$("#password_error").html("Password is Required Field.");
			return false;  
		  }else{
			  $("#password_error").html("");
			  }
		if(email.length==0){
			$("#email_error").html("Email is Required Field.");
			return false; 
		  }else if(re.test(email)==false){
			$("#email_error").html("Invalid Email.");
			return false; 
		  }else{
			  $("#email_error").html("");
			  }
			 $("#loadDiv").show();
		   var url = "<?php echo base_url()."user/saveemployee"?>";
		   var adminRedirectUrl = "<?php echo base_url()."user/employees"?>";
		    $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    name: name,
						callingCode: callingCode,
						mobile: mobile,
						password: password,
						email: email,
						user_code: user_code,
						designation : designation						
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Employee is inserted successfully", "success");
							
	                        setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 2000);
	                    } else {
	                        swal("Error!", "Employee is not been inserted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
						$("#loadDiv").hide();
						$( ".close" ).trigger( "click" );
	                }
	            });
		   } 
	function updateemployee(){
		var id = $('#emp_id').val();
		   var name = $('#name_edit').val();
		   var callingCode = $('#callingCode_edit').val();
		   var mobile = $('#mobile_edit').val();
		   var password = $('#password_edit').val();
		   var email = $('#email_edit').val();
		   var user_code = $('#user_code_edit').val();
		   var designation = $('#designation_edit').val();
		   var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  if(name.length==0){
			$("#name_edit_error").html("Name is Required Field.");
			return false;  
		  }else{
			$("#name_edit_error").html("");  
			 }
			if(callingCode.length==0){
			$("#callingCode_edit_error").html("Country Calling Code is Required Field.");
			return false;  
		  }else{
			$("#callingCode_edit_error").html("");  
			 }
         if(mobile.length==0){
			$("#mobile_edit_error").html("Mobile is Required Field.");
			return false;
		 }else if(isNaN(mobile)){
			$("#mobile_edit_error").html("Mobile must be a number.");
			return false; 
			
		  }else{
			  $("#mobile_edit_error").html("");
			  }
			if(password.length==0){
			$("#password_edit_error").html("Password is Required Field.");
			return false;  
		  }else{
			  $("#password_edit_error").html("");
			  }
		if(email.length==0){
			$("#email_edit_error").html("Email is Required Field.");
			return false; 
		  }else if(re.test(email)==false){
			$("#email_edit_error").html("Invalid Email.");
			return false; 
		  }else{
			  $("#email_edit_error").html("");
			  }
		   var url = "<?php echo base_url()."user/updateemployee"?>";
		   //var adminRedirectUrl = "<?php echo base_url()."user/employees"?>";
		    $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
						id: id,
	                    name: name,
						callingCode: callingCode,
						mobile: mobile,
						password: password,
						email: email,
						user_code: user_code,
						designation: designation						
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Employee is updated successfully", "success");
							 var t = $('#example').DataTable();
t.cell('.selected', ':eq(2)').data(name);
t.cell('.selected', ':eq(3)').data("+"+callingCode);
t.cell('.selected', ':eq(4)').data(mobile);
t.cell('.selected', ':eq(5)').data(password);
t.cell('.selected', ':eq(6)').data(email);
t.cell('.selected', ':eq(7)').data(user_code);
t.cell('.selected', ':eq(8)').data(designation);
t.draw();
t.row( { selected: true } ).deselect();
$( ".close" ).trigger( "click" );
	                       /* setTimeout(function() {
	                          
	                        }, 2000);*/
	                    } else {
	                        swal("Error!", "Employee is not been updated", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
						$('#myModaledit'+id).modal('hide');
						$("#loadDiv2").hide();
						$( ".close" ).trigger( "click" );
	                }
	            });
		   } 

function deleteemp(id){

	 var res = confirm("Are you sure you want to delete this Employee");
	    if(res == true) {
	            var url = "<?php echo base_url()."user/deleteemployee"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Employees are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else {
	                        swal("Error!", "Employees are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
function validate(){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	 
	 var res = confirm("Are you sure you want to delete "+countid+" Employees");
	    if(res == true) {
	            var url = "<?php echo base_url()."user/deleteemployee"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Employees are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else {
	                        swal("Error!", "Employees are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
 function validatestatus(status){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
		  var chkarr = checkedid.split(" ");
		  
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	
	 var res = confirm("Are you sure you want to update status "+countid+" Employees");
	    if(res == true) {
	            var url = "<?php echo base_url()."user/empchangestatus"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid,
						status : status
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Employees status updated successfully", "success");
	                       setTimeout(function() {
							   for (i = 0; i < countid; i++) {
								  var id = favorite[i];
								  $('#stat'+id).html('<a class="btn" id="boundOnPageLoaded'+id+'">'+status+'</a>');
                               }
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Employees are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
  	

$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {
                     columns: [ 1, 2, 3,4,5,6,7,8,9 ]
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Employee-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                    columns: [ 1, 2, 3,4,5,6,7,8,9 ]
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Employee-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {
                    columns: [ 1, 2, 3,4,5,6,7,8,9 ]
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Employee-'+date
            }
        ]
		}
		]
		
   });

});

$(document).ready(function(){
	var fruits = [<?php foreach($employees as $employee){?>"<?php echo $employee['id'];?>",<?php }?>];
	
	//var fk = fruits.values();
	//var fkey = fruits.keys();
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
	 	
	
 
  
});



   // Handle form submission event
  
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
