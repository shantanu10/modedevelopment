<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
<?php include 'common/header.php';?>
<?php include 'common/nav.php';?>
<!-- Offcanvas Navigation End -->
<div class="offcanvas-overlay"></div>
<!-- -------end-responsive-header------ -->
<div class="left-content">
<div class="mother-grid-inner">
<!--header start here-->
<?php include 'common/navbar.php';?>
<!--heder end here--> 

<!--inner block start here--> 

<!--market updates updates-->
<div class="inner-block">
<ol class="breadcrumb">
  <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
  <li class="active">Order Type</li>
</ol>
<h3>Order Type List</h3>
<div class="page-header">
<div class="main">
<!--button-->
<div class="main-content">
<!--<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
<div class="dropdown" style="float:right; padding-right:5px">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" title="Change Status"><i class="fa fa-check"></i> <span class="caret"></span></button>
  <ul class="dropdown-menu" style="min-width:75px">
    <li><a onClick="return validatestatus('Active')">Active</a></li>
    <li><a onClick="return validatestatus('Inactive')">Inactive</a></li>
  </ul>
</div>-->
<?php if($this->session->flashdata('msg')): ?>
<span style="color:green; padding-top:10px">
<center>
  <?php echo $this->session->flashdata('msg'); ?>
</center>
</span>
<?php endif; ?>
<table id="example" class="table table-striped table-bordered display select" cellspacing="0">
  <thead>
    <tr>
      <th width="10%">S.No</th>      
      <th>Name</th>
      <th>Images</th>
     <th>Upload Image</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($ordertypes)){
   	$i=1;
   foreach ($ordertypes as $type){
   	$id = $type['id'];
	$name = $type['name'];
	
    ?>
    <tr class="gradeX check">
      <td><?php echo $i;?></td>
      <td><?php echo $name;?></td>
       <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModalimage<?php echo $id;?>"  onClick="openmodalimg('<?php echo $id;?>')"><i class="fa fa-eye"></i></button></td>
      <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModaledit<?php echo $id;?>"  onClick="openmodal('<?php echo $id;?>')"><i class="fa fa-upload"></i></button>
      </td>
    </tr>
    
    <?php $i++;}}?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<div class="modal fade editmodal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Images</h4>
      </div>
      <div class="modal-backdrop fade in" id="loadDiv" style="display: none;">
    <center><div class="loadingmessage" style="padding-top:200px; color:#fff"><img
    src="<?php echo base_url();?>assets/img/ajax-loader.gif" /><br>Please wait...</div>
    </center>
    </div>
      <div id="itemdetails" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade editmodalimg" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Images</h4>
      </div>
      <div id="itemdetailsimg" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--inner block end here--> 
<!--copy rights start here-->
<?php include 'common/footer.php';?>
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
<div class="clearfix"> </div>
</div>

<script>
	 function openmodalimg(id){
	    $('.editmodalimg').removeAttr('id');
	   $('.editmodalimg').attr('id','myModalimage'+id);
	     var url = "<?php echo base_url()."Dashboard/ajaxordertypeimages"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetailsimg").html(data);
					 $('#myModalimage'+id).modal('show');
	                   
	                }
	            });
	  
	   }
	function openmodal(id){
	    $('.editmodal').removeAttr('id');
	   $('.editmodal').attr('id','myModaledit'+id);
	     var url = "<?php echo base_url()."Dashboard/ajaxeditordertypeimage"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModaledit'+id).modal('show');
	                   
	                }
	            });
	  
	   }     
		
	function updateimages(){
		
		var form_data = new FormData();		
        var totalfiles = document.getElementById('files_edit').files.length;
       for (var index = 0; index < totalfiles; index++) {
      form_data.append("image[]", document.getElementById('files_edit').files[index]);
     }
	  
      form_data.append("id", $('#id').val());  
	  $("#loadDiv").show(); 
		   var url = "<?php echo base_url()."Dashboard/updateordertypeimage"?>";
		     $.ajax({
	                type: "POST",
	                url: url,
	                data: form_data,	                
	                cache: false,
					contentType: false,
                    processData: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Images are updated successfully", "success");
							
							 /*var t = $('#example').DataTable();
t.cell('.selected', ':eq(2)').data($('#content_type_edit').val());
if($('#itemId_edit').val()==''){
t.cell('.selected', ':eq(3)').data($('#stageId_edit').val());
}else{
t.cell('.selected', ':eq(3)').data($('#itemId_edit').val());	
}
t.draw();
t.row( { selected: true } ).deselect();*/
	                       /* setTimeout(function() {
	                          
	                        }, 2000);*/
	                    } else {
						var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Images are not been updated", "error");

	                    }
						$("#loadDiv").hide();
						$( ".close" ).trigger( "click" );
	                }
	            });
		   } 

$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {
                    columns: [ 0,1 ]
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Order Type-'+date
            },
			 {
                extend: 'csvHtml5',
				charset: "utf-8",
                bom: "true",
				exportOptions: {
                    columns: [ 0,1]
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Order Type-'+date
            },
            {
                extend: 'pdfHtml5',
				charset: "utf-8",
                bom: true,
				exportOptions: {
                    columns: [ 0,1 ]
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Order Type-'+date
            }
        ]
		}
		]
		
   });

});
function Filevalidationedit(){
        const fi = document.getElementById('files_edit');		
        // Check if any file is selected. 
	  var filePath = fi.value; 
          
            // Allowing file type 
            var allowedExtensions =  
                    /(\.jpg|\.jpeg|\.png|\.gif)$/i; 
              
            if (!allowedExtensions.exec(filePath)) { 
                alert('Invalid file type. Allowed files are jpg,jpeg,gif');    
				 fi.value = '';            
                return false; 
            }
        if (fi.files.length > 0) { 
		
            for (const i = 0; i <= fi.files.length - 1; i++) { 
  
                const fsize = fi.files.item(i).size; 
                const file = Math.round((fsize / 1024)); 
				
                // The size of the file. 
                if (file >= 10240) { 
                    alert( 
                      "File too Big, please select a file less than 10mb"); 
                } /*else if (file < 2048) { 
                    alert( 
                      "File too small, please select a file greater than 2mb"); 
                } else { 
                    document.getElementById('size').innerHTML = '<b>'
                    + file + '</b> KB'; 
                } */
            } 
        } 
    } 
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
