<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
<title>MODE</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/layout.css">
<!--js-->
<script src="<?php echo base_url();?>assets/js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--static chart-->
</head>
<body>
<style>
body {
   /*  background: linear-gradient(to right, rgb(75, 108, 183), rgb(24, 40, 72)); */
   /* background: #B6091C;*/ /* fallback for old browsers */
  /*background: -webkit-linear-gradient(to left, #B6091C, #8e888b, #B6091C);*/ /* Chrome 10-25, Safari 5.1-6 */
  /*background: linear-gradient(to left, #B6091C, #8e888b, #B6091C);*/ /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  
     background: #2C313A; /* fallback for old browsers */
  background: -webkit-linear-gradient(to left, #2C313A, #2C313A, #2C313A); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to left, #2C313A, #2C313A, #2C313A); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  
    font-size: 12px;
}
</style>
<div class="login-page">
  <div class="login-main">
    <div class="login-head"><img src="<?php echo base_url();?>assets/img/logo.png" height="50%" width="50%"> </div>
    <center><div style="color:#ff0000">
      <?php 
	
	if($this->session->flashdata('errorlogin')) {?>
      <?php echo addslashes($this->session->flashdata('errorlogin')); ?>
      <?php } ?>
    </div>
    </center>
    <div class="forget_pass login-block" style="display: none;">
      <form action="<?php echo base_url(); ?>login/forget_pass" method="post">
        <div>
          <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email To Send Password Link" required autofocus>
        </div>
        <div class="boxes"> <a href="javascript:void(0);" class="display-block text-center m-t-md text-sm" onclick="check_div('login');">Log in to Account</a> </div>
        <input type="submit" name="Sign In" value="Submit">
      </form>
      <!-- <h5><a href="index.html">Go Back to Home</a></h5> --> 
    </div>
    <div class="login-block" id="login_div">
      <form action="<?php echo base_url(); ?>login/login_to_system" method="post">
        <div>
          <label>User ID</label>
          <i class="fa fa-user"></i>
          <input class="form-control" id="login_email" name="login_email" placeholder="Email" type="email" value="<?php if (get_cookie('login_email')) { echo get_cookie('login_email'); } ?>" required />
        </div>
        <div>
          <label>Password <a style="float: right; font-weight: normal;" onclick="check_div('forget');">Forgot your password?</a></label>
          <i class="fa fa-lock"></i>
          <input class="form-control" id="login_password" name="login_password" placeholder="Password" type="password" value="<?php if (get_cookie('login_password')) { echo get_cookie('login_password'); } ?>" required />
          <button style="position:absolute; top: 31px;" class="unmask1" type="button" title="Show/Hide Password"><i class="fa fa-fw fa-eye fa-eye-slash" style="position:relative; left:-3px; top:0; color:#fff"></i></button>
        </div>
        <div class="boxes">
          <input type="checkbox" name="remember" id="remember" <?php if (get_cookie('login_email')) { ?> checked="checked" <?php } ?>>
          <label for="remember">Remember me</label>
        </div>
        <input type="submit" name="Sign In" value="Login">
      </form>
      <!-- <h5><a href="index.html">Go Back to Home</a></h5> --> 
    </div>
  </div>
</div>
<!--inner block end here--> 
<!--copy rights start here--> 
<!-- <div class="copyrights">
         <p>© 2016 Shoppy. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
    </div>	 --> 
<!--COPY rights end here--> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<!--//scrolling js--> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<!-- mother grid end here--> 
<script type="text/javascript">
    $(document).ready(function() {
	
	$('.unmask1').on('click', function(){
	

  if($(this).prev('input').attr('type') == 'password')
    changeType($(this).prev('input'), 'text');
	

  else
    changeType($(this).prev('input'), 'password');

  return false;
});   
   });    
        function check_div(param){
            if(param == "login") {
                $(".forget_pass").hide();
                $("#login_div").show();
            }else{
                $(".forget_pass").show();
                $("#login_div").hide();
            }

        }
        $("#fg_button").click(function(){
            var base_url = '<?php echo base_url(); ?>';
            $.post(base_url+'login/forget_pass',{email:$("#email").val()},function(response){
                console.log(response);
                if(response == "success") {
                   /* setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: false,
                        showMethod: 'fadeIn',
                        hideMethod: 'fadeOut',
                        timeOut: 20000,
                        positionClass: "toast-top-center",
                    };
                        toastr.success('Reset password link sent to your email id.');
                    }, 100);*/
                    setTimeout(function(){window.location.href='<?php echo base_url(); ?>'},4000);
                }
                else {
                   /* setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        progressBar: false,
                        showMethod: 'fadeIn',
                        hideMethod: 'fadeOut',
                        timeOut: 20000,
                        positionClass: "toast-top-center",
                    };
                        toastr.error('Could not find your email ! Please enter correct email id.');
                    }, 100);*/

                }


            });
        });
               </script>
               <script>

/*
  Switch actions
*/
 function changeType(x, type) {
	 if(type=='text'){
	$(".fa-eye").removeClass('fa-eye-slash');
	 }else{
	$(".fa-eye").addClass('fa-eye-slash');	 
		}
  if(x.prop('type') == type)
  return x; //That was easy.
  try {
    return x.prop('type', type); //Stupid IE security will not allow this
  } catch(e) {
    //Try re-creating the element (yep... this sucks)
    //jQuery has no html() method for the element, so we have to put into a div first
    var html = $("<div>").append(x.clone()).html();
    var regex = /type=(\")?([^\"\s]+)(\")?/; //matches type=text or type="text"
    //If no match, we add the type attribute to the end; otherwise, we replace
    var tmp = $(html.match(regex) == null ?
      html.replace(">", ' type="' + type + '">') :
      html.replace(regex, 'type="' + type + '"') );
    //Copy data from old element
    tmp.data('type', x.data('type') );
    var events = x.data('events');
    var cb = function(events) {
      return function() {
            //Bind all prior events
            for(i in events)
            {
              var y = events[i];
              for(j in y)
                tmp.bind(i, y[j].handler);
            }
          }
        }(events);
        x.replaceWith(tmp);
    setTimeout(cb, 10); //Wait a bit to call function
    return tmp;
  }
}
</script>
<style>
.show-password {
	width: 100%;
}
.password-wrapper {
	position: absolute;
}
.unmask1 {
	position: relative;
	right: 10px;
	top: 57%;
	width: 25px;
	height: 25px;
	background: #2C313A;
	border-radius: 50%;
	cursor: pointer;
	border: none;
	-webkit-appearance: none;
}
.password + .unmask1:before {
	content: "";
	position: absolute;
	top: 4px;
	left: 4px;
	width: 17px;
	height: 17px;
	background: #e3e3e3;
	z-index: 0;
	border-radius: 50%;
}
.password[type="text"] + .unmask1:after {
	content: "";
	position: absolute;
	top: 6px;
	left: 6px;
	width: 13px;
	height: 13px;
	background: #aaa;
	z-index: 2;
	border-radius: 50%;
}
.login-block input[type="text"]{font-size: 0.9em;
    padding: 7px 20px !important;;
    width: 100%;
    color: #676a6c !important;
    height: 38px;
    outline: none !important;;
    border: 1px solid #D3D3D3;
    box-shadow: none;
    border-radius: 3px;
    /* -ms-border-radius: 5px; */
    /* -moz-border-radius: 5px; */
    /* -o-border-radius: 5px; */
    background: #F5F5F5;
    margin: 0em 0em 1.5em 0em !important;;
    padding-left: 28px !important;}
</style>
</body>
</html>
