<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
  <?php include 'common/header.php';?>
  <?php include 'common/nav.php';?>
  <!-- Offcanvas Navigation End -->
  <div class="offcanvas-overlay"></div>
  <!-- -------end-responsive-header------ -->
  <div class="left-content">
    <div class="mother-grid-inner"> 
      <!--header start here-->
      <?php include 'common/navbar.php';?>
      <!--heder end here--> 
      
      <!--inner block start here--> 
      
      <!--market updates updates-->
      <div class="inner-block">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
          <li class="active">Orders</li>
        </ol>
        <h3>Sales Order List</h3>
        <div class="page-header">
          <div class="main"> 
            <!--button-->
            <div class="main-content"> <a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
              <div class="dropdown" style="float:right; padding-right:5px">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" title="Change Status"><i class="fa fa-check"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right">
                  <?php foreach($sales_order_stages as $stages){?>
                  <li><a onClick="return validatestatus('<?php echo $stages['stageId'];?>','<?php echo $stages['name'];?>')"><?php echo $stages['name'];?></a></li>
                  <?php }?>
                </ul>
              </div>
              <?php if($this->session->flashdata('msg')): ?>
              <span style="color:green; padding-top:10px">
              <center>
                <?php echo $this->session->flashdata('msg'); ?>
              </center>
              </span>
              <?php endif; ?>
              <table id="example" class="table table-striped table-bordered display select" cellspacing="0">
                <thead>
                  <tr>
                    <th width="5%"><input type="checkbox" id="master"></th>
                    <th width="5%">S.No</th>
                    <th>Sales Order Id</th>
                    <th>Order Number</th>
                    <th>Order Type</th>
                    <th>Order Date</th>
                    <th>Order Due Date</th>
                    <th>customer Code</th>
                    <th>Customer Name</th>
                    <th>Stage</th>
                    <th>Vat Amount</th>
                    <th>Order Total</th>
                    <th>Order Items</th>
                    <th>Payment Transaction</th>
                    <!--<th>Action</th>--> 
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($orders)){
   	$i=1;
   foreach ($orders as $order){
   	$id = $order['id'];
   	$salesOrderId = $order['salesOrderId'];
	$orderNumber = $order['orderNumber'];
	$orderDate = $order['orderDate'];
	$orderDueDate = $order['orderDueDate'];
	$customerCode = $order['customerCode'];
	$customerName = $order['customerName'];
	$stageId = $order['stageId'];
	
	$vatAmount = $order['vatAmount'];
	$orderTotal = $order['orderTotal'];
	$orderTypeId = $order['orderTypeId'];
	 $wheretype = "id='".$orderTypeId."'";
     $typename = $this->admin_model->getDataById('order_type',$wheretype);
	 $where = "stageId='".$stageId."'";
     $status = $this->admin_model->getDataById('sales_order_stages',$where); 
	 if($stageId > 7 && $stageId < 14){
	 
		  $wherefitting = "order_id='".$id."'";
     $fittingstage = $this->admin_model->getDataById('order_fitting',$wherefitting);
	
	 if(!empty($fittingstage)){
	 $wherefit = "stageId='".$fittingstage[0]['stageId']."'";
     $statusfitting = $this->admin_model->getDataById('sales_order_stages',$wherefit);
	 $statusdata = $status[0]['name'].' , '.$statusfitting[0]['name'];
	 }
	 }else{
	$statusdata = $status[0]['name'];
	}
    ?>
                  <tr class="gradeX">
                    <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
                    <td><?php echo $i;?></td>
                    <td><?php echo $salesOrderId;?></td>
                    <td><?php echo $orderNumber;?></td>
                     <td><?php echo $typename[0]['name'];?></td>
                    <td><?php echo $orderDate;?></td>
                    <td><?php echo $orderDueDate;?></td>
                    <td><?php echo $customerCode;?></td>
                    <td><?php echo $customerName;?></td>
                    <td id="stat<?php echo $id;?>" style="font-size:10px;"><?php echo $statusdata;?></td>
                    <td><?php echo $vatAmount;?></td>
                    <td><?php echo $orderTotal;?></td>
                    <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?php echo $orderNumber;?>"  onClick="openmodal('<?php echo $orderNumber;?>')"><i class="fa fa-eye"></i></button>
                      <!--<a href='<?php echo base_url()?>order/items/<?php echo $orderNumber;?>' tooltip="View Items"  direction="right" class=" tooltip edit"></a> 
                    --></td>
                    <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalpayment<?php echo $id;?>"  onClick="openmodalpayment('<?php echo $id;?>')"><i class="fa fa-history"></i></button>
                   
                   </td>
                  </tr>
                  
                  <!-- Modal --> 
                  
                  <script>
                  $("body").delegate("#boundOnPageLoaded<?php echo $id;?>", "change", function(){
//alert("boundOnPageLoaded Button Clicked")

	var url = "<?php echo base_url()."order/changestatus"?>";
	/*var status = '<?php echo $changestatus;?>';*/
	var getstat = $("#boundOnPageLoaded<?php echo $id;?>").val();
	
	var id = '<?php echo $id;?>';
	
		
	$.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id,
						status : getstat
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Order status updated successfully", "success");						
							
							
	                        setTimeout(function() {	                            
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Order are not been updated", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	

});</script>
                  <?php $i++;}}?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:auto"> 
          
          <!-- Modal content-->
          <div class="modal-content" >
            <div id="itemdetails"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade paymentmodal" role="dialog">
        <div class="modal-dialog" style="width:auto"> 
          
          <!-- Modal content-->
          <div class="modal-content" >
            <div id="paymentdetails"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!--inner block end here--> 
      <!--copy rights start here-->
      <?php include 'common/footer.php';?>
      <!--COPY rights end here--> 
    </div>
  </div>
  <!--slider menu-->
  <div class="clearfix"> </div>
</div>
 
<script>
   function openmodal(docnum){
	    $('.modal').removeAttr('id');
	   $('.modal').attr('id','myModal'+docnum);
	     var url = "<?php echo base_url()."order/ajaxitems"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: docnum
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModal'+docnum).modal('show');
	                   
	                }
	            });
	  
	   }     	
	function openmodalpayment(id){
	    $('.paymentmodal').removeAttr('id');
	   $('.paymentmodal').attr('id','myModalpayment'+id);
	     var url = "<?php echo base_url()."order/ajaxpaymentTransaction"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#paymentdetails").html(data);
					 $('#myModalpayment'+id).modal('show');
	                   
	                }
	            });
	  
	   } 	
	
function validate(){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	 
	 var res = confirm("Are you sure you want to delete "+countid+" Sales Order");
	    if(res == true) {
	            var url = "<?php echo base_url()."order/deleteorder"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Sales Order are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else if(data==2) {
	                        swal("Error!", "Before delete this orders please delete their sales order items", "error");
						}else {
	                        swal("Error!", "Sales Order are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
 function validatestatus(status,name){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
		  var chkarr = checkedid.split(" ");
		  
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	
	 var res = confirm("Are you sure you want to update status "+countid+" Sales Order");
	    if(res == true) {
	            var url = "<?php echo base_url()."order/changestatus"?>";
	          var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid,
						status : status
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Order status updated successfully", "success");
	                       setTimeout(function() {
							   for (i = 0; i < countid; i++) {
								  var id = favorite[i];
								$('#stat'+id).text(name);
								  //$('#boundOnPageLoaded'+id+' option[value='+status+']').attr("selected", "selected");
     						 
                               }
	                        }, 1000);
	                    } else if(data==2) {
	                        swal("Error!", "Order status are not been updated with over step or back step", "error");
	                     
	                    }else {
	                        swal("Error!", "Order status are not been updated", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
  	

$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {                  
				 columns: [ 1,2, 3,4,5,6,7,8,9,10,11 ]	               
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Sales Order-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                  columns: [ 1,2, 3,4,5,6,7,8,9,10,11 ]	               
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Sales Order-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {                  
				  columns: [ 1,2, 3,4,5,6,7,8,9,10,11 ]
	               
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Sales Order-'+date
            }
        ]
		}
		]
		
   });

});

$(document).ready(function(){
	var fruits = [<?php foreach($orders as $order){?>"<?php echo $order['id'];?>",<?php }?>];
	
	//var fk = fruits.values();
	//var fkey = fruits.keys();
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
});



   // Handle form submission event
  
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
