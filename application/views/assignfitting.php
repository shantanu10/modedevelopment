<?php if(!empty($assigndetails)){
	?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">Assigned to Fitter</h4>
</div>
<table class="horizontal-table" width="100%">
  <tbody>
    <tr>
      <th>Order Number</th>
      <td><?php echo $orderNumber;?></td>
    </tr>
    <tr>
      <th>Fitter</th>
      <td><?php  $where = "id = '".$assigndetails[0]['user_id']."'";
    $fitter = $this->admin_model->getDataById('users',$where);
	echo $fitter[0]['name'];
	?></td>
    </tr>
    <tr>
      <th>Assign Date</th>
      <td><?php echo $assigndetails[0]['assign_date'];?></td>
    </tr>
    <tr>
      <th>Expected Fitting Date</th>
      <td><?php echo $assigndetails[0]['fitting_date'];?></td>
    </tr>
  </tbody>
</table>
<?php }else{?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">This order is not assigned to fitter.</h4>
</div>
<?php }?>
