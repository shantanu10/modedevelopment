
<table class="horizontal-table" width="100%">
  <tbody>
    <tr>
      <th>Name<span style="color:red">* </span></th>
      <td><input class="form-control" id="name_edit" name="name" required type="text" value="<?php echo $emps[0]['name'];?>" />
        <div class="text-danger" id="name_edit_error"></div></td>
    </tr>
    <tr>
      <th>Country Calling Code<span style="color:red">* </span></th>
      <td><select name="callingCode" id="callingCode_edit" class="form-control">
          <option value="">Select Country Calling Code</option>
          <?php foreach($countrydata as $country){?>
          <option value="<?php echo $country['callingCode'];?>" <?php if($country['callingCode'] == $emps[0]['callingCode']){?> selected="selected"<?php }?>><?php echo '+'.$country['callingCode'].' '.$country['name'];?></option>
          <?php }?>
        </select>
        <div class="text-danger" id="callingCode_edit_error"></div></td>
    </tr>
    <tr>
      <th>Mobile<span style="color:red">* </span></th>
      <td><input class="form-control" id="mobile_edit" name="mobile" required type="text" value="<?php echo $emps[0]['mobile'];?>" pattern="[0-9]{10}" maxlength="10"/>
        <div class="text-danger" id="mobile_edit_error"></div></td>
    </tr>
    <tr>
      <th>Password<span style="color:red">* </span></th>
      <td><input class="form-control" id="password_edit" name="password" required type="password" value="<?php echo $emps[0]['text_password'];?>" />
        <div class="text-danger" id="password_edit_error"></div></td>
    </tr>
    <tr>
      <th>Email<span style="color:red">* </span></th>
      <td><input class="form-control" id="email_edit" name="email" required type="email" value="<?php echo $emps[0]['email'];?>" />
        <div class="text-danger" id="email_edit_error"></div></td>
    </tr>
    <tr>
      <th>User Code</th>
      <td><input class="form-control" id="user_code_edit" name="user_code"  type="text" value="<?php echo $emps[0]['user_code'];?>" /></td>
    </tr>
    <tr>
      <th>Designation</th>
      <td><input class="form-control" id="designation_edit" name="user_code"  type="text" value="<?php echo $emps[0]['designation'];?>" /></td>
    </tr>
  </tbody>
</table>
<br>
<input type="hidden" name="id" id="emp_id" value="<?php echo $emps[0]['id'];?>" />
<button type="button" value="active" name="SubBtn" class="btn" onClick="updateemployee()">Update</button>
