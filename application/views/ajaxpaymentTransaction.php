<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Transaction History (Order Number : <?php echo $orderNumber;?>)</h4>
      </div>
      <div class="modal-body">
              
        <table id="examplepayment" class="table table-striped table-bordered display select" cellspacing="0">
                <thead>
                  <tr>
                   <th width="12%"><input type="checkbox" id="master"></th>
                    <th>S.No</th>                   
                    <th>Order Number</th>
                    <th>Customer Name</th> 
                    <th>Customer Code</th>                
                    <th>Order Amount</th>
                     <th>Paid Amount</th>
                      <th>Ref. No</th>
                       <th>Payment Date</th>
                        <th>Payment Status</th>
                   
                    
                    <!--<th>Action</th>-->
                  </tr>
                </thead>
                <tbody>
              
                  <?php if (!empty($transactions)){
   	$j=1;
   foreach ($transactions as $transaction){
   	$id = $transaction['id'];
   	$customer_id = $transaction['customer_id'];
	$customer = $this->db->select('name,foreignName,customerCode')->from('customers')->where('id',$customer_id)->get();
	$customerdata = $customer->result_array('array');
	$name = $customerdata[0]['name'];
	$customerCode = $customerdata[0]['customerCode'];
	$order_id = $transaction['order_id'];
	$order = $this->db->select('orderNumber')->from('sales_orders')->where('id',$order_id)->get();
	$orderdata = $order->result_array('array');
	$orderNumber = $orderdata[0]['orderNumber'];
	$orderAmount = $transaction['orderAmount'];
	$paidAmount = $transaction['paidAmount'];
	$referenceNo = $transaction['referenceNo'];
	$paymentDate = $transaction['paymentDate'];
	$paymentStatus = $transaction['paymentStatus'];
    ?>
                  <tr class="gradeX">
                  <td><input type="checkbox" class="sub_chk" name="paymentchk[]" id="paymentchk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
                    <td><?php echo $j;?></td>
                    <td><?php echo $orderNumber;?></td>
                      <td><?php echo $name;?></td>
                    <td><?php echo $customerCode;?></td>                     
                       <td><?php echo $orderAmount;?></td>
                        <td><?php echo $paidAmount;?></td>
                       <td><?php echo $referenceNo;?></td>
                        <td><?php echo $paymentDate;?></td>
                         <td><?php echo $paymentStatus;?></td>
                  </tr>
                  <?php $j++;}}else{?>
					  
					  <tr><td colspan="10" align="center">No transaction here.</td></tr>
                       <?php }?>
                </tbody>
              </table>
      </div>
<script>
$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#examplepayment').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {                  
				 columns: [ 1,2, 3,4,5,6,7,8,9 ]	               
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Payment Transaction(Order Number:<?php echo $orderNumber;?>)-'+date
            },
			 {
                extend: 'csvHtml5',
				charset: "utf-8",
                bom: "true",
				exportOptions: {
                  columns: [ 1,2, 3,4,5,6,7,8,9 ]	               
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Payment Transaction(Order Number:<?php echo $orderNumber;?>)-'+date
            },
            {
                extend: 'pdfHtml5',
				charset: "utf-8",
                bom: "true",
				exportOptions: {                  
				  columns: [ 1,2, 3,4,5,6,7,8,9 ]
	               
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Payment Transaction(Order Number:<?php echo $orderNumber;?>)-'+date
            }
        ]
		}
		]
		
   });

});
$(document).ready(function(){
	var items = [<?php foreach($transactions as $transaction){?>"<?php echo $transaction['id'];?>",<?php }?>];
	
		$('#examplepayment .dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(items[i]).attr('name' , 'paymentchk[]').attr('id' , 'paymentchk' + items[i]).wrap('<label></label>').closest('label');  
        });
});

</script>
