<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
<?php include 'common/header.php';?>
<?php include 'common/nav.php';?>
<!-- Offcanvas Navigation End -->
<div class="offcanvas-overlay"></div>
<!-- -------end-responsive-header------ -->
<div class="left-content">
<div class="mother-grid-inner">
<!--header start here-->
<?php include 'common/navbar.php';?>
<!--heder end here--> 

<!--inner block start here--> 

<!--market updates updates-->
<div class="inner-block">
<ol class="breadcrumb">
  <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
  <li class="active">Contracts</li>
</ol>
<h3>Contract List</h3>
<div class="page-header">
<div class="main">
<!--button-->
<div class="main-content">
<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
<!--<div class="dropdown" style="float:right; padding-right:5px">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" title="Change Status"><i class="fa fa-check"></i> <span class="caret"></span></button>
  <ul class="dropdown-menu" style="min-width:75px">
    <li><a onClick="return validatestatus('Active')">Active</a></li>
    <li><a onClick="return validatestatus('Inactive')">Inactive</a></li>
  </ul>
</div>-->
&nbsp;
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" value="" style="margin-right: 5px; float:right;"><i class="fa fa-plus"></i></button>

<?php if($this->session->flashdata('msg')): ?>
<span style="color:green; padding-top:10px">
<center>
  <?php echo $this->session->flashdata('msg'); ?>
</center>
</span>
<?php endif; ?>
<form id="frm-example"  method="POST">
<table id="example" class="table table-striped table-bordered display select" cellspacing="0">
  <thead>
    <tr>
      <th width="5%"><!--<input type="checkbox" id="master">--></th>
      <th width="10%">S.No</th>
      <th>Order Number</th>
       <th>Contract</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($contracts)){
   	$i=1;
   foreach ($contracts as $contract){
   	$id = $contract['id'];
   	$orderNumber = $contract['orderNumber'];
	$contractfile = $contract['contract'];
    ?>
    <tr class="gradeX check">
      <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
      <td><?php echo $i;?></td>
      <td><?php echo $orderNumber;?></td>
       <td><a href="<?php echo base_url();?>uploads/contract/<?php echo $contractfile;?>" target="_blank"><?php echo $contractfile;?></a></td>

      <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModaledit<?php echo $id;?>"  onClick="openmodal('<?php echo $id;?>')"><i class="fa fa-edit"></i></button>
        <a onClick="return deleteemp('<?php echo $id;?>')" title="Delete" class="btn btn-info "  direction="right"><i class="fa fa-trash"></i></a></td>
    </tr>
    
    <?php $i++;}}?>
  </tbody>
</table>
</div>
</div>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Contract</h4>
      </div>
      <div class="modal-body">
      <form method='post' action='' enctype="multipart/form-data">
        <table class="horizontal-table" width="100%">
          <tbody>
            
           <tr>
              <th>Order<span style="color:red">* </span></th>
              <td><select name="order_id" id="order_id" class="form-control" >
               <option value="">Select</option>
              <?php foreach($orders as $order){?>
              <option value="<?php echo $order['id'];?>"><?php echo $order['orderNumber'];?></option>
              <?php }?>
              </select>
                <div class="text-danger" id="order_error"></div></td>
            </tr>
            
             <tr>
              <th>Contract<span style="color:red">* </span></th>
              <td><input type="file" id='files' name="contract"  required onChange="Filevalidation()">
                <div class="text-danger" id="file_error"></div></td>
            </tr>
          </tbody>
        </table>
        <br>
        <button type="button" value="active" name="SubBtn" class="btn" onClick="saveemployee()">Add</button>
        </form>
        
    <div class="modal-backdrop fade in" id="loadDiv" style="display: none;">
    <center><div class="loadingmessage" style="padding-top:200px; color:#fff"><img
    src="<?php echo base_url();?>assets/img/ajax-loader.gif" /><br>Please wait...</div>
    </center>
    </div>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade editmodal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Contract</h4>
      </div>
      <div class="modal-backdrop fade in" id="loadDiv2" style="display: none;">
    <center><div class="loadingmessage" style="padding-top:200px; color:#fff"><img
    src="<?php echo base_url();?>assets/img/ajax-loader.gif" /><br>Please wait...</div>
    </center>
    </div>
      <div id="itemdetails" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--inner block end here--> 
<!--copy rights start here-->
<?php include 'common/footer.php';?>
<!--COPY rights end here-->
</div>

</div>
<!--slider menu-->
<div class="clearfix"> </div>
</div>
<script>

	function openmodal(id){
	    $('.editmodal').removeAttr('id');
	   $('.editmodal').attr('id','myModaledit'+id);
	     var url = "<?php echo base_url()."Dashboard/ajaxeditcontract"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModaledit'+id).modal('show');
	                   
	                }
	            });
	  
	   }     
		
       function saveemployee(){
		   var form_data = new FormData();
		
		   var order_id = $('#order_id').val();
		  
		  if(order_id.length==0){
			$("#order_error").html("Order is Required Field.");
			return false;  
		  }else{
			$("#order_error").html("");  
			 }
			  var totalfiles = document.getElementById('files').files.length;
			 if(totalfiles==0){
			$("#file_error").html("Contract is Required Field.");
			return false;  
		  }else{
			$("#file_error").html("");  
			 }
       
      
	 form_data.append("contract", document.getElementById('files').files[0]);
	  form_data.append("order_id", $('#order_id').val());
	$("#loadDiv").show();
	
	 //alert(form_data);
		   var url = "<?php echo base_url()."Dashboard/savecontract"?>";
		   var adminRedirectUrl = "<?php echo base_url()."dashboard/contracts"?>";
		    $.ajax({
	                type: "POST",
	                url: url,
					data: form_data,	                
	                cache: false,
					contentType: false,
                    processData: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Contract is inserted successfully", "success");
							
	                        setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 2000);
	                    } else if(data==2){
							swal("Error!", "This order number contract is already uploaded", "error");
							}else {
	                        swal("Error!", "Contract is not been inserted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
						$("#loadDiv").hide();
						$( ".close" ).trigger( "click" );
	                }
	            });
		   } 
	function updateemployee(){
		
		var form_data = new FormData();		
       form_data.append("id", $('#id_edit').val()); 
	   form_data.append("contract", document.getElementById('files_edit').files[0]);
     $("#loadDiv2").show();
		   var url = "<?php echo base_url()."Dashboard/updatecontract"?>";
		     $.ajax({
	                type: "POST",
	                url: url,
	                data: form_data,	                
	                cache: false,
					contentType: false,
                    processData: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Contract is updated successfully", "success");
							
							 var t = $('#example').DataTable();
/*t.cell('.selected', ':eq(2)').data($('#order_id_edit').val());
t.cell('.selected', ':eq(3)').data($('#files_edit').val());
t.draw();*/
t.row( { selected: true } ).deselect();
	                       /* setTimeout(function() {
	                          
	                        }, 2000);*/
	                    } else {
						var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Contract is not been updated", "error");

	                    }
						$("#loadDiv2").hide();
						$( ".close" ).trigger( "click" );
	                }
	            });
		   } 

function deleteemp(id){

	 var res = confirm("Are you sure you want to delete this contract");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deletecontract"?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Contract is deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else {
							var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Contract is not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
function validate(){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	 
	 var res = confirm("Are you sure you want to delete "+countid+" content Contracts");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deletecontract"?>";
	        
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Contracts are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else {
							var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Contracts are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
 
  	

$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {
                    columns: [ 1, 2,3 ]
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Contracts-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                    columns: [ 1, 2,3 ]
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Contracts-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {
                    columns: [ 1, 2,3 ]
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Contracts-'+date
            }
        ]
		}
		]
		
   });

});

$(document).ready(function(){
	var fruits = [<?php foreach($contracts as $contract){?>"<?php echo $contract['id'];?>",<?php }?>];
	
	//var fk = fruits.values();
	//var fkey = fruits.keys();
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
	 	
	
 
  
});


   // Handle form submission event
  function Filevalidation(){
        const fi = document.getElementById('files');		
        // Check if any file is selected. 
	  var filePath = fi.value; 
          
            // Allowing file type 
            var allowedExtensions =  
                    /(\.pdf|\.docx|\.doc)$/i; 
              
            if (!allowedExtensions.exec(filePath)) { 
                alert('Invalid file type. Allowed files are pdf,doc,docx');    
				 fi.value = '';            
                return false; 
            }
       /* if (fi.files.length > 0) { 
		
            for (const i = 0; i <= fi.files.length - 1; i++) { 
  
                const fsize = fi.files.item(i).size; 
                const file = Math.round((fsize / 1024)); 
				
                if (file >= 10240) { 
                    alert( 
                      "File too Big, please select a file less than 10mb"); 
                } 
            } 
        } */
    } 
	function Filevalidationedit(){
        const fi = document.getElementById('files_edit');		
        // Check if any file is selected. 
	  var filePath = fi.value; 
          
            // Allowing file type 
            var allowedExtensions =  
                    /(\.pdf|\.docx|\.doc)$/i; 
              
            if (!allowedExtensions.exec(filePath)) { 
                alert('Invalid file type. Allowed files are pdf,doc,docx');    
				 fi.value = '';            
                return false; 
            }
       /* if (fi.files.length > 0) { 
		
            for (const i = 0; i <= fi.files.length - 1; i++) { 
  
                const fsize = fi.files.item(i).size; 
                const file = Math.round((fsize / 1024)); 
				
               
                if (file >= 10240) { 
                    alert( 
                      "File too Big, please select a file less than 10mb"); 
                } 
            } 
        } */
    } 
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
