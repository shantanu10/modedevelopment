<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
  <?php include 'common/header.php';?>
  <?php include 'common/nav.php';?>
  <!-- Offcanvas Navigation End -->
  <div class="offcanvas-overlay"></div>
  <!-- -------end-responsive-header------ -->
  <div class="left-content">
    <div class="mother-grid-inner"> 
      <!--header start here-->
      <?php include 'common/navbar.php';?>
      <!--heder end here--> 
      
      <!--inner block start here--> 
      
      <!--market updates updates-->
      <div class="inner-block">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
          <li class="active">Fitting Orders</li>
        </ol>
        <h3>Fitting Order List</h3>
        <div class="page-header">
          <div class="main"> 
            <!--button-->
            <div class="main-content"> <!--<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>-->
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"  value="" style="margin-right: 5px; float:right;"><i class="fa fa-plus"></i></button>
  
              <?php if($this->session->flashdata('msg')): ?>
              <span style="color:green; padding-top:10px">
              <center>
                <?php echo $this->session->flashdata('msg'); ?>
              </center>
              </span>
              <?php endif; ?>
              <table id="example" class="table table-striped table-bordered display select" cellspacing="0">
                <thead>
                  <tr>
                    <th width="5%"><input type="checkbox" id="master"></th>
                    <th width="5%">S.No</th>
                    <th>Order Number</th>
                    <th>Order Date</th>
                    <th>Order Due Date</th>
                    <th>customer Code</th>
                    <th>Customer Name</th>
                    <th>Customer Mobile</th>
                    <th>Order Total</th>
                    <th>Status</th>
                    <th>Assign for fitting</th>
                    
                    <!--<th>Action</th>--> 
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($fittings)){
   	$i=1;
   foreach ($fittings as $fitting){
   	$id = $fitting['id'];
	$order_id = $fitting['order_id'];
	$where = "id = '".$order_id."'";
    $order = $this->admin_model->getDataById('sales_orders',$where);
   	$salesOrderId = $order[0]['salesOrderId'];
	$orderNumber = $order[0]['orderNumber'];
	$orderDate = $order[0]['orderDate'];
	$orderDueDate = $order[0]['orderDueDate'];
	$customerCode = $order[0]['customerCode'];
	$customerName = $order[0]['customerName'];
	$stageId = $fitting['stageId'];
	 $where = "stageId='".$stageId."'";
     $status = $this->admin_model->getDataById('sales_order_stages',$where);
	
	$orderTotal = $order[0]['orderTotal'];
	$customer_id = $order[0]['customer_id'];
	$where = "id = '".$customer_id."'";
    $customer = $this->admin_model->getDataById('customers',$where);
    ?>
                  <tr class="gradeX">
                    <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
                    <td><?php echo $i;?></td>
                   <td><?php echo $orderNumber;?></td>
                    <td><?php echo $orderDate;?></td>
                    <td><?php echo $orderDueDate;?></td>
                    <td><?php echo $customerCode;?></td>
                    <td><?php echo $customerName;?></td>
                   <td><?php echo $customer[0]['mobile'];?></td>
                    <td><?php echo $orderTotal;?></td>
                     <td id="stat<?php echo $id;?>"><?php echo $status[0]['name'];?></td>
                    
                  <td><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalview<?php echo $id;?>"  onClick="openmodal('<?php echo $id;?>','<?php echo $orderNumber;?>')"><i class="fa fa-eye"></i></button>
                      </td>
                  <!-- Modal --> 
                  
                  
                  <?php $i++;}}?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assign to Fitter</h4>
      </div>
      <div class="modal-body">
        <table class="horizontal-table" width="100%">
                            <tbody>
                                                            
                                
                                <tr>
                                    <th>Assign to Fitter<span style="color:red">* </span></th>
                                    <td>
                                    <select name="user_id" id="user_id" class="form-control">
                                    <option value="">Select</option>
                                    <?php if(!empty($fitters)){ foreach($fitters as $fitter){?>
                                    <option value="<?php echo $fitter['id'];?>"><?php echo $fitter['name'];?></option>
                                    <?php } }?>
                                    </select>
                                    <div class="text-danger" id="fitter_error"></div>
                                     	</td>
                                </tr>
                                <tr>
                                    <th>Assign Date<span style="color:red">* </span></th>
                                    <td><input class="form-control" id="assign_date" name="assign_date" required type="date" />
                                    <div class="text-danger" id="assigndate_error"></div>
                                    </td>
                                </tr>
                                 <tr>
                                    <th>Expected Fitting Date<span style="color:red">* </span></th>
                                    <td><input class="form-control" id="fitting_date" name="fitting_date" required type="date" />
                                    <div class="text-danger" id="fitting_date_error"></div>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
    <br>
    &nbsp; <button type="button" value="active" name="SubBtn" class="btn" onClick="saveassign()">Assign</button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
      <div class="modal fade modalview" role="dialog">
        <div class="modal-dialog" style="width:30%"> 
       
          <!-- Modal content-->
          <div class="modal-content">
           
            <div id="itemdetails"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!--inner block end here--> 
      <!--copy rights start here-->
      <?php include 'common/footer.php';?>
      <!--COPY rights end here--> 
    </div>
  </div>
  <!--slider menu-->
  <div class="clearfix"> </div>
</div>
 
<script>
	 function openmodal(id,orderNumber){
		 //alert(order);
	    $('.modalview').removeAttr('id');
	   $('.modalview').attr('id','myModalview'+id);
	     var url = "<?php echo base_url()."fitting/assignfitting"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id,
						orderNumber: orderNumber
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModalview'+id).modal('show');
	                   
	                }
	            });
	  
	   } 
	  
function saveassign(){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
		  var chkarr = checkedid.split(" ");
		  
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	var user_id = $('#user_id').val();
	//alert(delivery_man_id);
		   var assign_date = $('#assign_date').val();
		   var fitting_date = $('#fitting_date').val();		   
		  if(user_id.length==0){
			$("#fitter_error").html("Fitter is Required Field.");
			return false;  
		  }else{
			$("#fitter_error").html("");  
			 }
         if(assign_date.length==0){
			$("#assigndate_error").html("Assign date is Required Field.");
			return false;
		 }else{
			  $("#assigndate_error").html("");
			  }
			if(fitting_date.length==0){
			$("#fitting_date_error").html("Fitting date is Required Field.");
			return false;  
		  }else{
			  $("#fitting_date_error").html("");
			  }
	
	 var res = confirm("Are you sure you want to assign "+countid+" orders");
	    if(res == true) {
	            var url = "<?php echo base_url()."fitting/saveassignfitting"?>";
	        
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid,
						user_id: user_id,
						assign_date: assign_date,
						fitting_date: fitting_date					
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Fitting assignments are inserted successfully", "success");
	                       setTimeout(function() {
							   for (i = 0; i < countid; i++) {
								  var id = favorite[i];
								  $('#stat'+id).text('Fitter Assigned');
                               }
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Fitting assignments are not been deleted", "error");
	                   
	                    }
						$( ".close" ).trigger( "click" );
	                }
	            });
	        }
}		
 
$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {                  
				 columns: [ 1,2, 3,4,5,6,7,8,9 ]	               
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Order Fitting-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                  columns: [ 1,2, 3,4,5,6,7,8,9 ]	               
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Order Fitting-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {                  
				  columns: [ 1,2, 3,4,5,6,7,8,9 ]
	               
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Order Fitting-'+date
            }
        ]
		}
		]
		
   });

});

$(document).ready(function(){
	var fruits = [<?php foreach($fittings as $fitting){?>"<?php echo $fitting['id'];?>",<?php }?>];
	
	//var fk = fruits.values();
	//var fkey = fruits.keys();
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
});



   // Handle form submission event
  
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
