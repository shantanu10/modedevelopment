<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Item List (Order Number : <?php echo $orderNumber;?>)</h4>
      </div>
      <div class="modal-body">
      <a onClick="return itemvalidate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
             
        <table id="exampleitem" class="table table-striped table-bordered display select" cellspacing="0">
                <thead>
                  <tr>
                   <th width="10%"><input type="checkbox" id="master"></th>
                    <th>S.No</th>
                    <th>Sales Order Id</th>
                    <th>Order Number</th>
                    <th>Item Code</th>                
                    <th>Description</th>
                     <th>Foreign Description</th>
                    <th>Quantity</th>
                    
                    <!--<th>Action</th>-->
                  </tr>
                </thead>
                <tbody>
              
                  <?php if (!empty($items)){
   	$j=1;
   foreach ($items as $item){
   	$id = $item['id'];
   	$salesOrderId = $item['salesOrderId'];
	$orderNumber = $item['orderNumber'];
	$itemCode = $item['itemCode'];
	$description = $item['description'];
	$foreignDescription = $item['foreignDescription'];
	$quantity = $item['quantity'];
	
    ?>
                  <tr class="gradeX">
                  <td><input type="checkbox" class="sub_chk" name="itemchk[]" id="itemchk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
                    <td><?php echo $j;?></td>
                    <td><?php echo $salesOrderId;?></td>
                      <td><?php echo $orderNumber;?></td>
                    <td><?php echo $itemCode;?></td>                     
                       <td><?php echo $description;?></td>
                       <td><?php echo $foreignDescription;?></td>
                        <td><?php echo $quantity;?></td>
                      
                  </tr>
                  <?php $j++;}}else{?>
					  
					  <tr><td colspan="6" align="center">No items here.</td></tr>
                       <?php }?>
                </tbody>
              </table>
      </div>
<script>
$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#exampleitem').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {                  
				 columns: [ 1,2, 3,4,5,6 ]	               
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Sales Order Items(Order Number:<?php echo $orderNumber;?>)-'+date
            },
			 {
                extend: 'csvHtml5',
				charset: "utf-8",
                bom: "true",
				exportOptions: {
                  columns: [ 1,2, 3,4,5,6 ]	               
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Sales Order Items(Order Number:<?php echo $orderNumber;?>)-'+date
            },
            {
                extend: 'pdfHtml5',
				charset: "utf-8",
                bom: "true",
				exportOptions: {                  
				  columns: [ 1,2, 3,4,5,6 ]
	               
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Sales Order Items(Order Number:<?php echo $orderNumber;?>)-'+date
            }
        ]
		}
		]
		
   });

});
$(document).ready(function(){
	var items = [<?php foreach($items as $item){?>"<?php echo $item['id'];?>",<?php }?>];
	
		$('#exampleitem .dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(items[i]).attr('name' , 'itemchk[]').attr('id' , 'itemcheckbox' + items[i]).wrap('<label></label>').closest('label');  
        });
});
function itemvalidate(){

    if ($('input[name^=itemchk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='itemchk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=itemchk]:checked').length;
	
	 
	 var res = confirm("Are you sure you want to delete "+countid+" Sales Order Item");
	    if(res == true) {
	            var url = "<?php echo base_url()."order/deleteorderitems"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Sales Order Items are deleted successfully", "success");
							$('#exampleitem .selected').remove().draw( false );
	                      $( ".close" ).trigger( "click" );
	                    } else {
	                        swal("Error!", "Sales Order Items are not been deleted", "error");
	                      
	                    }
	                }
	            });
	        }
}
</script>
