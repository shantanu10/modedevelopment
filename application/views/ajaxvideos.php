<div class="modal-body"> <a onClick="return imagevalidate('<?php echo $id;?>','<?php echo count($videodata);?>')" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
  <?php if (!empty($videodata)){
   foreach ($videodata as $video){?>
  <div  class="img-wraps" id="img<?php echo $video['id'];?>"> <span  class="closes" title="Delete" onClick="return imagedel('<?php echo $video['id'];?>')" >&times;</span> <video width="220" height="140" controls>
  <source src="<?php echo base_url();?>uploads/image_videos/<?php echo $video['image_video'];?>" type="video/mp4">
  <source src="<?php echo base_url();?>uploads/image_videos/<?php echo $video['image_video'];?>" type="video/avi">
  <source src="<?php echo base_url();?>uploads/image_videos/<?php echo $video['image_video'];?>" type="video/wmv">
 
</video></div>
  <?php } }?>
</div>
<style>
.img-wraps {
    position: relative;
    display: inline-block;
   
    font-size: 0;
}
.img-wraps .closes {
    position: absolute;
    top: 5px;
    right: 8px;
    z-index: 100;
    background-color: #FFF;
    padding: 4px 3px;
    
    color: #000;
    font-weight: bold;
    cursor: pointer;
   
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
    border:1px solid red;
}
.img-wraps:hover .closes {
    opacity: 1;
}
</style>
<script>
function imagevalidate(id,count){
	
	var res = confirm("Are you sure you want to delete "+count+" videos");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deleteallvideo"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Videos are deleted successfully", "success");
							
	                      $( ".close" ).trigger( "click" );
	                    } else {
	                        swal("Error!", "Videos are not been deleted", "error");
	                      
	                    }
	                }
	            });
	        }
	 
}
function imagedel(id){
	
	var res = confirm("Are you sure you want to delete this video");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deletevideo"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Video is deleted successfully", "success");
							
	                      $( "#img"+id ).remove();
	                    } else {
	                        swal("Error!", "Video is not been deleted", "error");
	                      
	                    }
	                }
	            });
	        }
	 
}

</script>