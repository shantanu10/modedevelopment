
<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
    <div class="page-container">
     <?php include 'common/header.php';?>
      <!-- Offcanvas Navigation Start -->
     <?php include 'common/nav.php';?>

<!-- Offcanvas Navigation End -->
<div class="offcanvas-overlay"></div>
    <!-- -------end-responsive-header------ -->
        <div class="left-content">
            <div class="mother-grid-inner">
                <!--header start here-->
                <?php include 'common/navbar.php';?>
                <!--heder end here-->
               
                <!--inner block start here-->
            <!--market updates updates-->
                    
<style>
.form-control{
  margin-bottom: 7px;
}
</style>

<style type="text/css">
    .file {
        visibility: hidden;
        position: absolute;
    }
    .browse {
        box-shadow: none;
        border: 1px solid #ddd;
        padding: 8px;
    }
    i.glyphicon {
        margin-bottom: 0px;
        font-size: 11px;
    }
    .input-group .form-control {
        height: 36px;
        padding: 5px;
    }
    .main-content {
        /*  padding: 2em 1em 4em;*/
        box-shadow: none;
    }
     .form-control {
        height: 28px;
        font-size: 11px;
        padding: 2px 12px;
    }
    .main-content table tr:nth-child(2n) {
        background: transparent;
    }

    .main-content table tr td {
    padding-left: 40px;
    }

    .main-content table td, th {
        padding: 6px;
        font-weight:normal;
    }
    .pay table tr td {
    padding-left: 40px;
    }
   .pay table tr:nth-child(even) {
        background-color: #fff;
    }
    #TypeNewtable .form-control {
        border:none;
        box-shadow:none;
    }
    #TypeNewtable  tr:nth-child(even){
        background:#fff !important;
    }
    #TypeNewtable > tbody > tr > td {
        padding: 15px 8px 0px 0px;
    }
    .tab-content .col-md-10 input{
        border:none;
    }
    .ul {
        list-style-type:none;
    }
</style>

<div class="inner-block">
    <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
        <li class="active">Change Password</li>
    </ol>
    <div class="page-header">
        <h3>Change Password</h3>
    </div>
    <div class="main">
    <!--button-->
    <strong class="text-muted d-block mb-2" style="color:#ff0000!important;"> <?php echo validation_errors(); ?></strong>
     <center><div style="color:#ff0000">
      <?php 
	
	if($this->session->flashdata('msg')) {?>
      <?php echo addslashes($this->session->flashdata('msg')); ?>
      <?php } ?>
    </div>
    </center>
     <form action="<?php echo base_url()?>dashboard/change_password" enctype="multipart/form-data" method="post">        
         <div class="main-content">
                <div class="col-md-12" style="padding: 0;">
                    <span class="clearfix"></span>
                    <div class="col-md-8" style="padding-left: 200px;">
                        
                        <table class="horizontal-table" width="100%">
                            <tbody>
                                                            
                                <tr>
                                    <th>Old Password <span style="color:red">* </span></th>
                                    <td><input class="form-control" placeholder="Old Password" id="oldpassword" name="oldpassword" required type="password" value="" /></td>
                                </tr>
                                <tr>
                                    <th>New Password <span style="color:red">* </span></th>
                                    <td><input class="form-control" placeholder="New Password" id="newpassword" name="newpassword" required type="password" value="" /></td>
                                </tr>
                                <tr>
                                    <th>Confirm New Password <span style="color:red">* </span></th>
                                    <td><input class="form-control" placeholder="Confirm New Password" id="confirmpassword" name="confirmpassword" required type="password" value="" /></td>
                                </tr>
                                                               
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <span class="clearfix"></span>
                <div class="col-md-12 " style="padding-left: 200px;padding: 0;">
                <button type="submit" value="active" name="SubBtn" class="btn">Update</button>
                    <a href="<?php echo base_url()?>dashboard" class="btn">Cancel</a>
                </div>
            </div>
        </form>                   
          </div>
        <span class="clearfix"></span>
    </div>
                <!--inner block end here-->
                <!--copy rights start here-->
               <?php include 'common/footer.php';?>
                <!--COPY rights end here-->
            </div>
        </div>
        <!--slider menu-->
        <div class="clearfix"> </div>
    </div>
    <!--slide bar menu end here-->
    <?php include 'common/script.php';?>
    <script>
	 var password = document.getElementById("newpassword"), confirm_password = document.getElementById("confirmpassword");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
 </script>
</body>
</html>
