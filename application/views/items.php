<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
<?php include 'common/header.php';?>
<?php include 'common/nav.php';?>
<!-- Offcanvas Navigation End -->
<div class="offcanvas-overlay"></div>
<!-- -------end-responsive-header------ -->
<div class="left-content">
<div class="mother-grid-inner">
<!--header start here-->
<?php include 'common/navbar.php';?>
<!--heder end here--> 

<!--inner block start here--> 

<!--market updates updates-->
<div class="inner-block">
<ol class="breadcrumb">
  <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
  <li class="active">Item Code</li>
</ol>
<h3>Item Code List</h3>
<div class="page-header">
<div class="main">
<!--button-->
<div class="main-content">
<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
<div class="dropdown" style="float:right; padding-right:5px">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" title="Change Status"><i class="fa fa-check"></i> <span class="caret"></span></button>
  <ul class="dropdown-menu" style="min-width:75px">
    <li><a onClick="return validatestatus('Active')">Active</a></li>
    <li><a onClick="return validatestatus('Inactive')">Inactive</a></li>
  </ul>
</div>
&nbsp;
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" value="Add Item Code" style="margin-right: 5px; float:right;"><i class="fa fa-plus"></i></button>

<?php if($this->session->flashdata('msg')): ?>
<span style="color:green; padding-top:10px">
<center>
  <?php echo $this->session->flashdata('msg'); ?>
</center>
</span>
<?php endif; ?>
<?php // echo '<pre>';print_r($customers);?>
<form id="frm-example"  method="POST">
<table id="example" class="table table-striped table-bordered display select" cellspacing="0">
  <thead>
    <tr>
      <th width="5%"><!--<input type="checkbox" id="master">--></th>
      <th width="10%">S.No</th>
      <th>Item Code</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($itemcodes)){
   	$i=1;
   foreach ($itemcodes as $item){
   	$id = $item['id'];
   	$itemCode = $item['itemCode'];	
	$status = $item['status'];
    ?>
    <tr class="gradeX check">
      <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
      <td><?php echo $i;?></td>
      <td><?php echo $itemCode;?></td>

      <td id="stat<?php echo $id;?>"><a  class="btn" id="boundOnPageLoaded<?php echo $id;?>"><?php echo $status;?></a></td>
      <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModaledit<?php echo $id;?>"  onClick="openmodal('<?php echo $id;?>')"><i class="fa fa-edit"></i></button>
        <a onClick="return deleteemp('<?php echo $id;?>')" title="Delete" class="btn btn-info "  direction="right"><i class="fa fa-trash"></i></a></td>
    </tr>
    <script>
                  $("body").delegate("#boundOnPageLoaded<?php echo $id;?>", "click", function(){
//alert("boundOnPageLoaded Button Clicked")

	var url = "<?php echo base_url()."Dashboard/itemcodechangestatus"?>";
	/*var status = '<?php echo $changestatus;?>';*/
	var getstat = $("#boundOnPageLoaded<?php echo $id;?>").text();
	
	var id = '<?php echo $id;?>';
	if(getstat=='Active'){
		var changestat = 'Inactive';
		}else{
		var changestat = 'Active';	
		}
		
	$.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id,
						status : changestat
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Item Code status updated successfully", "success");
							
							
							
	                        setTimeout(function() {
	                            $('#stat'+id).html('<a class="btn" id="boundOnPageLoaded'+id+'">'+changestat+'</a>');
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Item Code is not been updated", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	

});</script>
    <?php $i++;}}?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Item Code</h4>
      </div>
      <div class="modal-body">
        <table class="horizontal-table" width="100%">
          <tbody>
            <tr>
              <th>Name<span style="color:red">* </span></th>
              <td><input class="form-control" id="itemCode" name="itemCode" required type="text" value="" />
                <div class="text-danger" id="name_error"></div></td>
            </tr>
          
          </tbody>
        </table>
        <br>
        <button type="button" value="active" name="SubBtn" class="btn" onClick="saveemployee()">Add</button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade editmodal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Item Code</h4>
      </div>
      <div id="itemdetails" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--inner block end here--> 
<!--copy rights start here-->
<?php include 'common/footer.php';?>
<!--COPY rights end here-->
</div>

</div>
<!--slider menu-->
<div class="clearfix"> </div>
</div>
<script>
		 function openmodal(id){
	    $('.editmodal').removeAttr('id');
	   $('.editmodal').attr('id','myModaledit'+id);
	     var url = "<?php echo base_url()."Dashboard/ajaxedititemcode"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModaledit'+id).modal('show');
	                   
	                }
	            });
	  
	   }     
		
       function saveemployee(){
		   var itemCode = $('#itemCode').val();
		  
		  if(itemCode.length==0){
			$("#name_error").html("Item Code is Required Field.");
			return false;  
		  }else{
			$("#name_error").html("");  
			 }
        
		   var url = "<?php echo base_url()."Dashboard/saveitemcode"?>";
		   var adminRedirectUrl = "<?php echo base_url()."dashboard/items"?>";
		    $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    itemCode: itemCode					
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Item Code is inserted successfully", "success");
							
	                        setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 2000);
	                    } else {
	                        swal("Error!", "Item Code is not been inserted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
		   } 
	function updateemployee(){
		var id = $('#itemCode_id').val();
		   var itemCode = $('#itemCode_edit').val();
		 
		  if(itemCode.length==0){
			$("#name_edit_error").html("Item Code is Required Field.");
			return false;  
		  }else{
			$("#name_edit_error").html("");  
			 }
        
		   var url = "<?php echo base_url()."Dashboard/updateitemcode"?>";
		   //var adminRedirectUrl = "<?php echo base_url()."Dashboard/employees"?>";
		    $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
						id: id,
	                    itemCode: itemCode												
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Item Code is updated successfully", "success");
							 var t = $('#example').DataTable();
t.cell('.selected', ':eq(2)').data(itemCode);

t.draw();
t.row( { selected: true } ).deselect();
	                       /* setTimeout(function() {
	                          
	                        }, 2000);*/
	                    } else {
	                        swal("Error!", "Item Code is not been updated", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
						$('#myModaledit'+id).modal('hide');
	                }
	            });
		   } 

function deleteemp(id){

	 var res = confirm("Are you sure you want to delete this item code");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deleteitemcode"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Item codes are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else {
	                        swal("Error!", "Item codes are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
function validate(){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	 
	 var res = confirm("Are you sure you want to delete "+countid+" Item code");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deleteitemcode"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Item codes are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else {
	                        swal("Error!", "Item codes are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
 function validatestatus(status){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
		  var chkarr = checkedid.split(" ");
		  
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	
	 var res = confirm("Are you sure you want to update status "+countid+" Item code");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/itemcodechangestatus"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid,
						status : status
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Item code status updated successfully", "success");
	                       setTimeout(function() {
							   for (i = 0; i < countid; i++) {
								  var id = favorite[i];
								  $('#stat'+id).html('<a class="btn" id="boundOnPageLoaded'+id+'">'+status+'</a>');
                               }
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Item codes are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
  	

$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {
                    columns: [ 1, 2 ]
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Item Codes-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                    columns: [ 1, 2 ]
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Item Codes-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {
                    columns: [ 1, 2 ]
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Item Codes-'+date
            }
        ]
		}
		]
		
   });

});

$(document).ready(function(){
	var fruits = [<?php foreach($itemcodes as $item){?>"<?php echo $item['id'];?>",<?php }?>];
	
	//var fk = fruits.values();
	//var fkey = fruits.keys();
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
	 	
	
 
  
});



   // Handle form submission event
  
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
