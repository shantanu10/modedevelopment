<div class="modal-body"> <a onClick="return imagevalidate('<?php echo $id;?>','<?php echo count($imagedata);?>')" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
  <?php if (!empty($imagedata)){
   foreach ($imagedata as $img){?>
  <div  class="img-wraps" id="img<?php echo $img['id'];?>"> <span  class="closes" title="Delete" onClick="return imagedel('<?php echo $img['id'];?>')" >&times;</span> <img src="<?php echo base_url();?>uploads/order_type/<?php echo $img['image'];?>" width="100" style="padding:5px" class="img-responsive" ></div>
  <?php } }?>
</div>
<style>
.img-wraps {
    position: relative;
    display: inline-block;
   
    font-size: 0;
}
.img-wraps .closes {
    position: absolute;
    top: 5px;
    right: 8px;
    z-index: 100;
    background-color: #FFF;
    padding: 4px 3px;
    
    color: #000;
    font-weight: bold;
    cursor: pointer;
   
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
    border:1px solid red;
}
.img-wraps:hover .closes {
    opacity: 1;
}
</style>
<script>
function imagevalidate(id,count){
	
	var res = confirm("Are you sure you want to delete "+count+" images");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deleteallordertypeimage"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Images are deleted successfully", "success");
							
	                      $( ".close" ).trigger( "click" );
	                    } else {
	                        swal("Error!", "Images are not been deleted", "error");
	                      
	                    }
	                }
	            });
	        }
	 
}
function imagedel(id){
	
	var res = confirm("Are you sure you want to delete this image");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deleteordertypeimage"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Image is deleted successfully", "success");
							
	                      $( "#img"+id ).remove();
	                    } else {
	                        swal("Error!", "Image is not been deleted", "error");
	                      
	                    }
	                }
	            });
	        }
	 
}

</script>