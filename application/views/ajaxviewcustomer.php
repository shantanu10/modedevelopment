
<table class="horizontal-table" width="100%">
  <tbody>
    <tr>
      <th>Name</th>
      <td><?php echo $customer[0]['name'];?></td>
    </tr>
    <tr>
      <th>Foreign Name</th>
      <td><?php echo $customer[0]['foreignName'];?></td>
    </tr>
    <tr>
      <th>Country Calling Code</th>
      <td><?php echo '+'.$customer[0]['callingCode'];?></td>
    </tr>
    <tr>
      <th>Mobile</th>
      <td><?php echo $customer[0]['mobile'];?></td>
    </tr>
    <tr>
      <th>Customer Code</th>
      <td><?php echo $customer[0]['customerCode'];?></td>
    </tr>
    <tr>
      <th>Email</th>
      <td><?php echo $customer[0]['email'];?></td>
    </tr>
    <tr>
      <th>Address</th>
      <td><?php echo $customer[0]['address'];?></td>
    </tr>
    <tr>
      <th>Foreign Address</th>
      <td><?php echo $customer[0]['foreignAddress'];?></td>
    </tr>
    <tr>
      <th>Alternate Mobile</th>
      <td><?php echo $customer[0]['altMobile'];?></td>
    </tr>
    <tr>
      <th>Date of birth</th>
      <td><?php echo $customer[0]['birthDate'];?></td>
    </tr>
    <tr>
      <th>Date of anniversary</th>
      <td><?php echo $customer[0]['anniversaryDate'];?></td>
    </tr>
    <tr>
      <th>Profile Picture</th>
      <td><?php if($customer[0]['image']!=''){?>
        <img src="<?php echo base_url();?>uploads/profile/<?php echo $customer[0]['image'];?>" width="50%" />
        <?php }else{?>
        None
        <?php }?></td>
    </tr>
  </tbody>
</table>
<br>
