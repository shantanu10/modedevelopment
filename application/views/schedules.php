<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
  <?php include 'common/header.php';?>
  <?php include 'common/nav.php';?>
  <!-- Offcanvas Navigation End -->
  <div class="offcanvas-overlay"></div>
  <!-- -------end-responsive-header------ -->
  <div class="left-content">
    <div class="mother-grid-inner"> 
      <!--header start here-->
      <?php include 'common/navbar.php';?>
      <!--heder end here--> 
      
      <!--inner block start here--> 
      
      <!--market updates updates-->
      <div class="inner-block">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
          <li class="active">Delivery Schedules</li>
        </ol>
        <h3>Delivery Schedules List</h3>
        <div class="page-header">
          <div class="main"> 
            <!--button-->
            <div class="main-content"> <!--<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>-->
          
  
              <?php if($this->session->flashdata('msg')): ?>
              <span style="color:green; padding-top:10px">
              <center>
                <?php echo $this->session->flashdata('msg'); ?>
              </center>
              </span>
              <?php endif; ?>
              <table id="example" class="table table-striped table-bordered display select" cellspacing="0">
                <thead>
                  <tr>
                    <th width="5%"><input type="checkbox" id="master"></th>
                    <th width="5%">S.No</th>
                    <th>Order Number</th>                   
                    <th>customer Code</th>
                    <th>Customer Name</th>
                    <th>Customer Mobile</th>
                    <th>Order Total</th>
                    <th>Assigned Date</th>
                    <th>Exp. Delivery Date</th>
                     <th>Delivery Man</th>
                    <th>Status</th>
                   
                    
                    <!--<th>Action</th>--> 
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($schedules)){
   	$i=1;
   foreach ($schedules as $schedule){
   	$id = $delivery['id'];
	
	$orderNumber = $schedule['orderNumber'];
	
	$customerCode = $schedule['customerCode'];
	$customerName = $schedule['customerName'];
	
	$orderTotal = $schedule['orderTotal'];
	$assign_date = $schedule['assign_date'];
	$delivery_date = $schedule['delivery_date'];
	$mobile = $schedule['mobile'];
	$name = $schedule['name'];
	$stageId = $schedule['stageId'];
	 $where = "stageId='".$stageId."'";
     $status = $this->admin_model->getDataById('sales_order_stages',$where);
    ?>
                  <tr class="gradeX">
                    <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
                    <td><?php echo $i;?></td>
                   <td><?php echo $orderNumber;?></td>
                    <td><?php echo $customerCode;?></td>
                    <td><?php echo $customerName;?></td>
                    <td><?php echo $mobile;?></td>
                    <td><?php echo $orderTotal;?></td>
                   <td><?php echo $assign_date;?></td>
                    <td><?php echo $delivery_date;?></td>
                     <td><?php echo $name;?></td>
                    <td><?php echo $status[0]['name'];?></td>
              
                  <!-- Modal --> 
                  
                  
                  <?php $i++;}}?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  
      <!--inner block end here--> 
      <!--copy rights start here-->
      <?php include 'common/footer.php';?>
      <!--COPY rights end here--> 
    </div>
  </div>
  <!--slider menu-->
  <div class="clearfix"> </div>
</div>
 
<script>

$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {                  
				 columns: [ 1,2, 3,4,5,6,7,8,9,10 ]	               
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Dellivery Schedules-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                  columns: [ 1,2, 3,4,5,6,7,8,9,10 ]	               
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Dellivery Schedules-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {                  
				  columns: [ 1,2, 3,4,5,6,7,8,9,10 ]
	               
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Dellivery Schedules-'+date
            }
        ]
		}
		]
		
   });

});

$(document).ready(function(){
	var fruits = [<?php foreach($schedules as $schedule){?>"<?php echo $schedule['id'];?>",<?php }?>];
	
	//var fk = fruits.values();
	//var fkey = fruits.keys();
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
});



   // Handle form submission event
  
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
