<form method='post' action='' enctype="multipart/form-data">
        <table class="horizontal-table" width="100%">
          <tbody>
           <tr>
              <th>Order<span style="color:red">* </span></th>
              <td><select name="order_id" id="order_id_edit" class="form-control" disabled="disabled" >
               <option value="">Select</option>
              <?php foreach($orders as $order){?>
              <option value="<?php echo $order['id'];?>"<?php if($contracts[0]['order_id']==$order['id']){?> selected="selected"<?php }?>><?php echo $order['orderNumber'];?></option>
              <?php }?>
              </select>
                <div class="text-danger" id="order_error"></div></td>
            </tr>
            
             <tr>
              <th>Contract<span style="color:red">* </span></th>
              <td><input type="file" id='files_edit' name="contract"  required onChange="Filevalidationedit()">
                <div class="text-danger" id="file_error"></div></td>
            </tr>
            <tr><th>Contract File</th>
            <td><a href="<?php echo base_url();?>uploads/contract/<?php echo $contracts[0]['contract'];?>" target="_blank"><?php echo $contracts[0]['contract'];?></a></td>
            </tr>
          </tbody>
        </table>
        <br>
        <input type="hidden" id="id_edit" name="id" value="<?php echo $contracts[0]['id'];?>" />
        <button type="button" value="active" name="SubBtn" class="btn" onClick="updateemployee()">Update</button>
        </form>