<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
<?php include 'common/header.php';?>
<?php include 'common/nav.php';?>
<!-- Offcanvas Navigation End -->
<div class="offcanvas-overlay"></div>
<!-- -------end-responsive-header------ -->
<div class="left-content">
<div class="mother-grid-inner">
<!--header start here-->
<?php include 'common/navbar.php';?>
<!--heder end here--> 

<!--inner block start here--> 

<!--market updates updates-->
<div class="inner-block">
<ol class="breadcrumb">
  <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
  <li class="active">Images</li>
</ol>
<h3>Image List</h3>
<div class="page-header">
<div class="main">
<!--button-->
<div class="main-content">
<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
<div class="dropdown" style="float:right; padding-right:5px">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" title="Change Status"><i class="fa fa-check"></i> <span class="caret"></span></button>
  <ul class="dropdown-menu" style="min-width:75px">
    <li><a onClick="return validatestatus('Active')">Active</a></li>
    <li><a onClick="return validatestatus('Inactive')">Inactive</a></li>
  </ul>
</div>
&nbsp;
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" value="Add Item Code" style="margin-right: 5px; float:right;"><i class="fa fa-plus"></i></button>

<?php if($this->session->flashdata('msg')): ?>
<span style="color:green; padding-top:10px">
<center>
  <?php echo $this->session->flashdata('msg'); ?>
</center>
</span>
<?php endif; ?>
<?php // echo '<pre>';print_r($customers);?>
<form id="frm-example"  method="POST">
<table id="example" class="table table-striped table-bordered display select" cellspacing="0">
  <thead>
    <tr>
      <th width="5%"><!--<input type="checkbox" id="master">--></th>
      <th width="10%">S.No</th>
      <th>Type</th>
       <th>Item/Stage</th>
      <th>Status</th>
      <th>Images</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($image_details)){
   	$i=1;
   foreach ($image_details as $images){
   	$id = $images['id'];
   	$content_type = $images['content_type'];
	if($content_type=='item'){
	$itemId = $images['itemId'];
	$whereitem = $this->db->select('itemCode')->from('items')->where('id',$itemId)->get();
	$item_details = $whereitem->result_array('array');
	$contentdata = $item_details[0]['itemCode'];
	}else{
	$stageId = $images['stageId'];
	$wherestage = $this->db->select('name')->from('sales_order_stages')->where('stageId',$stageId)->get();
	$stage_details = $wherestage->result_array('array');
	$contentdata = $stage_details[0]['name'];
	}
	$status = $images['status'];
    ?>
    <tr class="gradeX check">
      <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $id;?>"  value="<?php echo $id;?>"  data-id="<?php echo $id;?>"></td>
      <td><?php echo $i;?></td>
      <td><?php echo $content_type;?></td>
       <td><?php echo $contentdata;?></td>

      <td id="stat<?php echo $id;?>"><a  class="btn" id="boundOnPageLoaded<?php echo $id;?>"><?php echo $status;?></a></td>
      <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModalimage<?php echo $id;?>"  onClick="openmodalimg('<?php echo $id;?>')"><i class="fa fa-eye"></i></button></td>
      <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModaledit<?php echo $id;?>"  onClick="openmodal('<?php echo $id;?>')"><i class="fa fa-edit"></i></button>
        <a onClick="return deleteemp('<?php echo $id;?>')" title="Delete" class="btn btn-info "  direction="right"><i class="fa fa-trash"></i></a></td>
    </tr>
    <script>
                  $("body").delegate("#boundOnPageLoaded<?php echo $id;?>", "click", function(){
//alert("boundOnPageLoaded Button Clicked")

	var url = "<?php echo base_url()."Dashboard/contentchangestatus"?>";
	/*var status = '<?php echo $changestatus;?>';*/
	var getstat = $("#boundOnPageLoaded<?php echo $id;?>").text();
	
	var id = '<?php echo $id;?>';
	if(getstat=='Active'){
		var changestat = 'Inactive';
		}else{
		var changestat = 'Active';	
		}
		
	$.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id,
						status : changestat
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Image status updated successfully", "success");
							
							
							
	                        setTimeout(function() {
	                            $('#stat'+id).html('<a class="btn" id="boundOnPageLoaded'+id+'">'+changestat+'</a>');
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Image is not been updated", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	

});</script>
    <?php $i++;}}?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Images</h4>
      </div>
      <div class="modal-body">
      <form method='post' action='' enctype="multipart/form-data">
        <table class="horizontal-table" width="100%">
          <tbody>
            <tr>
              <th>Content Type<span style="color:red">* </span></th>
              <td><select name="content_type" id="content_type" class="form-control" required onChange="changetype(this.value)">
              <option value="">Select</option>
              <option value="item">Item</option>
               <option value="stage">Stage</option>
              </select>
                <div class="text-danger" id="type_error"></div></td>
            </tr>
           <tr id="itemrow" style="display:none">
              <th>Item<span style="color:red">* </span></th>
              <td><select name="itemId" id="itemId" class="form-control" >
              <option value="">Select</option>
              <?php foreach($itemcodes as $code){?>
              <option value="<?php echo $code['id'];?>"><?php echo $code['itemCode'];?></option>
              <?php }?>
              </select>
                <div class="text-danger" id="itemId_error"></div></td>
            </tr>
             <tr id="stagerow" style="display:none">
              <th>Stage<span style="color:red">* </span></th>
              <td><select name="stageId" id="stageId" class="form-control" >
              <option value="">Select</option>
              <?php foreach($stages as $stage){?>
              <option value="<?php echo $stage['stageId'];?>"><?php echo $stage['name'];?></option>
              <?php }?>
              </select>
                <div class="text-danger" id="stageId_error"></div></td>
            </tr>
             <tr>
              <th>Images<span style="color:red">* </span></th>
              <td><input type="file" id='files' name="image_video[]" multiple required onChange="Filevalidation()">
                <div class="text-danger" id="image_error"></div></td>
            </tr>
          </tbody>
        </table>
        <br>
        <button type="button" value="active" name="SubBtn" class="btn" onClick="saveemployee()">Add</button>
        </form>
        <div class="modal-backdrop fade in" id="loadDiv" style="display: none;">
    <center><div class="loadingmessage" style="padding-top:200px; color:#fff"><img
    src="<?php echo base_url();?>assets/img/ajax-loader.gif" /><br>Please wait...</div>
    </center>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade editmodal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Images</h4>
      </div>
      <div class="modal-backdrop fade in" id="loadDiv2" style="display: none;">
    <center><div class="loadingmessage" style="padding-top:200px; color:#fff"><img
    src="<?php echo base_url();?>assets/img/ajax-loader.gif" /><br>Please wait...</div>
    </center>
    </div>
      <div id="itemdetails" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade editmodalimg" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Images</h4>
      </div>
      <div id="itemdetailsimg" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--inner block end here--> 
<!--copy rights start here-->
<?php include 'common/footer.php';?>
<!--COPY rights end here-->
</div>

</div>
<!--slider menu-->
<div class="clearfix"> </div>
</div>
<script>
	 function openmodalimg(id){
	    $('.editmodalimg').removeAttr('id');
	   $('.editmodalimg').attr('id','myModalimage'+id);
	     var url = "<?php echo base_url()."Dashboard/ajaximages"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetailsimg").html(data);
					 $('#myModalimage'+id).modal('show');
	                   
	                }
	            });
	  
	   }
	function openmodal(id){
	    $('.editmodal').removeAttr('id');
	   $('.editmodal').attr('id','myModaledit'+id);
	     var url = "<?php echo base_url()."Dashboard/ajaxeditdisplaycontentimage"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModaledit'+id).modal('show');
	                   
	                }
	            });
	  
	   }     
		
       function saveemployee(){
		   var form_data = new FormData();
		
		   var content_type = $('#content_type').val();
		  
		  if(content_type.length==0){
			$("#type_error").html("Content type is Required Field.");
			return false;  
		  }else{
			$("#type_error").html("");  
			 }
			  var totalfiles = document.getElementById('files').files.length;
			 if(totalfiles==0){
			$("#image_error").html("Image is Required Field.");
			return false;  
		  }else{
			$("#image_error").html("");  
			 }
       
       for (var index = 0; index < totalfiles; index++) {
      form_data.append("image_video[]", document.getElementById('files').files[index]);
     }
	  form_data.append("content_type", $('#content_type').val());
	 form_data.append("itemId", $('#itemId').val());
	 form_data.append("stageId", $('#stageId').val());
	 $("#loadDiv").show();
	 //alert(form_data);
		   var url = "<?php echo base_url()."Dashboard/saveimages"?>";
		   var adminRedirectUrl = "<?php echo base_url()."dashboard/images"?>";
		    $.ajax({
	                type: "POST",
	                url: url,
					data: form_data,	                
	                cache: false,
					contentType: false,
                    processData: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Images are inserted successfully", "success");
							
	                        setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 2000);
	                    } else {
	                        swal("Error!", "Images are not been inserted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
						$( ".close" ).trigger( "click" );
						$("#loadDiv").hide();
	                }
	            });
		   } 
	function updateemployee(){
		
		var form_data = new FormData();		
        var totalfiles = document.getElementById('files_edit').files.length;
       for (var index = 0; index < totalfiles; index++) {
      form_data.append("image_video[]", document.getElementById('files_edit').files[index]);
     }
	  
      form_data.append("id", $('#id').val()); 
	  $("#loadDiv2").show();  
		   var url = "<?php echo base_url()."Dashboard/updatedisplaycontentimage"?>";
		     $.ajax({
	                type: "POST",
	                url: url,
	                data: form_data,	                
	                cache: false,
					contentType: false,
                    processData: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Images are updated successfully", "success");
							
							 /*var t = $('#example').DataTable();
t.cell('.selected', ':eq(2)').data($('#content_type_edit').val());
if($('#itemId_edit').val()==''){
t.cell('.selected', ':eq(3)').data($('#stageId_edit').val());
}else{
t.cell('.selected', ':eq(3)').data($('#itemId_edit').val());	
}
t.draw();
t.row( { selected: true } ).deselect();*/
	                       /* setTimeout(function() {
	                          
	                        }, 2000);*/
	                    } else {
						var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Images are not been updated", "error");

	                    }
						$( ".close" ).trigger( "click" );
						$("#loadDiv2").hide();
	                }
	            });
		   } 

function deleteemp(id){

	 var res = confirm("Are you sure you want to delete this image content");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deletecontent"?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Image content are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else if(data==2) {
							var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Before delete this row please delete images", "error");
						}else {
							var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Image content are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
function validate(){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	 
	 var res = confirm("Are you sure you want to delete "+countid+" content Images");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/deletecontent"?>";
	        
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Images are deleted successfully", "success");
							$('.selected').remove().draw( false );
	                       /* setTimeout(function() {
	                            window.location.href = adminRedirectUrl;
	                        }, 4000);*/
	                    } else if(data==2) {
							var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Before delete this row please delete images", "error");
						}else {
							var t = $('#example').DataTable();
						t.draw();
						t.row( { selected: true } ).deselect();
	                        swal("Error!", "Images are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
 function validatestatus(status){

    if ($('input[name^=chk]:checked').length <= 0) {
        alert("Check atleast one row");
		return false;
    }
var favorite = [];
            $.each($("input[name^='chk']:checked"), function(){
                favorite.push($(this).val());
            });
          var checkedid = favorite.join(", ");
		  var chkarr = checkedid.split(" ");
		  
	//var checkid = document.getElementsByName('chk');
var countid = $('input[name^=chk]:checked').length;
	
		//var checkidall = JSON.stringify(checkedid);
	//alert(checkedid);
	
	 var res = confirm("Are you sure you want to update status "+countid+" content Image");
	    if(res == true) {
	            var url = "<?php echo base_url()."Dashboard/imagechangestatus"?>";
	         // var adminRedirectUrl = "<?php echo base_url().'customer'?>";
	            // 	var redirectUrl

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: checkedid,
						status : status
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
							
	                        swal("Success!", "Images status updated successfully", "success");
	                       setTimeout(function() {
							   for (i = 0; i < countid; i++) {
								  var id = favorite[i];
								  $('#stat'+id).html('<a class="btn" id="boundOnPageLoaded'+id+'">'+status+'</a>');
                               }
	                        }, 1000);
	                    } else {
	                        swal("Error!", "Images are not been deleted", "error");
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
	                }
	            });
	        }
}
  	

$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {
                    columns: [ 1, 2 ]
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Images-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                    columns: [ 1, 2 ]
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Images-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {
                    columns: [ 1, 2 ]
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Images-'+date
            }
        ]
		}
		]
		
   });

});

$(document).ready(function(){
	var fruits = [<?php foreach($image_details as $item){?>"<?php echo $item['id'];?>",<?php }?>];
	
	//var fk = fruits.values();
	//var fkey = fruits.keys();
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
	 	
	
 
  
});

function changetype(val){
	if(val=='item'){
		$("#itemrow").show();
		$("#stagerow").hide();
		}else{
		$("#stagerow").show();
		$("#itemrow").hide();	
			}
	}
function changetypeedit(val){
	if(val=='item'){
		$("#itemrowedit").show();
		$("#stagerowedit").hide();
		}else{
		$("#stagerowedit").show();
		$("#itemrowedit").hide();	
			}
	}
   // Handle form submission event
  function Filevalidation(){
        const fi = document.getElementById('files');		
        // Check if any file is selected. 
	  var filePath = fi.value; 
          
            // Allowing file type 
            var allowedExtensions =  
                    /(\.jpg|\.jpeg|\.png|\.gif)$/i; 
              
            if (!allowedExtensions.exec(filePath)) { 
                alert('Invalid file type. Allowed files are jpg,jpeg,gif');    
				 fi.value = '';            
                return false; 
            }
        if (fi.files.length > 0) { 
		
            for (const i = 0; i <= fi.files.length - 1; i++) { 
  
                const fsize = fi.files.item(i).size; 
                const file = Math.round((fsize / 1024)); 
				
                // The size of the file. 
                if (file >= 10240) { 
                    alert( 
                      "File too Big, please select a file less than 10mb"); 
                } /*else if (file < 2048) { 
                    alert( 
                      "File too small, please select a file greater than 2mb"); 
                } else { 
                    document.getElementById('size').innerHTML = '<b>'
                    + file + '</b> KB'; 
                } */
            } 
        } 
    } 
	function Filevalidationedit(){
        const fi = document.getElementById('files_edit');		
        // Check if any file is selected. 
	  var filePath = fi.value; 
          
            // Allowing file type 
            var allowedExtensions =  
                    /(\.jpg|\.jpeg|\.png|\.gif)$/i; 
              
            if (!allowedExtensions.exec(filePath)) { 
                alert('Invalid file type. Allowed files are jpg,jpeg,gif');    
				 fi.value = '';            
                return false; 
            }
        if (fi.files.length > 0) { 
		
            for (const i = 0; i <= fi.files.length - 1; i++) { 
  
                const fsize = fi.files.item(i).size; 
                const file = Math.round((fsize / 1024)); 
				
                // The size of the file. 
                if (file >= 10240) { 
                    alert( 
                      "File too Big, please select a file less than 10mb"); 
                } /*else if (file < 2048) { 
                    alert( 
                      "File too small, please select a file greater than 2mb"); 
                } else { 
                    document.getElementById('size').innerHTML = '<b>'
                    + file + '</b> KB'; 
                } */
            } 
        } 
    } 
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
