<table id="examplecustomer" class="table table-striped table-bordered display select" cellspacing="0">
                <thead>
                  <tr>
                    <th width="5%"><!--<input type="checkbox" id="master">--></th>
                    <th width="10%">S.No</th>
                    <th>CardCode</th>
                    <th>Cellular</th>
                    <th>CardName</th>                
                    <th>CardForeignName</th>
                    <th>Status</th>
                    <!--<th>Action</th>-->
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($customers)){
   	$i=1;
   foreach ($customers as $customer){
   	$id = $customer['id'];
   	$CardName = $customer['CardName'];
	$CardCode = $customer['CardCode'];
	$Cellular = $customer['Cellular'];
	$CardForeignName = $customer['CardForeignName'];
	$status = $customer['status'];
    ?>
                  <tr class="gradeX check" onClick="checkrow(<?php echo $id;?>)" id="check<?php echo $id;?>">
                    <td></td>
                    <td><?php echo $i;?></td>
                    <td><?php echo $CardCode;?></td>
                      <td><?php echo $Cellular;?></td>
                    <td><?php echo $CardName;?></td>                     
                       <td><?php echo $CardForeignName;?></td>
                      <td>
                      <?php  if($status=='Active'){ ?>
                       <a href="<?php echo base_url();?>customer/index/<?php echo $id;?>/Inactive" class="btn"><?php echo 'Active';?></a>
                      <?php }else{ ?>
                      <a href="<?php echo base_url();?>customer/index/<?php echo $id;?>/Active" class="btn"><?php echo 'Inactive';?></a>
                      <?php } ?>
                      </td>
                      
                  <!--  <td><a href='<?php echo base_url()?>customer/view_customer/<?php echo $id;?>' tooltip="View"  direction="right" class=" tooltip edit"><i class="fa fa-eye"></i></a> 
                     </td>-->
                  </tr>
                  <?php $i++;}}?>
                </tbody>
              </table>
                   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
      <!--<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>-->
      <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>
     <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js" ></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">