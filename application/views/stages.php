<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
<?php include 'common/header.php';?>
<?php include 'common/nav.php';?>
<!-- Offcanvas Navigation End -->
<div class="offcanvas-overlay"></div>
<!-- -------end-responsive-header------ -->
<div class="left-content">
<div class="mother-grid-inner">
<!--header start here-->
<?php include 'common/navbar.php';?>
<!--heder end here--> 

<!--inner block start here--> 

<!--market updates updates-->
<div class="inner-block">
<ol class="breadcrumb">
  <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
  <li class="active">Stages</li>
</ol>
<h3>Stage List</h3>
<div class="page-header">
<div class="main">
<!--button-->
<div class="main-content">
<!--<a onClick="return validate()" type="button" class="btn" title="Delete" style="margin-bottom: 16px; float:right"> <i class="fa fa-trash"></i></a>
<div class="dropdown" style="float:right; padding-right:5px">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" title="Change Status"><i class="fa fa-check"></i> <span class="caret"></span></button>
  <ul class="dropdown-menu" style="min-width:75px">
    <li><a onClick="return validatestatus('Active')">Active</a></li>
    <li><a onClick="return validatestatus('Inactive')">Inactive</a></li>
  </ul>
</div>-->
<?php if($this->session->flashdata('msg')): ?>
<span style="color:green; padding-top:10px">
<center>
  <?php echo $this->session->flashdata('msg'); ?>
</center>
</span>
<?php endif; ?>
<?php // echo '<pre>';print_r($customers);?>
<table id="example" class="table table-striped table-bordered display select" cellspacing="0">
  <thead>
    <tr>
    <th width="5%"><!--<input type="checkbox" id="master">--></th>
      <th width="10%">S.No</th>
      <th>Stage Id</th>
      <th>Name</th>
       <th>Short Name</th>
        <th>Stage Media Title</th>
         <th>Stage Media Sub Title</th>
          <th>Image</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($stages)){
   	$i=1;
   foreach ($stages as $stage){
   	$stageId = $stage['stageId'];
	$name = $stage['name'];
	$shortName = $stage['shortName'];
	$stageMediaTitle = $stage['stageMediaTitle'];
	$stageMediaSubtitle = $stage['stageMediaSubtitle'];
	$image = $stage['image'];
	
    ?>
    <tr class="gradeX check">
     <td><input type="checkbox" class="sub_chk" name="chk[]" id="chk<?php echo $stageId;?>"  value="<?php echo $stageId;?>"  data-id="<?php echo $stageId;?>"></td>
      <td><?php echo $i;?></td>
      <td><?php echo $stageId;?></td>
      <td><?php echo $name;?></td>
       <td><?php echo $shortName;?></td>
        <td><?php echo $stageMediaTitle;?></td>
         <td><?php echo $stageMediaSubtitle;?></td>
          <td><img src="<?php echo base_url();?>uploads/stages/<?php echo $image;?>" style="background-color:#000"></td>
          <td><button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModaledit<?php echo $stageId;?>"  onClick="openmodal('<?php echo $stageId;?>')"><i class="fa fa-edit"></i></button></td>
    </tr>
    
    <?php $i++;}}?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<div class="modal fade editmodal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Stage</h4>
      </div>
      <div id="itemdetails" style="padding:10px"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--inner block end here--> 
<!--copy rights start here-->
<?php include 'common/footer.php';?>
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
<div class="clearfix"> </div>
</div>

<script>
 function openmodal(id){
	    $('.editmodal').removeAttr('id');
	   $('.editmodal').attr('id','myModaledit'+id);
	     var url = "<?php echo base_url()."Dashboard/ajaxeditstage"?>";
	        
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
	                    id: id
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
					$("#itemdetails").html(data);
					 $('#myModaledit'+id).modal('show');
	                   
	                }
	            });
	   }     
function updatestage(){
		var stageId = $('#stageId').val();
		
		   var name = $('#name_edit').val();
		   var shortName = $('#shortName_edit').val();
		   var stageMediaTitle = $('#stageMediaTitle_edit').val();
		   var stageMediaSubtitle = $('#stageMediaSubtitle_edit').val();
		  if(shortName.length==0){
			$("#shortName_edit_error").html("Short Name is Required Field.");
			return false;  
		  }else{
			$("#shortName_edit_error").html("");  
			 }
		if(stageMediaTitle.length==0){
			$("#stageMediaTitle_edit_error").html("Promotion Title is Required Field.");
			return false;  
		  }else{
			$("#stageMediaTitle_edit_error").html("");  
			 }
		if(stageMediaSubtitle.length==0){
			$("#stageMediaSubtitle_edit_error").html("Promotion Sub Title is Required Field.");
			return false;  
		  }else{
			$("#stageMediaSubtitle_edit_error").html("");  
			 }
		   var url = "<?php echo base_url()."Dashboard/updatestage"?>";
		    $.ajax({
	                type: "POST",
	                url: url,
	                data: ({
						stageId: stageId,
	                    name: name,
						shortName: shortName,
						stageMediaTitle: stageMediaTitle,
						stageMediaSubtitle: stageMediaSubtitle
										
	                }),
	                cache: false,
	                success: function(data) { //alert(data);
	                    if (data == 1) {
						
	                   swal("Success!", "Stage is updated successfully", "success");
						var t = $('#example').DataTable();
						t.cell('.selected', ':eq(3)').data(name);
						t.cell('.selected', ':eq(4)').data(shortName);
						t.cell('.selected', ':eq(5)').data(stageMediaTitle);
						t.cell('.selected', ':eq(6)').data(stageMediaSubtitle);
						
						t.draw();	
						t.row( { selected: true } ).deselect();
						$( ".close" ).trigger( "click" );
	                       /* setTimeout(function() {
	                          
	                        }, 2000);*/
	                    } else {
						swal("Error!", "Stage is not been updated", "error");
						var t = $('#example').DataTable();
						t.row( { selected: true } ).deselect();
						$( ".close" ).trigger( "click" );
	                       // setTimeout(function() {

	                         //   window.location.href = adminRedirectUrl;

	                       // }, 4000);

	                    }
						$('#myModaledit'+id).modal('hide');
	                }
	            });
		   } 
		   
$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
      'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {
                    columns: [ 1, 2,3,4,5,6,7 ]
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Stages-'+date
            },
			 {
                extend: 'csvHtml5',
				charset: "utf-8",
                bom: "true",
				exportOptions: {
                    columns: [ 1, 2,3,4,5,6.7 ]
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Stages-'+date
            },
            {
                extend: 'pdfHtml5',
				charset: "utf-8",
                bom: true,
				exportOptions: {
                    columns: [ 1, 2,3,4,5,6,7 ]
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Stages-'+date
            }
        ]
		}
		]
		
   });

});
$(document).ready(function(){
	var fruits = [<?php foreach($stages as $stage){?>"<?php echo $stage['stageId'];?>",<?php }?>];
	
		$('.dt-checkboxes').each(function(i) {
			//i = i + 1;
          $(this).val(fruits[i]).attr('name' , 'chk[]').attr('id' , 'checkbox' + fruits[i]).wrap('<label></label>').closest('label');  
        });
  
});
    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
