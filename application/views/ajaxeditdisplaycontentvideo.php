<form method='post' action='' enctype="multipart/form-data">
        <table class="horizontal-table" width="100%">
          <tbody>
            <tr>
              <th>Content Type<span style="color:red">* </span></th>
              <td><select name="content_type" id="content_type_edit" class="form-control"  onChange="changetypeedit(this.value)"  disabled="disabled">
              <option value="">Select</option>
              <option value="item" <?php if($content[0]['content_type']=='item'){?> selected="selected"<?php }?>>Item</option>
               <option value="stage" <?php if($content[0]['content_type']=='stage'){?> selected="selected"<?php }?>>Stage</option>
              </select>
                <div class="text-danger" id="type_edit_error"></div></td>
            </tr>
           <tr id="itemrowedit" <?php if($content[0]['content_type']=='item'){?>style="display: table-row;" <?php }else{?> style="display:none"<?php }?>>
              <th>Item<span style="color:red">* </span></th>
              <td><select name="itemId" id="itemId" class="form-control" disabled="disabled">
              <?php foreach($itemcodes as $code){?>
              <option value="<?php echo $code['id'];?>" <?php if($content[0]['itemId']==$code['id']){?> selected="selected"<?php }?>><?php echo $code['itemCode'];?></option>
              <?php }?>
              </select>
                <div class="text-danger" id="itemId_edit_error"></div></td>
            </tr>
             <tr id="stagerowedit" <?php if($content[0]['content_type']=='stage'){?>style="display: table-row;" <?php }else{?> style="display:none"<?php }?>>
              <th>Stage<span style="color:red">* </span></th>
              <td><select name="stageId" id="stageId" class="form-control" disabled="disabled">
              <?php foreach($stages as $stage){?>
              <option value="<?php echo $stage['stageId'];?>" <?php if($content[0]['stageId']==$stage['stageId']){?> selected="selected"<?php }?>><?php echo $stage['name'];?></option>
              <?php }?>
              </select>
                <div class="text-danger" id="stageId_edit_error"></div></td>
            </tr>
             <tr>
              <th>Videos<span style="color:red">* </span></th>
              <td><input type="file" id='files_edit' name="image_video[]" multiple required onchange="Filevalidationedit()">
                <div class="text-danger" id="stageId_error"></div></td>
            </tr>
          </tbody>
        </table>
        <br>
        <input type="hidden" id="id" name="id" value="<?php echo $content[0]['id'];?>" />
        <button type="button" value="active" name="SubBtn" class="btn" onClick="updateemployee()">Update</button>
        </form>