<!DOCTYPE html>
<html>
<?php include 'common/head.php';?>
<body>
<div class="page-container">
  <?php include 'common/header.php';?>
  <?php include 'common/nav.php';?>
  <!-- Offcanvas Navigation End -->
  <div class="offcanvas-overlay"></div>
  <!-- -------end-responsive-header------ -->
  <div class="left-content">
    <div class="mother-grid-inner"> 
      <!--header start here-->
      <?php include 'common/navbar.php';?>
      <!--heder end here--> 
      
      <!--inner block start here--> 
      
      <!--market updates updates-->
      <div class="inner-block">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url()?>dashboard">Home</a></li>
          <li class="active">Material Receipts</li>
        </ol>
        <h3>Receipt List</h3>
        <div class="page-header">
          <div class="main"> 
            <!--button-->
            <div class="main-content"> 
              <?php if($this->session->flashdata('msg')): ?>
              <span style="color:green; padding-top:10px">
              <center>
                <?php echo $this->session->flashdata('msg'); ?>
              </center>
              </span>
              <?php endif; ?>
              <table id="example" class="table table-striped table-bordered display select" cellspacing="0">
                <thead>
                  <tr>
                    
                    <th width="5%">S.No</th>                   
                    <th>Order Number</th>                    
                    <th>customer Code</th>
                    <th>Customer Name</th>
                    <th>Item Code</th>
                    <th>Quantity</th>
                    <th>Palette Code</th>
                    <th>Consignment No</th>
                    <th>GRN</th>
                    <!--<th>Action</th>--> 
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($receipts)){
   	$i=1;
   foreach ($receipts as $receipt){
   	$id = $receipt['id'];
	$whereorder = "id='".$receipt['order_id']."'";
     $orderdata = $this->admin_model->getDataById('sales_orders',$whereorder);
	$orderNumber = $orderdata[0]['orderNumber'];
	$where = "id='".$receipt['customer_id']."'";
     $customerdata = $this->admin_model->getDataById('customers',$where);
	$customerCode = $customerdata[0]['customerCode'];
	$customerName = $customerdata[0]['name'];
	$itemCode = $receipt['itemCode'];
	$quantity = $receipt['quantity'];
	$paletteCode = $receipt['paletteCode'];
	$consignmentNo = $receipt['consignmentNo'];
	$GRN = $receipt['GRN'];
    ?>
                  <tr class="gradeX">
                   
                    <td><?php echo $i;?></td>
                    <td><?php echo $orderNumber;?></td>
                    <td><?php echo $customerCode;?></td>
                    <td><?php echo $customerName;?></td>
                    <td><?php echo $itemCode;?></td>
                    <td><?php echo $quantity;?></td>
                    <td><?php echo $paletteCode;?></td>
                    <td><?php echo $consignmentNo;?></td>
                    <td><?php echo $GRN;?></td>
                    
                  </tr>
                  
                  <!-- Modal --> 
                  
                  
                  <?php $i++;}}?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
      <!--inner block end here--> 
      <!--copy rights start here-->
      <?php include 'common/footer.php';?>
      <!--COPY rights end here--> 
    </div>
  </div>
  <!--slider menu-->
  <div class="clearfix"> </div>
</div>
 
<script>
  
$(document).ready(function (){
	var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
   var table = $('#example').DataTable({
     // 'ajax': 'https://gyrocode.github.io/files/jquery-datatables/arrays_id.json',
      
	  dom: 'Bfrtip',
			  buttons: [
            
            {
                extend: 'collection',               
				text: '<button class="btn btn-info" title="Export"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>',              
			   buttons: [
            {
                extend: 'excelHtml5',
				exportOptions: {                  
				 columns: [ 0,1,2, 3,4,5,6,7,8 ]	               
                },
				text: '<button class="btn btn-info" title="Download Excel"><i class="fa fa-file-excel-o"></i> Excel</button>',
                title: 'Material Receipt-'+date
            },
			 {
                extend: 'csvHtml5',
				exportOptions: {
                  columns: [ 0,1,2, 3,4,5,6,7,8 ]	               
                },
				 text: '<button class="btn btn-info" title="Download CSV"><i class="fa fa-files-o"></i> CSV</button>',
                title: 'Material Receipt-'+date
            },
            {
                extend: 'pdfHtml5',
				exportOptions: {                  
				  columns: [ 0,1,2, 3,4,5,6,7,8 ]
	               
                },
				text: '<button class="btn btn-info" title="Download PDF"><i class="fa fa-file-pdf-o"></i> PDF</button>',
                title: 'Material Receipt-'+date
            }
        ]
		}
		]
		
   });

});


    </script> 

<!--slide bar menu end here-->
<?php include 'common/script.php';?>
</body>
</html>
