<?php
class Dashboard_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
				$this->load->helper("mode");
				$this->load->library('session');
				$this->load->library('upload');
        }
   

   	
	public function duplicatedata($table,$val){
		$this->db->select('*');
		$this->db->from($table);	
		$this->db->where('name', $val);
		$query = $this->db->get();
		$arr = $query->result_array('array');
		return (!empty($arr)) ? $arr : array();
		}
	 public function updateduplicatedata($table,$val,$id){
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where('id <>', $id);
			$this->db->where('name', $val);
			$query = $this->db->get();
			//echo $this->db->last_query();exit;
			$arr = $query->result_array('array');
			return (!empty($arr)) ? $arr : array();
		}
			
	public function delete($table,$id){
		 $this->db->delete($table, array('id' => $id)); 
		
		}
		
	public function getdeldetails($table,$column,$id){
		 $query = $this->db->get_where($table, array($column => $id));
	     return $query->row_array();
		}
	public function getdetails($table,$id){
		 $query = $this->db->get_where($table, array('id' => $id));
	     return $query->row_array();
		}
	
	public function userlist(){
		$this->db->select('*');
		$this->db->from('admins');		
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		$arr = $query->result_array('array');
		return (!empty($arr)) ? $arr : array();
	}
	
		function getTabledata($table)
		{
			$query = $this->db->get($table);
			// echo $this->db->last_query();die;
			if ($query->num_rows() > 0) {
				return $query->result_array();
			} else {
				return false;
			}
			return $resultData;
		}
		function getDataById($table, $where) {
			$this->db->where ( $where );
			$query = $this->db->get ( $table );
			// echo $this->pdo->last_query();die;
			if ($query->num_rows () > 0) {
				$resultData = $query->result_array ();
			} else {
				$resultData = false;
			}
			return $resultData;
		}
	

}