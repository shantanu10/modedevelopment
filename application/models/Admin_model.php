<?php
class Admin_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
				$this->load->helper("mode");
				$this->load->library('session');
        }
    public function validateLogin($email, $password)
        {
        $password = md5($password);
        $this->db->select('id,email,password,name');
        $this->db->from('admin');
	
		$this->db->where('email', $email);
		$this->db->where('password', $password);
        
        $query = $this->db->get();        
        $arr = $query->first_row('array');
        
     
        // pre($arr,1);
        return (! empty($arr)) ? $arr : array();
        // print_r($arr); die("ppp");
    }

    public function check_email_exist($reset_flag = FALSE, $reset = '')
    {
        $this->db->select('id,email,password');
        $this->db->from('admin');
        if ($reset_flag) {
            // die('123');
            $email = base64_decode($this->input->get('key'));
            // $reset = base64_decode($this->input->get('reset'));
            $this->db->where('email', $email);
            $this->db->where('id', $reset);
            $query = $this->db->get();
            // echo $this->db->last_query();
            // die;
            return $query->num_rows();
        } else {
            $this->db->where('email', $this->input->post('email'));
            $query = $this->db->get();
            // $this->db->last_query();
            // die;
            $query->num_rows();
            if ($query->num_rows() > 0) {
                $table = '';
                $result = $query->first_row('array');
                // pre($result,1);
                        $email = base64_encode($this->input->post('email'));
                        $reset = base64_encode($result['id'] . '||timestamp=' . time());
						
                        $activation_link = "<a href='" . base_url() . "login/reset_pass?key=" . $email . "&reset=" . $reset . "'>Click To Reset password</a>";
                        // die;
                        include ('application/libraries/phpmailer/sendEmail.php');
                        $html = '<span style="text-align:center;margin-top:50px;display:block;"> <img width="200" src="' . base_url() . 'assets/img/logo.png" alt="Haircut" title="Haircut"></span><p style="font-size:20px;text-align:center;margin-top:50px;">Click On This Link to Reset Password ' . $activation_link . '</p>';
                      $from = 'haircut@gmail.com';
                        $to = $result['email'];
                        $subject = 'Reset Password Link';
                        $body = $html;
                       // ->send();
                        $result_mail = mailer( $from, $to, $subject, $body );
                        // pre($result_mail,1);
                        if ($result_mail)
                            return true;
            } else {
                return false;
            }
            // pre($result,1);
        }
    }
	public function savepassword(){
		$oldpass = $this->input->post('oldpassword');
		$newpass = $this->input->post('newpassword');
		$confirmpass = $this->input->post('confirmpassword');
		$id = 1;
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$arr = $query->result_array('array');
		
		if(md5($oldpass)!=$arr[0]['password']){
       return $this->session->set_flashdata('msg', 'Current password is incorrect');
	  
		}elseif($newpass==$confirmpass){
		 $data['password'] = md5($newpass);
		 $data['text_password'] = $newpass;
		$this->db->where('id',$id);
		$this->db->update('admin',$data);

			$this->session->set_flashdata('msg', 'Admin password updated succcessfully');
			redirect('dashboard/change_password');
		
		}else{
		return $this->session->set_flashdata('msg', 'New password and confirm password are mismatch');	
		}
		
		}
    public function update_password()
    {
        $email = base64_decode($this->input->post('key'));
        $reset = base64_decode($this->input->post('reset'));
        $reset_arr = explode('||timestamp=', $reset);
        $this->db->where('id', $reset_arr[0]);
        $data['password'] = md5($this->input->post('re_password'));
       // $data['date_modified'] = to_db_date();
        return $this->db->update('admin', $data);
        // echo $this->db->last_query();
    }
	
	public function delete($table,$id){
		 $this->db->delete($table, array('id' => $id)); 
		 
		}
		
	
	public function getdetails($table,$id){
		 $query = $this->db->get_where($table, array('id' => $id));
	     return $query->row_array();
		}
		
		public function getdeldetails($table,$column,$id){
			$query = $this->db->get_where($table, array($column => $id));
			return $query->row_array();
		}
    function getTabledata($table)
		{
			$query = $this->db->get($table);
			// echo $this->db->last_query();die;
			if ($query->num_rows() > 0) {
				return $query->result_array();
			} else {
				return false;
			}
			return $resultData;
		}
	function getDataById($table, $where) {
			$this->db->where ( $where );
			$query = $this->db->get ( $table );
			// echo $this->pdo->last_query();die;
			if ($query->num_rows () > 0) {
				$resultData = $query->result_array ();
			} else {
				$resultData = false;
			}
			return $resultData;
		}
		
		
		
		public function duplicatedata($table,$val){
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where('name', $val);
			$query = $this->db->get();
			$arr = $query->result_array('array');
			return (!empty($arr)) ? $arr : array();
		}
	 public function updateduplicatedata($table,$val,$id){
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where('id <>', $id);
			$this->db->where('name', $val);
			$query = $this->db->get();
			$arr = $query->result_array('array');
			return (!empty($arr)) ? $arr : array();
		}
	
		public function duplicategroupdata($table,$val){
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where('group_name', $val);
			$query = $this->db->get();
			$arr = $query->result_array('array');
			return (!empty($arr)) ? $arr : array();
		}
		
	function delete1($table, $where)
    {
        $this->db->where($where);
        return $this->db->delete($table);
    }
    
   
	function getSMMMobile($mobile,$message)
    {
		if($mobile=='9876543210'){
			return true;
			}
        $msg = urlencode ($message );
        $datetime = "2017-11-08%2013:36:59";
		//$mobile = '91'.$mobile;
		
		// Send the POST request with cURL
		$data = array('userid' => 'kshehzad@al-essa.me', 'password' => 'Nov26@2013','msg'=>$msg, "sender" =>'IXINA', "to" => $mobile);
	$ch = curl_init('http://api.unifonic.com/wrapper/sendSMS.php');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
	return true;
	// Process your response here
	//print_r($response);exit;
	//return json_decode($response);
		
		//$url = file_get_contents ("http://api.msg91.com/api/sendotp.php?authkey=304508A9NwPvYk3F5dd2c582&mobile=$mobile&message=$msg&sender=HAIRCT&otp=$uniqueid");
		//$url = file_get_contents ("http://api.unifonic.com/wrapper/sendSMS.php?userid=kshehzad@al-essa.me&password=Nov26@2013&msg=$msg&sender=IXINA&to=$mobile");
        //$url = file_get_contents ("https://mobile.btmsindia.com/api/api_http.php?username=SHAAPP&password=SHAAPP@2017&senderid=GLOCAL&to=$mobile&text=$msg&route=Informative&type=text&datetime=$datetime");
        /*$jsondecode = explode ( ' [', $url );
        $response = json_decode($jsondecode [0]);	
		print_r($response);exit;	
        return $response->type;*/
	  // $jsondecode = explode(' [', $url);
        // print_r($jsondecode);die;
       // $response = $jsondecode[0];
		
       /* if ($response == "OK") {
            return "Success";
        } else {
            return "Fail";
        }*/
    }
	function salesorderlist($customer_id,$orderTypeId){
		$this->db->select('so.*,st.name as status');
    	$this->db->from('sales_orders so');
		//$this->db->join('customers cm', 'cm.customerCode = so.customerCode');
    	$this->db->join('sales_order_stages st', 'st.stageId = so.stageId');
		if($orderTypeId==1){
    	$this->db->where ( "so.customer_id='".$customer_id."' and so.orderTypeId='2'" );
		}else{
		$this->db->where ( "so.customer_id='".$customer_id."' and so.orderTypeId <> '2'" );	
		}
		$this->db->order_by('so.id','DESC');
    	$query = $this->db->get ();
    	// echo $this->pdo->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}
	function deliveryorderlist(){
		$this->db->select('ds.*,so.orderNumber,so.customerName,so.customerCode,so.orderTotal,od.stageId,u.name,cm.mobile');
    	$this->db->from('delivery_schedules ds');
		$this->db->join('order_delivery od', 'od.id = ds.delivery_id');
    	$this->db->join('sales_orders so', 'so.id = od.order_id');
		$this->db->join('users u', 'u.id = ds.user_id');
		$this->db->join('customers cm', 'cm.id = so.customer_id');
    	//$this->db->where ( "so.customer_id='".$customer_id."'" );
    	$query = $this->db->get ();
    	// echo $this->pdo->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}
	function fittingorderlist(){
		$this->db->select('fs.*,so.orderNumber,so.customerName,so.customerCode,so.orderTotal,of.stageId,u.name,cm.mobile');
    	$this->db->from('fitting_schedules fs');
		$this->db->join('order_fitting of', 'of.id = fs.order_fitting_id');
    	$this->db->join('sales_orders so', 'so.id = of.order_id');
		$this->db->join('users u', 'u.id = fs.user_id');
		$this->db->join('customers cm', 'cm.id = so.customer_id');
    	//$this->db->where ( "so.customer_id='".$customer_id."'" );
    	$query = $this->db->get ();
    	// echo $this->pdo->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}
   function salesorderdetails($orderId){
		$this->db->select('so.*,st.name as status');
    	$this->db->from('sales_orders so');
		//$this->db->join('customers cm', 'cm.customerCode = so.customerCode');
    	$this->db->join('sales_order_stages st', 'st.stageId = so.stageId');
    	$this->db->where ( "so.id='".$orderId."'" );
    	$query = $this->db->get ();
    	// echo $this->pdo->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}
		
	function deliverySchedules($userId){
		$this->db->select('so.id as orderId,cm.name,cm.foreignName,cm.image as customerImage,cm.address,cm.foreignAddress,so.orderNumber,ds.delivery_date as scheduleDateTime,od.stageId');
    	$this->db->from('delivery_schedules ds');
		$this->db->join('order_delivery od', 'od.id = ds.delivery_id');
    	$this->db->join('sales_orders so', 'so.id = od.order_id');
		$this->db->join('users u', 'u.id = ds.user_id');
		$this->db->join('customers cm', 'cm.id = so.customer_id');
    	$this->db->where ( "ds.user_id='".$userId."'" );
		$this->db->order_by ( "od.id","DESC" );
    	$query = $this->db->get ();
    	// echo $this->pdo->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}
	function deliveryOrderDetails($orderId,$userId){
		$this->db->select('so.*,cm.name,cm.foreignName,cm.image as customerImage,cm.address,cm.foreignAddress,cm.callingCode,cm.mobile,ds.delivery_date as scheduleDateTime,od.stageId');
    	$this->db->from('delivery_schedules ds');
		$this->db->join('order_delivery od', 'od.id = ds.delivery_id');
    	$this->db->join('sales_orders so', 'so.id = od.order_id');
		$this->db->join('users u', 'u.id = ds.user_id');
		$this->db->join('customers cm', 'cm.id = so.customer_id');
    	$this->db->where ( "ds.user_id='".$userId."' and od.order_id='".$orderId."'" );
    	$query = $this->db->get ();
    	// echo $this->db->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}
	function fittingSchedules($userId){
		$this->db->select('so.id as orderId,cm.name,cm.foreignName,cm.image as customerImage,cm.address,cm.foreignAddress,so.orderNumber,fs.fitting_date as scheduleDateTime,of.stageId');
    	$this->db->from('fitting_schedules fs');
		$this->db->join('order_fitting of', 'of.id = fs.order_fitting_id');
    	$this->db->join('sales_orders so', 'so.id = of.order_id');
		$this->db->join('users u', 'u.id = fs.user_id');
		$this->db->join('customers cm', 'cm.id = so.customer_id');
    	$this->db->where ( "fs.user_id='".$userId."'" );
		$this->db->order_by ( "of.id","DESC" );
    	$query = $this->db->get ();
    	// echo $this->pdo->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}
	function fittingOrderDetails($orderId,$userId){
		$this->db->select('so.*,cm.name,cm.foreignName,cm.image as customerImage,cm.address,cm.foreignAddress,cm.callingCode,cm.mobile,fs.fitting_date as scheduleDateTime,of.stageId');
    	$this->db->from('fitting_schedules fs');
		$this->db->join('order_fitting of', 'of.id = fs.order_fitting_id');
    	$this->db->join('sales_orders so', 'so.id = of.order_id');
		$this->db->join('users u', 'u.id = fs.user_id');
		$this->db->join('customers cm', 'cm.id = so.customer_id');
    	$this->db->where ( "fs.user_id='".$userId."' and of.order_id='".$orderId."'" );
    	$query = $this->db->get ();
    	// echo $this->db->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
		}	
   function distance($lat1,$lon1) {
	  
        $this->db->select("id,distance('".$lat1."','".$lon1."',latitude,longitude) as searchdistance");
    	$this->db->from('saloons');    	
    	$this->db->where ( "status='Active'" );
    	$query = $this->db->get ();
    	 //echo $this->db->last_query();die;
    	if ($query->num_rows () > 0) {
    		$resultData = $query->result_array ();
    	} else {
    		$resultData = false;
    	}
    	return $resultData;
    }
  
	function sendpush($token,$msg,$orderId,$type){
		
			$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
					$token = $token;
					$message = $msg;
					$fields = array (
						'to' => $token,
						'data' => array (
							'title' => 'Mode',
							'body' => $message,
							'orderId' => $orderId,
							'notificationType' => $type,
							'message' => $message
						),
						'priority' => 'high'
					);
					$headers = array (						'Authorization:key=AAAAUPcF9Xk:APA91bFuWiVHAmkQw-o99lt27ATKBrJoKVrCcnj7O1D-DvreTzhCA_f0L11li_zscJ0aVWV46faNxcyMF6-GUw6e5QCMpAQgKtjoy589m3wRhOs0KI3_E7fKjvQi0JcSGkf9p2vA7cWk',
					'Content-Type:application/json'
					);
					$ch = curl_init ();
					curl_setopt ( $ch, CURLOPT_URL, $path_to_firebase_cm );
					curl_setopt ( $ch, CURLOPT_POST, true );
					curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
					curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
					//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, true );
					curl_setopt ( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
					curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
					$output = curl_exec ( $ch );
					
				  //print_r($output);die;
					
			curl_close ( $ch );
			return true;
} 
     
}