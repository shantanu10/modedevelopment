<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model("admin_model");		
		$this->load->helper("url");
		$this->load->helper('cookie');
    }

	
    public function index(){
		
        $session_check = $this->session->has_userdata('user_id');
        if ($session_check != 1) {//echo "ok";die;
            $this->load->view('login');
        } else {
            redirect('dashboard');
        }    	
    }

	public function login_to_system(){
		
        $session_check = $this->session->has_userdata('user_id');
		
        if ($session_check != 1) {
             $email = $this->input->post("login_email");
            $password = $this->input->post("login_password"); 
		
            if ($email != '') {				
					  $result = $this->admin_model->validateLogin($email, $password);
                if (!empty($result)) {                
                    $sess_data = array(
                           'user_id' => $result['id'],
                           'email' => $result['email'],
						   'name' => $result['name'],                                                 
                    );
                    $this->session->set_userdata($sess_data);  
                 if ($this->input->post("remember"))
                    {
                        $this->input->set_cookie('login_email', $email, 86500); /* Create cookie for store emailid */
                        $this->input->set_cookie('login_password', $password, 86500); /* Create cookie for password */
                        
                    }
                    else
                    {
                        delete_cookie('login_email'); /* Delete email cookie */
                        delete_cookie('login_password'); /* Delete password cookie */
                    }               
                    redirect('dashboard');
                } else {
                    $this->session->set_flashdata('errorlogin', 'Invalid Email or Password');					
                    redirect('login');
					
                }
            }
        } else {            
            redirect('login');
        }       
    }


    public function forget_pass(){
		 $session_check = $this->session->has_userdata('user_id');
		if($session_check !=1){
        $result = $this->admin_model->check_email_exist();
		}else{
		$result = "";
	    }
		
        if($result){
        	$this->session->set_flashdata('errorlogin', 'Re-set pasword link has been sent to your email id');
        	redirect('login');
        } else {
       $this->session->set_flashdata('errorlogin', 'Invalid Email');
       redirect('login');
        }
    }
    public function reset_pass(){
        $data = array();
        if(!empty($this->input->post('re_password')) && !empty($this->input->post('key')) && !empty($this->input->post('reset'))){
           
			  
			$result = $this->admin_model->update_password();
			
            if($result){
                $this->session->set_flashdata('errorlogin', 'Pasword has been re-set successfully');
        	     redirect('login');
            } else{
                $this->session->set_flashdata('errorlogin', 'Try again');
        	     redirect('login');
            }
        }
        else
        {
            $reset = base64_decode($this->input->get('reset'));
            $reset_arr = explode('||timestamp=', $reset);
            $today_time = time();
            //echo '<br>';
            //echo $reset_arr[1];
            $check_time = $today_time - $reset_arr[1];
            if($check_time > (24*60*60)) {
                $data['error_msg']= 1;
                $this->load->view('reset_pass',$data);
            }
            else
            {
				$model = 'admin_model';
				
                $result = $this->$model->check_email_exist(TRUE,$reset_arr[0]);
                if($result) $this->load->view('reset_pass');
                else show_404();
            }
        }
    }
	public function logout(){
        $this->session->sess_destroy();
		// $data['errormsg'] = 'You have successfully logged out';
       // header("location:" . base_url('Login') . "");
        redirect('login');
    }

    public function proceedforcelogin() {
        $sess_id = $this->input->post('sess_id');

        $data_sess = $this->db
                            ->select(array('user_id', 'session_id'))
                            ->from('user_sessions')
                            ->where('id', $sess_id)
                            ->get()
                            ->first_row();
        
        $this->db
            ->set('session_end', strtotime(to_db_date()))
            ->set('force_logout', '1')
            ->where('id', $sess_id)
            ->update('user_sessions');

        $this->db
            ->where('id', $data_sess->session_id)
            ->delete('ci_sessions');
            
        $result = $this->user->validateLogin('', '', $data_sess->user_id);

        $data_sess = $this->db
                            ->select(array('id', 'session_last_active', 'ip_address'))
                            ->from('user_sessions')
                            ->where('user_id', $result['id'])
                            ->where('session_end', '0')
                            ->order_by('session_last_active', 'DESC')
                            ->get()
                            ->first_row();

        if (isset($data_sess->session_last_active)) {
            $time_gap = strtotime(to_db_date()) - $data_sess->session_last_active;
            if ($time_gap < 300000) {
                $str_temp = 'Oops! It seems that you are already working on other device (IP Address : ' . $data_sess->ip_address . '). To end the existing session <a href="javascript: proceedForceLogIn(\'' . $data_sess->id . '\');"><b>Click Here</b></a>, and start using our sevices here.';
                //die($str_temp);
                $this->session->set_flashdata('message_name', $str_temp);
                redirect('login');
            }

        }
        
        $sess_data = array(
               'user_id'               => $result['id'],
               'username'              => $result['username'],
               'user_type'             => $result['bean_module'],
               'agent_code'            => $result['agent_code'],
               'transaction_password'  => $result['transaction_password'],
        );
        $this->session->set_userdata($sess_data);

        $this->db
            ->set('id', create_guid())
            ->set('session_id', session_id())
            ->set('user_id', $this->session->userdata('user_id'))
            ->set('session_start', strtotime(to_db_date()))
            ->set('session_last_active', strtotime(to_db_date()))
            ->set('ip_address', $this->input->ip_address())
            ->insert('user_sessions');

        redirect('dashboard');
    }
}
