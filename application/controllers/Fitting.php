<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fitting extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper("mode");
		$this->load->model("admin_model");
		$this->load->model("dashboard_model");
		$this->load->helper("url");
		$this->load->helper('form');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
		$this->load->library('upload');
		session_check();
		getusername();
		date_default_timezone_set('Asia/Riyadh');
	}

	public function index()
	{
		$table = 'order_fitting';		
		$where = "1 order by id desc";
        $data['fittings'] = $this->admin_model->getDataById($table,$where);
		$wherefitter = "status='Active' and userType='2' order by name asc";
        $data['fitters'] = $this->admin_model->getDataById('users',$wherefitter);
		
		$this->load->view('fitting', $data);
	}
 
  	
	public function assignfitting(){
	 $id = $_POST['id'];
     $orderNumber = $_POST['orderNumber'];
	 $data['orderNumber'] = $orderNumber;
	   $table = 'order_fitting';
	   $where = "id='".$id."'";
       $data['fittingdetails'] = $this->admin_model->getDataById($table,$where);
	    
		$whereassign = "order_fitting_id='".$id."'";
       $data['assigndetails'] = $this->admin_model->getDataById('fitting_schedules',$whereassign);
	  // print_r($data['assigndetails']);
		$this->load->view('assignfitting', $data);
	   }
	public function saveassignfitting(){
		 $user_id = $_POST['user_id'];
		$assign_date = $_POST['assign_date'];
		$fitting_date = $_POST['fitting_date'];
		
		$id = explode(',',$_POST['id']);
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('user_id'=>$user_id,'order_fitting_id'=>$id[$i],'assign_date'=>$assign_date,'fitting_date'=>$fitting_date,'created_at'=>date('Y-m-d H:i:s'));
		
	   $saveassign[$i] = $this->db->insert('fitting_schedules',$data[$i]);
	    $whereorder[$i] = "id='".$id[$i]."'";
        $order_id[$i] = $this->admin_model->getDataById('order_fitting',$whereorder[$i]);
	   /*$datastage[$i] = array('stageId'=>14);
		$this->db->where('id',$order_id[$i][0]['order_id']);	
		$saveorder[$i] = $this->db->update('sales_orders',$datastage[$i]);*/
		$datastage[$i] = array('stageId'=>14);
		$this->db->where('id',$id[$i]);	
		$saveorderdelivery[$i] = $this->db->update('order_fitting',$datastage[$i]);
		$datalog[$i] = array('stageId'=>14,'order_id'=>$order_id[$i][0]['order_id'],'creationDateTime'=>date('Y-m-d H:i:s'));
		$savelog[$i] = $this->db->insert('stages_log',$datalog[$i]);
		
		$where[$i] = "id='".$order_id[$i][0]['order_id']."'";
       $orderdata[$i] = $this->admin_model->getDataById('sales_orders',$where[$i]);
		$wherecust[$i] = "id='".$orderdata[$i][0]['customer_id']."'";
       $customerdata[$i] = $this->admin_model->getDataById('customers',$wherecust[$i]);
	   $token[$i] = $customerdata[$i][0]['fcm_token'];
		$message[$i] = "Your order(Number:".$orderdata[$i][0]['orderNumber'].") has been assigned to Fitter person.";
		 $this->admin_model->sendpush($token[$i],$message[$i],$order_id[$i][0]['order_id'],'ORDER_DETAILS_SCREEN');
		 $fitter = "id='".$user_id."'";
        $fitterdata = $this->admin_model->getDataById('users',$fitter);
		$fittertoken = $fitterdata[0]['fcm_token'];
		$messagefitting = "An order(Number:".$orderdata[$i][0]['orderNumber'].") has been assigned to you for fitting.";
		 $this->admin_model->sendpush($fittertoken,$messagefitting,$order_id[$i][0]['order_id'],'ORDER_DETAILS_SCREEN'); 	
		}
	  if ($saveassign) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	   
   public function fittingschedules()
	{
        $data['schedules'] = $this->admin_model->fittingorderlist();
		
		$this->load->view('fittingschedules', $data);
	}
}
