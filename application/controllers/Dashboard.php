<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper("mode");
		$this->load->model("admin_model");
		$this->load->model("dashboard_model");
		$this->load->helper("url");
		$this->load->helper('form');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
		$this->load->library('upload');
		session_check();
		getusername();
		date_default_timezone_set('Asia/Kolkata');
	}

	public function index()
	{
		$data['name'] = $this->session->userdata('name');
		$customer = $this->db->select('count(*) as totalcustomer')->from('customers')->get();
		$customerdata = $customer->result_array('array');
        $data['customercount'] = $customerdata[0]['totalcustomer'];
		$order = $this->db->select('count(*) as totalorder')->from('sales_orders')->get();
		$orderdata = $order->result_array('array');
        $data['ordercount'] = $orderdata[0]['totalorder']; 
		 
		$this->load->view('dashboard', $data);
	}

	public function adminusers()
	{
		$data['userlist'] = $this->admins_model->getdetails('admins', 1);
		$this->load->view('webuser/userlist', $data);
	}
	public function change_password()
	{

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('oldpassword', 'Old Password', 'required');
		$this->form_validation->set_rules('newpassword', 'New Password', 'required');
		$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->load->view('change_password');
		} else {

			$this->admin_model->savepassword();
			redirect('dashboard/change_password');
		}
	}
	 public function order_type(){
		$table = 'order_type';
		$where = $this->db->select('*')->from('order_type')->order_by('id','ASC')->get();
		$data['ordertypes'] = $where->result_array('array');
		$this->load->view('order_type', $data);
	}
	public function ajaxeditordertypeimage(){
		$id = $_POST['id'];	
	   $table = 'order_type';
	   $where = "id='".$id."'";
       $data['ordertypes'] = $this->admin_model->getDataById($table,$where);	  
		$this->load->view('ajaxeditordertypeimage', $data);
	   } 
	public function updateordertypeimage(){
	   $id = $_POST['id'];
	   // Count total files
      $countfiles = count($_FILES['image']['name']);
   // Looping all files
    for($i=0;$i<$countfiles;$i++){
     $filename = time().str_replace(' ','',$_FILES['image']['name'][$i]);
 
  // Upload file
  move_uploaded_file($_FILES['image']['tmp_name'][$i],'uploads/order_type/'.$filename);
  $dataimg = array('orderTypeId'=>$id,'image'=>$filename);
  $saveimage = $this->db->insert('order_type_images',$dataimg);
 }
	  if ($saveimage) {
            echo 1;
        } else {
            echo 0;
        }
	}
	public function ajaxordertypeimages(){
	$id = $_POST['id'];
	$data['id'] = $id;	
	   $table = 'order_type_images';
	   $where = "orderTypeId='".$id."'";
       $data['imagedata'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('ajaxordertypeimages', $data);
	}
	 public function deleteallordertypeimage(){
		$id = $_POST['id'];
		$where = "orderTypeId='".$id."'";
		$images = $this->admin_model->getDataById('order_type_images',$where);
		
		for($i=0;$i<count($images);$i++){
			if($images[$i]['image']!=''){
		unlink($_SERVER["DOCUMENT_ROOT"].'/uploads/order_type/'.$images[$i]['image']);
			}
        $res[$i] = $this->admin_model->delete('order_type_images', $images[$i]['id']);
		}
		//print_r($res);exit;
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
	public function deleteordertypeimage(){
		$id = $_POST['id'];
		$where = "id='".$id."'";
		$images = $this->admin_model->getDataById('order_type_images',$where);
		//print_r($images);exit;
		if($images[0]['image']!=''){
		unlink($_SERVER["DOCUMENT_ROOT"].'/uploads/order_type/'.$images[0]['image']);
			}
        $res = $this->admin_model->delete('order_type_images', $images[0]['id']);
		echo 1;
       
		}
    public function stages(){
		$table = 'sales_order_stages';
		
		$where = $this->db->select('*')->from('sales_order_stages')->order_by('stageId','ASC')->get();
		$data['stages'] = $where->result_array('array');
		$this->load->view('stages', $data);
	}
	public function ajaxeditstage(){
		$id = $_POST['id'];	
	   $table = 'sales_order_stages';
	   $where = "stageId='".$id."'";
       $data['emps'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('ajaxeditstage', $data);
	   }
	public function updatestage(){
	   $stageId = $_POST['stageId'];
	   $name = $_POST['name'];
	   $shortName = $_POST['shortName'];
	   $stageMediaTitle = $_POST['stageMediaTitle'];
	   $stageMediaSubtitle = $_POST['stageMediaSubtitle'];
	  
	   $data = array('name'=>$name,'shortName'=>$shortName,'stageMediaTitle'=>$stageMediaTitle,'stageMediaSubtitle'=>$stageMediaSubtitle);
	   $this->db->where('stageId',$stageId);	
	   $saveemployee = $this->db->update('sales_order_stages',$data);
	  if ($saveemployee) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	
	public function items()
	{
		$table = 'items';
		
		$where = "1 order by id desc";
        $data['itemcodes'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('items', $data);
	}
   public function itemcodechangestatus()
	{
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$savecustomer[$i] = $this->db->update('items',$data[$i]);
		}
		if ($savecustomer) {
            echo 1;
        } else {
            echo 0;
        }
		
	}
	public function deleteitemcode(){
		$id = explode(',',$_POST['id']);
		
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('items', $id[$i]);
		}
		
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
   public function saveitemcode(){
	   $itemCode = $_POST['itemCode'];
	   $data = array('itemCode'=>$itemCode);
	   $saveitem = $this->db->insert('items',$data);
	  if ($saveitem) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	public function ajaxedititemcode(){
		$id = $_POST['id'];	
	   $table = 'items';
	   $where = "id='".$id."'";
       $data['emps'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('ajaxedititemcode', $data);
	   }
	public function updateitemcode(){
	   $id = $_POST['id'];
	   $itemCode = $_POST['itemCode'];
	  
	   $data = array('itemCode'=>$itemCode);
	   $this->db->where('id',$id);	
	   $saveitem = $this->db->update('items',$data);
	  if ($saveitem) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	public function contentchangestatus()
	{
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$savecustomer[$i] = $this->db->update('display_contents',$data[$i]);
		}
		if ($savecustomer) {
            echo 1;
        } else {
            echo 0;
        }
	}
	
	public function images(){
		$table = 'display_contents';		
		$where = $this->db->select('*')->from('display_contents')->where('file_type','image')->order_by('id','ASC')->get();
		$data['image_details'] = $where->result_array('array');
		$where = $this->db->select('*')->from('sales_order_stages')->order_by('stageId','ASC')->get();
		$data['stages'] = $where->result_array('array');
		$where = "1 order by id desc";
        $data['itemcodes'] = $this->admin_model->getDataById('items',$where);
		$this->load->view('images', $data);
		}
	
   
    public function ajaximages(){
	$id = $_POST['id'];
	$data['id'] = $id;	
	   $table = 'image_videos';
	   $where = "display_content_id='".$id."'";
       $data['imagedata'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('ajaximages', $data);
	}
    public function ajaxeditdisplaycontentimage(){
		$id = $_POST['id'];	
	   $table = 'display_contents';
	   $where = "id='".$id."'";
       $data['content'] = $this->admin_model->getDataById($table,$where);
	   $where = $this->db->select('*')->from('sales_order_stages')->order_by('stageId','ASC')->get();
		$data['stages'] = $where->result_array('array');
		$where = "1 order by id desc";
        $data['itemcodes'] = $this->admin_model->getDataById('items',$where);
		$this->load->view('ajaxeditdisplaycontentimage', $data);
	   } 
    public function saveimages(){
	   $content_type = $_POST['content_type'];
	   $itemId = $_POST['itemId'];
	   $stageId = $_POST['stageId'];
	   // Count total files
      $countfiles = count($_FILES['image_video']['name']);
      $data = array('file_type'=>'image','content_type'=>$content_type,'itemId'=>$itemId,'stageId'=>$stageId,'created_at'=>date('Y-m-d H:i:s'));
	  $saveitem = $this->db->insert('display_contents',$data);
	  $insert_id = $this->db->insert_id();
 // Looping all files
    for($i=0;$i<$countfiles;$i++){
     $filename = time().str_replace(' ','',$_FILES['image_video']['name'][$i]);
 
  // Upload file
  move_uploaded_file($_FILES['image_video']['tmp_name'][$i],'uploads/image_videos/'.$filename);
  $dataimg = array('display_content_id'=>$insert_id,'image_video'=>$filename);
  $saveimage = $this->db->insert('image_videos',$dataimg);
 }

	  if ($saveitem) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	   
    public function updatedisplaycontentimage(){
	   $id = $_POST['id'];
	   // Count total files
      $countfiles = count($_FILES['image_video']['name']);
   // Looping all files
    for($i=0;$i<$countfiles;$i++){
     $filename = time().str_replace(' ','',$_FILES['image_video']['name'][$i]);
 
  // Upload file
  move_uploaded_file($_FILES['image_video']['tmp_name'][$i],'uploads/image_videos/'.$filename);
  $dataimg = array('display_content_id'=>$id,'image_video'=>$filename);
  $saveimage = $this->db->insert('image_videos',$dataimg);
 }

	  if ($saveimage) {
            echo 1;
        } else {
            echo 0;
        }
	   }
    public function deleteallimage(){
		$id = $_POST['id'];
		$where = "display_content_id='".$id."'";
		$images = $this->admin_model->getDataById('image_videos',$where);
		
		for($i=0;$i<count($images);$i++){
			if($images[$i]['image_video']!=''){
		unlink($_SERVER["DOCUMENT_ROOT"].'/uploads/image_videos/'.$images[$i]['image_video']);
			}
        $res[$i] = $this->admin_model->delete('image_videos', $images[$i]['id']);
		}
		//print_r($res);exit;
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
	public function deleteimage(){
		$id = $_POST['id'];
		$where = "id='".$id."'";
		$images = $this->admin_model->getDataById('image_videos',$where);
		//print_r($images);exit;
		if($images[0]['image_video']!=''){
		unlink($_SERVER["DOCUMENT_ROOT"].'/uploads/image_videos/'.$images[0]['image_video']);
			}
        $res = $this->admin_model->delete('image_videos', $images[0]['id']);
		echo 1;
       
		}
	public function deletecontent(){
		$id = explode(',',$_POST['id']);
		$where = "display_content_id IN ('".$_POST['id']."')";
		$existdata = $this->admin_model->getDataById('image_videos',$where);
		if(!empty($existdata)){
			echo 2;
		}else{
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('display_contents', $id[$i]);
		}
		
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
		}	
		
	public function imagechangestatus()
	{
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$save[$i] = $this->db->update('display_contents',$data[$i]);
		}
		if ($save) {
            echo 1;
        } else {
            echo 0;
        }
		
	}	
	/*VIDEO*/
	public function videos(){
		$table = 'display_contents';		
		$where = $this->db->select('*')->from('display_contents')->where('file_type','video')->order_by('id','ASC')->get();
		$data['video_details'] = $where->result_array('array');
		$where = $this->db->select('*')->from('sales_order_stages')->order_by('stageId','ASC')->get();
		$data['stages'] = $where->result_array('array');
		$where = "1 order by id desc";
        $data['itemcodes'] = $this->admin_model->getDataById('items',$where);
		$this->load->view('videos', $data);
		}
	 public function savevideos(){
	    $content_type = $_POST['content_type'];
	   $itemId = $_POST['itemId'];
	   $stageId = $_POST['stageId'];
	   // Count total files
      $countfiles = count($_FILES['image_video']['name']);
      $data = array('file_type'=>'video','content_type'=>$content_type,'itemId'=>$itemId,'stageId'=>$stageId);
	 
	  $saveitem = $this->db->insert('display_contents',$data);
	  $insert_id = $this->db->insert_id();
	  
 // Looping all files
    for($i=0;$i<$countfiles;$i++){
     $filename = time().str_replace(' ','',$_FILES['image_video']['name'][$i]);
 
  // Upload file
  move_uploaded_file($_FILES['image_video']['tmp_name'][$i],'uploads/image_videos/'.$filename);
  $datavideo = array('display_content_id'=>$insert_id,'image_video'=>$filename);
  $savevideo = $this->db->insert('image_videos',$datavideo);
 }
	  if ($saveitem) {
            echo 1;
        } else {
            echo 0;
        }
	   }
   public function ajaxvideos(){
	   $id = $_POST['id'];
	   $data['id'] = $id;	
	   $table = 'image_videos';
	   $where = "display_content_id='".$id."'";
       $data['videodata'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('ajaxvideos', $data);
	}
	public function deleteallvideo(){
		$id = $_POST['id'];
		$where = "display_content_id='".$id."'";
		$images = $this->admin_model->getDataById('image_videos',$where);
		
		for($i=0;$i<count($images);$i++){
			if($images[$i]['image_video']!=''){
		unlink($_SERVER["DOCUMENT_ROOT"].'/uploads/image_videos/'.$images[$i]['image_video']);
			}
        $res[$i] = $this->admin_model->delete('image_videos', $images[$i]['id']);
		}
		//print_r($res);exit;
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
	public function deletevideo(){
		$id = $_POST['id'];
		$where = "id='".$id."'";
		$images = $this->admin_model->getDataById('image_videos',$where);
		//print_r($images);exit;
		if($images[0]['image_video']!=''){
		unlink($_SERVER["DOCUMENT_ROOT"].'/uploads/image_videos/'.$images[0]['image_video']);
			}
        $res = $this->admin_model->delete('image_videos', $images[0]['id']);
		echo 1;
       
		}
	public function ajaxeditdisplaycontentvideo(){
		$id = $_POST['id'];	
	   $table = 'display_contents';
	   $where = "id='".$id."'";
       $data['content'] = $this->admin_model->getDataById($table,$where);
	   $where = $this->db->select('*')->from('sales_order_stages')->order_by('stageId','ASC')->get();
		$data['stages'] = $where->result_array('array');
		$where = "1 order by id desc";
        $data['itemcodes'] = $this->admin_model->getDataById('items',$where);
		$this->load->view('ajaxeditdisplaycontentvideo', $data);
	   } 
    public function updatedisplaycontentvideo(){
	   $id = $_POST['id'];
	   // Count total files
      $countfiles = count($_FILES['image_video']['name']);
   // Looping all files
    for($i=0;$i<$countfiles;$i++){
     $filename = time().str_replace(' ','',$_FILES['image_video']['name'][$i]);
 
  // Upload file
  move_uploaded_file($_FILES['image_video']['tmp_name'][$i],'uploads/image_videos/'.$filename);
  $datavideo = array('display_content_id'=>$id,'image_video'=>$filename);
  $savevideo = $this->db->insert('image_videos',$datavideo);
 }

	  if ($savevideo) {
            echo 1;
        } else {
            echo 0;
        }
	   }
    public function videochangestatus()
	{
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$save[$i] = $this->db->update('display_contents',$data[$i]);
		}
		if ($save) {
            echo 1;
        } else {
            echo 0;
        }
		
	}	
	
	public function contracts(){
		$where = $this->db->select('c.*,so.orderNumber')->from('contracts c')->join('sales_orders so','c.order_id=so.id')->order_by('c.id','DESC')->get();
		$data['contracts'] = $where->result_array('array');
		$whereorder = $this->db->select('id,orderNumber')->from('sales_orders')->order_by('id','ASC')->get();
		$data['orders'] = $whereorder->result_array('array');
		
		$this->load->view('contracts', $data);
		}
	public function savecontract(){
	   $order_id = $_POST['order_id'];
      $filename = time().str_replace(' ','',$_FILES['contract']['name']);
	$where = "order_id='".$order_id."'";
	$contracts = $this->admin_model->getDataById('contracts',$where);
	if(empty($contracts)){
  // Upload file
  move_uploaded_file($_FILES['contract']['tmp_name'],'uploads/contract/'.$filename);
  $datacontract = array('order_id'=>$order_id,'contract'=>$filename,'created_at'=>date('Y-m-d H:i:s'));
  $savecontract = $this->db->insert('contracts',$datacontract);
	}else{
		echo 2;
		exit;
		}
	  if ($savecontract) {
            echo 1;
        } else {
            echo 0;
        }
	   }
   public function deletecontract(){
		$id = explode(',',$_POST['id']);
		
		for($i=0;$i<count($id);$i++){
		$where = "id='".$id[$i]."'";
		$contracts = $this->admin_model->getDataById('contracts',$where);
		
		if($contracts[0]['contract']!=''){
		unlink($_SERVER["DOCUMENT_ROOT"].'/uploads/contract/'.$contracts[0]['contract']);
			}
        $res[$i] = $this->admin_model->delete('contracts', $id[$i]);
		}
		
            echo 1;
       
		}
  public function ajaxeditcontract(){
		$id = $_POST['id'];	
	   $where = $this->db->select('c.*,so.orderNumber')->from('contracts c')->join('sales_orders so','c.order_id=so.id')->where('c.id',$id)->order_by('c.id','DESC')->get();
		$data['contracts'] = $where->result_array('array');
	   $whereorder = $this->db->select('id,orderNumber')->from('sales_orders')->order_by('id','ASC')->get();
		$data['orders'] = $whereorder->result_array('array');
		
		$this->load->view('ajaxeditcontract', $data);
	   } 
    public function updatecontract(){
	   $id = $_POST['id'];
   // Looping all files
     $filename = time().str_replace(' ','',$_FILES['contract']['name']);
// echo $filename;exit;
  // Upload file
  move_uploaded_file($_FILES['contract']['tmp_name'],'uploads/contract/'.$filename);
  $datacontract = array('contract'=>$filename);
  $this->db->where('id',$id);	
  $savecontract = $this->db->update('contracts',$datacontract);

	  if ($savecontract) {
            echo 1;
        } else {
            echo 0;
        }
	   }	   
}
