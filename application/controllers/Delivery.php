<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Delivery extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper("mode");
		$this->load->model("admin_model");
		$this->load->model("dashboard_model");
		$this->load->helper("url");
		$this->load->helper('form');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
		$this->load->library('upload');
		session_check();
		getusername();
		date_default_timezone_set('Asia/Riyadh');
	}

	public function index()
	{
		$table = 'order_delivery';		
		$where = "1 order by id desc";
        $data['deliveries'] = $this->admin_model->getDataById($table,$where);
		$wheredeliveryman = "status='Active' and userType='1' order by name asc";
        $data['deliveryman'] = $this->admin_model->getDataById('users',$wheredeliveryman);
		
		$this->load->view('delivery', $data);
	}
 
  	
	public function assigndelivery(){
	 $id = $_POST['id'];
     $orderNumber = $_POST['orderNumber'];
	 $data['orderNumber'] = $orderNumber;
	   $table = 'order_delivery';
	   $where = "id='".$id."'";
       $data['deliverydetails'] = $this->admin_model->getDataById($table,$where);
	    
		$whereassign = "delivery_id='".$id."'";
       $data['assigndetails'] = $this->admin_model->getDataById('delivery_schedules',$whereassign);
	  // print_r($data['assigndetails']);
		$this->load->view('assigndelivery', $data);
	   }
	public function saveassigndelivery(){
		 $user_id = $_POST['user_id'];
		$assign_date = $_POST['assign_date'];
		$delivery_date = $_POST['delivery_date'];
		
		$id = explode(',',$_POST['id']);
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('user_id'=>$user_id,'delivery_id'=>$id[$i],'assign_date'=>$assign_date,'delivery_date'=>$delivery_date,'created_at'=>date('Y-m-d H:i:s'));
		
	   $saveassign[$i] = $this->db->insert('delivery_schedules',$data[$i]);
	    $whereorder[$i] = "id='".$id[$i]."'";
        $order_id[$i] = $this->admin_model->getDataById('order_delivery',$whereorder[$i]);
	   $datastage[$i] = array('stageId'=>8);
		$this->db->where('id',$order_id[$i][0]['order_id']);	
		$saveorder[$i] = $this->db->update('sales_orders',$datastage[$i]);
		$this->db->where('id',$id[$i]);	
		$saveorderdelivery[$i] = $this->db->update('order_delivery',$datastage[$i]);
		$datalog[$i] = array('stageId'=>8,'order_id'=>$order_id[$i][0]['order_id'],'creationDateTime'=>date('Y-m-d H:i:s'));
		$savelog[$i] = $this->db->insert('stages_log',$datalog[$i]);
		
		$where[$i] = "id='".$order_id[$i][0]['order_id']."'";
       $orderdata[$i] = $this->admin_model->getDataById('sales_orders',$where[$i]);
		$wherecust[$i] = "customer_id='".$orderdata[$i][0]['customer_id']."'";
       $customerdata[$i] = $this->admin_model->getDataById('customer_tokens',$wherecust[$i]);
	   $token[$i] = $customerdata[$i][0]['fcm_token'];
		$message[$i] = "Your order(Number:".$orderdata[$i][0]['orderNumber'].") has been assigned to delivery person.";
		 $this->admin_model->sendpush($token[$i],$message[$i],$order_id[$i][0]['order_id'],'ORDER_DETAILS_SCREEN');
		$userdata = "id='".$user_id."'";
        $userdataarr = $this->admin_model->getDataById('users',$userdata);
		$fcmtoken = $userdataarr[0]['fcm_token'];
		$messagedelivery = "An order(Number:".$orderdata[$i][0]['orderNumber'].") has been assigned to you for delivery.";
		 $this->admin_model->sendpush($fcmtoken,$messagedelivery,$order_id[$i][0]['order_id'],'ORDER_DETAILS_SCREEN'); 	
		}
	  if ($saveassign) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	   
   public function schedules()
	{
        $data['schedules'] = $this->admin_model->deliveryorderlist();
		
		$this->load->view('schedules', $data);
	}
}
