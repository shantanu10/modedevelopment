<?php
defined('BASEPATH') or exit('No direct script access allowed');
include ('application/libraries/REST_Controller.php');
class Webservice extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        // load the pdo for db connection
        $this->db = $this->load->database();
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->helper('url');
        // load the models
        $this->load->model('Admin_model');
        $this->load->model('Dashboard_model');
		date_default_timezone_set('Asia/Riyadh');
		
    }
   function country_codes_post(){
	   $resArr['result'] = 0;
    	$resArr['message'] = "";
		$dataarr = json_decode(file_get_contents("php://input"), true);
		$header = $this->input->request_headers();
		$country = $this->db->select('countryCode,callingCode')->from('country_codes')->get();
		$countrydata = $country->result_array('array');
		foreach($countrydata as $country){
				
				$data['countryCode'] = $country['countryCode'];
				$data['callingCode'] = $country['callingCode'];
				$countryarr[] = $data;
		}
		$resArr ['result'] = 1;
		$resArr ['message'] = "Country Calling Code List";
		$resArr ['data'] = $countryarr;
		 echo $this->response($resArr, 200); 
	   }
   
   function validate_token_post(){
	   $resArr['result'] = 0;
    	$resArr['message'] = "";
		$dataarr = json_decode(file_get_contents("php://input"), true);	
		$userId =  $dataarr['userId'];
		$deviceId =  $dataarr['deviceId'];
		$token =  $dataarr['token'];
		$fcmToken =  $dataarr['fcmToken'];	
		
		$header = $this->input->request_headers();
		
		if(!$userId || !$token){
		$resArr['message'] = "Please enter your cellular";	
		}else{
			$where = "id='".$userId."'";
			$customer = $this->Admin_model->getDataById('customers',$where);
			$wheretoken = "customer_id='".$userId."' and token = '".$token."' and deviceId='".$deviceId."' order by id desc";
			$customertoken = $this->Admin_model->getDataById('customer_tokens',$wheretoken);
			
			if(!empty($customer) && !empty($customertoken)){
		    $datafcm = array('fcm_token'=>$fcmToken);
			$this->db->where('id',$customer[0]['id']);	
			$updatedata = $this->db->update('customers',$datafcm);
			$where = "id='".$userId."'";
			$customerdata = $this->Admin_model->getDataById('customers',$where);
			    $data['userId'] = $customerdata[0]['id'];
				if($header['Lang']=='ar'){
				$data['name'] = $customerdata[0]['foreignName'];
				$data['address'] = $customerdata[0]['foreignAddress'];
				}else{
					$data['name'] = $customerdata[0]['name'];
					$data['address'] = $customerdata[0]['address'];
					}
			    $data['callingCode'] = $customerdata[0]['callingCode'];
				$data['mobile'] = $customerdata[0]['mobile'];
				$data['altMobile'] = $customerdata[0]['altMobile'];
				$data['email'] = $customerdata[0]['email'];
				$data['birthDate'] = $customerdata[0]['birthDate'];
				$data['anniversaryDate'] = $customerdata[0]['anniversaryDate'];
				//$data['locationCoords'] = $customerdata[0]['locationCoords'];
				if($customerdata[0]['image']!=''){
				$data['image'] = base_url().'uploads/profile/'.$customerdata[0]['image'];
				}else{
				$data['image'] = base_url().'assets/img/user.png';	
					}
				//$data['token'] = $customerdata[0]['valid_token'];
					$resArr ['result'] = 1;
					$resArr ['message'] = "You are logged in";
					$resArr ['data'] = $data;
					
					
			}else{
				$resArr ['message'] = "This Cellular doesn't exist";	
            }
		    
		}
      echo $this->response($resArr, 200); 
	   }
   function send_otp_post(){
	   $resArr['result'] = 0;
    	$resArr['message'] = "";	
		$header = $this->input->request_headers();
		
		$dataarr = json_decode(file_get_contents("php://input"), true);
		$code = $dataarr['callingCode'];
		$mobile = $dataarr['mobile'];
		if($dataarr['mobile']==''){
		$resArr['message'] = "Please enter your mobile";	
		}else{
			$where = "mobile='".$mobile."' and status='Active'";
			$customer = $this->Admin_model->getDataById('customers',$where);
			
			if(!empty($customer)){
		
			$uniqueid = mt_rand(100000, 999999);
				//$pdata = new StdClass ();
				$number = $code.$mobile;
				$message = "Mode Customer OTP: $uniqueid";
				//$message [] = $pdata;
				$isSent = $this->Admin_model->getSMMMobile( $number,$message);
				
                if($isSent == true) {
					if($mobile!='9876543210'){
					$data = array('otp'=>$uniqueid,'callingCode'=>$code);
			$this->db->where('id',$customer[0]['id']);	
            $savecustomer = $this->db->update('customers',$data);
					}
					$resArr ['result'] = 1;
					$resArr ['message'] = "OTP sent successfully.";
					//$resArr ['otp'] = "$uniqueid";
					
					
				} else {
					$resArr ['result'] = 0;
					$resArr ['message'] = "OTP sending failed.Please try again.";
				}	
			}else{
				
			$resArr ['result'] = 0;
			$resArr ['message'] = "The Cellular doesn't exist.";	
				
			}
		    
		}
      echo $this->response($resArr, 200); 
	   }
  function validate_otp_post(){
	   $resArr['result'] = 0;
    	$resArr['message'] = "";
		$dataarr = json_decode(file_get_contents("php://input"), true);	
		$otp =  $dataarr['otp'];
		$deviceId =  $dataarr['deviceId'];
		$mobile =  $dataarr['mobile'];
		
		if(!$deviceId || !$otp || !$mobile){
		$resArr['message'] = "Please pass the parameters.";	
		}else{
			$where = "mobile='".$mobile."'";
			$customer = $this->Admin_model->getDataById('customers',$where);
			
		if(!empty($customer)){
		if($customer[0]['otp']==$otp){
			$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
  
           $valid_token = substr(str_shuffle($str_result),0, 16); 
			$data = array('customer_id'=>$customer[0]['id'],'deviceId'=>$deviceId,'token'=>$valid_token);		
            $savecustomerotken = $this->db->insert('customer_tokens',$data);
			
			$resArr ['result'] = 1;
			$resArr ['message'] = "Your OTP has been validated successfully.";	
			$resArr ['token'] = $valid_token;
			$resArr ['userId'] = $customer[0]['id'];	
		}else{
			$resArr ['result'] = 0;
			$resArr ['message'] = "Your given OTP is not valid";	
			}
			}else{
				
			$resArr ['result'] = 0;
			$resArr ['message'] = "This mobile doesn't exist.";	
				
			
			}
		}
      echo $this->response($resArr, 200); 
	   }
  function sales_orders_post(){
	   $resArr['result'] = 0;
    	$resArr['message'] = "";	
		$header = $this->input->request_headers();
		$dataarr = json_decode(file_get_contents("php://input"), true);
		$userId = $dataarr['userId'];
		$orderType = $dataarr['orderType'];
		if($userId!=''){
			$sales_order = $this->Admin_model->salesorderlist($userId,$orderType);
			if(!empty($sales_order)){
			foreach($sales_order as $order){
				$data['orderId'] = $order['id'];
				$data['orderType'] = $order['orderTypeId'];
				$data['orderNumber'] = $order['orderNumber'];
				/*$data['orderDate'] = $order['orderDate'];
				$data['orderDueDate'] = $order['orderDueDate'];
				$data['stageId'] = $order['stageId'];
				$data['currency'] = 'SAR';
				$data['vatAmount'] = $order['vatAmount'];
				$data['orderTotal'] = $order['orderTotal'];
				$data['status'] = $order['status'];*/
				$images = $this->db->select('image')->from('order_type_images')->where('orderTypeId',$data['orderType'])->get();
		$imagedata = $images->result_array('array');
		$imagedataarr = array();
		/*if(!empty($imagedata)){
			foreach($imagedata as $img){				
				$imgdata = base_url().'uploads/order_type/'.$img['image'];
				$imagedataarr[] = $imgdata;
				}
			}*/
			if(!empty($imagedata)){
			$typeimage = $imagedata[0]['image'];
			}else{
			$typeimage = '';	
				}
			$imagedataarr[] = base_url().'uploads/order_type/'.$typeimage;
			$data['images'] = $imagedataarr;	
			
			$contract = $this->db->select('contract')->from('contracts')->where('order_id',$data['orderId'])->get();
		$contractdata = $contract->result_array('array');
		$contractdataarr = '';
		$contractfile = '';
		if(!empty($contractdata)){
							
				$contractfile = base_url().'uploads/contract/'.$contractdata[0]['contract'];
				
			}else{
				$contractfile = '';
				}
			$data['contract'] = $contractfile;	
			
			$order_items = $this->db->select('id,itemCode,description,foreignDescription,quantity')->from('order_items')->where('orderNumber',$data['orderNumber'])->get();
		$order_itemsarr = $order_items->result_array('array');
		$itemdataarr = array();
		if(!empty($order_itemsarr)){
			foreach($order_itemsarr as $item){
				$itemdata['itemId'] = $item['id'];
				$itemdata['itemCode'] = $item['itemCode'];
				if($header['Lang']=='ar'){
				$itemdata['description'] = $item['foreignDescription'];
				}else{
					$itemdata['description'] = $item['description'];
					}
				
				$itemdata['quantity'] = $item['quantity'];
				$itemdataarr[] = $itemdata;
				}
			}
		$data['items'] = $itemdataarr;
		
		
		$where = "FIND_IN_SET('Customer', userGroup)";
		$sales_order_stages = $this->Admin_model->getDataById('sales_order_stages',$where);
		$datastage = [];		
		foreach($sales_order_stages as $stages){
				
			$wherelog = $this->db->select('stageId,creationDateTime,modifiedDateTime')->from('stages_log')->where('stageId',$stages['stageId'])->where('order_id',$data['orderId'])->get();
		$logdata = $wherelog->result_array('array');
		
		if(!empty($logdata)){
		if($logdata[0]['stageId']==$order['stageId']){
			$isCompleted = false; 
			$isCurrent = true; 
			}else{
				if($logdata[0]['stageId'] <= $order['stageId']){
				$isCompleted = true;	
				$isCurrent = false;
				}
				
			}
		$creationDateTime = $logdata[0]['creationDateTime'];
		$modifiedDateTime = $logdata[0]['modifiedDateTime'];
	$datastage = array('shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'uploads/stages/'.$stages['image'],'isCompleted'=>$isCompleted,'isCurrent'=>$isCurrent,'creationDateTime'=>$creationDateTime,'modifiedDateTime'=>$modifiedDateTime);
	/*if($stages['stageId'] >= 2 && $stages['stageId'] <=6){
				if($stages['stageId'] == $order['stageId']){
				  $isCompleted = false;
				   $isCurrent = true;
				 }else if($stages['stageId'] < $order['stageId']){
			
					$isCompleted = true; 
					 $isCurrent = false;
				 }else{
					$isCompleted = false; 
					 $isCurrent = false;
					 }
			$datastage = array('shortName'=>'Processing','name'=>'Processing Order','image'=>base_url().'uploads/stages/processing.png','isCompleted'=>$isCompleted,'isCurrent'=>$isCurrent,'creationDateTime'=>$order['created_at'],'modifiedDateTime'=>$order['modified_at']);
		}*/
		}else{
		 if($stages['stageId'] < $order['stageId']){
			
					$isCompleted = true; 
					 $isCurrent = false;
					
			 
	$datastage = array('shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'uploads/stages/'.$stages['image'],'isCompleted'=>$isCompleted,'isCurrent'=>$isCurrent,'creationDateTime'=>$order['created_at'],'modifiedDateTime'=>$order['modified_at']);	
			}else{
			$datastage = array('shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'uploads/stages/'.$stages['image'],'isCompleted'=>false,'isCurrent'=>false,'creationDateTime'=>'','modifiedDateTime'=>'');	
				}
				/*if($stages['stageId']==2 && $stages['stageId'] < 3){
				if($stages['stageId'] == $order['stageId']){
				  $isCompleted = false;
				   $isCurrent = true;
				 }else if($stages['stageId'] < $order['stageId']){
			
					$isCompleted = true; 
					 $isCurrent = false;
				 }else{
					$isCompleted = false; 
					 $isCurrent = false;
					 }
			$datastage = array('shortName'=>'Processing','name'=>'Processing Order','image'=>base_url().'uploads/stages/processing.png','isCompleted'=>$isCompleted,'isCurrent'=>$isCurrent,'creationDateTime'=>$order['created_at'],'modifiedDateTime'=>$order['modified_at']);
		}*/	
			}
			
		/*if($stages['stageId']==2 || $stages['stageId']==3 || $stages['stageId']==4 || $stages['stageId']==5 || $stages['stageId']==6){
			$datastage = $datastage;
		}else{*/
		if($stages['stageId']==2 || $stages['stageId']==3 || $stages['stageId']==4 || $stages['stageId']==5 || $stages['stageId']==6){
			$datastage = '';
		}
		if($stages['stageId']==$order['stageId'] && ($stages['stageId']==2 || $stages['stageId']==3 || $stages['stageId']==4 || $stages['stageId']==5 || $stages['stageId']==6)){
			$datastage = array('shortName'=>'Processing','name'=>'Processing Order','image'=>base_url().'uploads/stages/processing.png','isCompleted'=>false,'isCurrent'=>true,'creationDateTime'=>$order['created_at'],'modifiedDateTime'=>$order['modified_at']);
			
			}else if($stages['stageId']==2 && $order['stageId']==1){
				$datastage = array('shortName'=>'Processing','name'=>'Processing Order','image'=>base_url().'uploads/stages/processing.png','isCompleted'=>false,'isCurrent'=>false,'creationDateTime'=>$order['created_at'],'modifiedDateTime'=>$order['modified_at']);
			}else if($stages['stageId'] == 7 && $order['stageId']==$stages['stageId']){
				$datastage = array('shortName'=>'Processing','name'=>'Processing Order','image'=>base_url().'uploads/stages/processing.png','isCompleted'=>true,'isCurrent'=>false,'creationDateTime'=>$order['created_at'],'modifiedDateTime'=>$order['modified_at']);
				}
			$stagesdata[] = $datastage;
				
		//}
		//unset($stagesdata[1]);
		
		//$stagesdata[] = $datastage;
				}
				$stagesdata = array_values(array_filter($stagesdata));
			$data['stages'] = $stagesdata;
			$contentimage = $this->db->select('image_video')->from('display_contents dc')->join('image_videos iv','dc.id=iv.display_content_id')->where('dc.stageId',$order['stageId'])->where('dc.content_type','stage')->get();
		$contentimagedata = $contentimage->result_array('array');
		$dataimg = array();
		if(!empty($contentimagedata)){
			foreach($contentimagedata as $img){
			$fileUrl = base_url().'uploads/image_videos/'.$img['image_video'];
			$dataimg[] = $fileUrl;
			}
		}
		$wherepromo = "stageId='".$order['stageId']."'";
		$stagesdetails = $this->Admin_model->getDataById('sales_order_stages',$wherepromo);
		$data['stageMediaTitle'] = $stagesdetails[0]['stageMediaTitle'];
		$data['stageMediaSubtitle'] = $stagesdetails[0]['stageMediaSubtitle'];
		$data['stageMediaItems'] = $dataimg;
		
			
			
			$orderdata[] = $data;
				$datastage = '';
				$order_itemsarr = '';
				$contractdata = '';
				$imagedata = '';
				$contentimagedata = '';
				}
			$resArr ['result'] = 1;
			$resArr ['message'] = "Sales Order";
			$resArr ['data'] = $orderdata;
			}else{
			$resArr ['message'] = "No order available.";	
			}
		}else{
		$resArr ['message'] = "please pass the user id";		
		}
		
      echo $this->response($resArr, 200); 
	   }
    function stage_list_post(){
	   $resArr['result'] = 0;
    	$resArr['message'] = "";	
		$header = $this->input->request_headers();
		$dataarr = json_decode(file_get_contents("php://input"), true);
		$orderId = $dataarr['orderId'];
		    $whereorder = "id='".$orderId."'";
			$orderdata = $this->Admin_model->getDataById('sales_orders',$whereorder);
			$where = "1 order by stageId asc";
			$sales_order_stages = $this->Admin_model->getDataById('sales_order_stages',$where);
			$datastage = [];
			
			foreach($sales_order_stages as $stages){
				
			$wherelog = $this->db->select('stageId,creationDateTime,modifiedDateTime')->from('stages_log')->where('stageId',$stages['stageId'])->where('order_id',$orderId)->get();
		$logdata = $wherelog->result_array('array');
		
		if(!empty($logdata)){
		if($logdata[0]['stageId']==$orderdata[0]['stageId']){
			$isCompleted = false; 
			$isCurrent = true; 
			}else{
				if($logdata[0]['stageId'] <= $orderdata[0]['stageId']){
				$isCompleted = true;	
				}
			$isCurrent = false;	
			}
		$creationDateTime = $logdata[0]['creationDateTime'];
		$modifiedDateTime = $logdata[0]['modifiedDateTime'];
		$datastage = array('id'=>$stages['stageId'],'shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'assets/img/'.$stages['image'],'isCompleted'=>$isCompleted,'isCurrent'=>$isCurrent,'creationDateTime'=>$creationDateTime,'modifiedDateTime'=>$modifiedDateTime);
		}else{
		 $datastage = array('id'=>$stages['stageId'],'shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'assets/img/'.$stages['image']);	
			}
		
				$stagesdata[] = $datastage;
				$datastage = '';
				}
			$resArr ['result'] = 1;
			$resArr ['message'] = "Stages";
			$resArr ['data'] = $stagesdata;
		
      echo $this->response($resArr, 200); 
	   } 
 function order_details_post(){
	   $resArr['result'] = 0;
    	$resArr['message'] = "";	
		$header = $this->input->request_headers();
		$dataarr = json_decode(file_get_contents("php://input"), true);
		$orderId = $dataarr['orderId'];
		if($orderId!=''){
			$order = $this->Admin_model->salesorderdetails($orderId);
		      $data['orderId'] = $order[0]['id'];
				$data['orderType'] = $order[0]['orderTypeId'];
				$data['orderNumber'] = $order[0]['orderNumber'];
				$data['orderDate'] = $order[0]['orderDate'];
				$data['orderDueDate'] = $order[0]['orderDueDate'];
				$data['stageId'] = $order[0]['stageId'];
				$data['currency'] = 'SAR';
				$data['vatAmount'] = $order[0]['vatAmount'];
				$data['orderTotal'] = $order[0]['orderTotal'];
				$data['stageText'] = $order[0]['status'];
			$customerdata = $this->db->select('address,foreignAddress')->from('customers')->where('id',$order[0]['customer_id'])->get();
		    $customerdataarr = $customerdata->result_array('array');
			 if($header['Lang']=='ar'){
		       $data['address'] = $customerdataarr[0]['foreignAddress'];
			 }else{
			 $data['address'] = $customerdataarr[0]['address'];	 
			}
			$images = $this->db->select('image')->from('order_type_images')->where('orderTypeId',$data['orderType'])->get();
		$imagedata = $images->result_array('array');
		$imagedataarr = array();
		if(!empty($imagedata)){
			foreach($imagedata as $img){				
				$imgdata = base_url().'uploads/order_type/'.$img['image'];
				$imagedataarr[] = $imgdata;
				}
			}
			$data['images'] = $imagedataarr;	
			$imagedata = '';
			$contract = $this->db->select('contract')->from('contracts')->where('order_id',$orderId)->get();
		$contractdata = $contract->result_array('array');
		$contractdataarr = '';
		if(!empty($contractdata)){							
				$contractfile = base_url().'uploads/contract/'.$contractdata[0]['contract'];				
			}else{
			$contractfile = '';	
				}
			$data['contract'] = $contractfile;	
			$contractdata = '';
			$order_items = $this->db->select('id,itemCode,description,foreignDescription,quantity')->from('order_items')->where('orderNumber',$data['orderNumber'])->get();
		$order_itemsarr = $order_items->result_array('array');
		$itemdataarr = array();
		if(!empty($order_itemsarr)){
			foreach($order_itemsarr as $item){
				$itemdata['itemId'] = $item['id'];
				$itemdata['itemCode'] = $item['itemCode'];
				if($header['Lang']=='ar'){
				$itemdata['description'] = $item['foreignDescription'];
				}else{
					$itemdata['description'] = $item['description'];
					}
				
				$itemdata['quantity'] = $item['quantity'];
				$itemdataarr[] = $itemdata;
				}
			}
		$data['items'] = $itemdataarr;
		
		$where = "1 order by stageId asc";
		$sales_order_stages = $this->Admin_model->getDataById('sales_order_stages',$where);
		$datastage = [];		
		foreach($sales_order_stages as $stages){
				
			$wherelog = $this->db->select('stageId,creationDateTime,modifiedDateTime')->from('stages_log')->where('stageId',$stages['stageId'])->where('order_id',$data['orderId'])->get();
		$logdata = $wherelog->result_array('array');
		
		if(!empty($logdata)){
		if($logdata[0]['stageId']==$order[0]['stageId']){
			$isCompleted = false; 
			$isCurrent = true; 
			}else{
				if($logdata[0]['stageId'] <= $order[0]['stageId']){
				$isCompleted = true;	
				}
			$isCurrent = false;	
			}
		$creationDateTime = $logdata[0]['creationDateTime'];
		$modifiedDateTime = $logdata[0]['modifiedDateTime'];
		$datastage = array('shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'uploads/stages/'.$stages['image'],'isCompleted'=>$isCompleted,'isCurrent'=>$isCurrent,'creationDateTime'=>$creationDateTime,'modifiedDateTime'=>$modifiedDateTime);
		}else{
		 if($stages['stageId'] < $order[0]['stageId']){
		 $datastage = array('shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'uploads/stages/'.$stages['image'],'isCompleted'=>true,'isCurrent'=>false,'creationDateTime'=>$order[0]['created_at'],'modifiedDateTime'=>$order[0]['modified_at']);	
			}else{
			$datastage = array('shortName'=>$stages['shortName'],'name'=>$stages['name'],'image'=>base_url().'uploads/stages/'.$stages['image'],'isCompleted'=>false,'isCurrent'=>false,'creationDateTime'=>'','modifiedDateTime'=>'');	
				}	
			}
			if($stages['stageId']==3 && $stages['stageId'] < 4){
			$datastage = array('shortName'=>'Processing','name'=>'Processing Order','image'=>base_url().'uploads/stages/processing.png','isCompleted'=>true,'isCurrent'=>false,'creationDateTime'=>$order[0]['created_at'],'modifiedDateTime'=>$order[0]['modified_at']);
		}
		if($stages['stageId']==7 || $stages['stageId']==4 || $stages['stageId']==5 || $stages['stageId']==6 || $stages['stageId']==11){
			$datastage = '';
		}else{
			
				$stagesdata[] = $datastage;
		}
				}
				$stagesdata = array_filter($stagesdata);
			$data['stages'] = $stagesdata;
				$orderdata = $data;
				$order_itemsarr = '';
				
			$resArr ['result'] = 1;
			$resArr ['message'] = "Sales Order Details";
			$resArr ['data'] = $orderdata;
			
		}else{
		$resArr ['message'] = "please pass the order id";		
		}
		
      echo $this->response($resArr, 200); 
	   }
  function contract_post(){
	  $resArr['result'] = 0;
    	$resArr['message'] = "";	
		$header = $this->input->request_headers();
	    $dataarr = json_decode(file_get_contents("php://input"), true);
		$orderId = $dataarr['orderId'];
		if($orderId!=''){
		$contract = $this->db->select('contract')->from('contracts')->where('order_id',$orderId)->get();
		$contractdata = $contract->result_array('array');
		if($contractdata[0]['contract']!=''){
		$resArr ['result'] = 1;
			$resArr ['message'] = "Contract Details";
			$resArr ['contract'] = base_url().'uploads/contract/'.$contractdata[0]['contract'];
		}else{
		$resArr ['message'] = "No contract available.";	
		}
		}else{
		$resArr ['message'] = "Please pass the user id";	
			}
	  echo $this->response($resArr, 200); 
	  }
  function stage_media_list_post(){
	  $resArr['result'] = 0;
    	$resArr['message'] = "";	
		$header = $this->input->request_headers();
	    $dataarr = json_decode(file_get_contents("php://input"), true);
		$where = "1 order by stageId asc";
		$sales_order_stages = $this->Admin_model->getDataById('sales_order_stages',$where);
		$mediadata2 = [];
				$mediadata2['title'] = 'Latest';	
			$image = $this->db->select('image_video')->from('display_contents dc')->join('image_videos iv','dc.id=iv.display_content_id')->where('dc.content_type','stage')->order_by('dc.created_at','DESC')->limit(10)->get();
		$imagedata = $image->result_array('array');
		$dataimg = array();
		if(!empty($imagedata)){
			foreach($imagedata as $img){
			$fileUrl = base_url().'uploads/image_videos/'.$img['image_video'];
			$dataimg[] = $fileUrl;
			}
		}
		$mediadata2['items'] = $dataimg;
			
		foreach($sales_order_stages as $stages){
			
			$mediadata['title'] = $stages['shortName'];	
			$image = $this->db->select('image_video')->from('display_contents dc')->join('image_videos iv','dc.id=iv.display_content_id')->where('dc.stageId',$stages['stageId'])->where('dc.content_type','stage')->get();
		$imagedata = $image->result_array('array');
		$dataimg = array();
		if(!empty($imagedata)){
			foreach($imagedata as $img){
			$fileUrl = base_url().'uploads/image_videos/'.$img['image_video'];
			$dataimg[] = $fileUrl;
			}
		}
		$mediadata['items'] = $dataimg;		
		
			
			if($stages['stageId']==3 && $stages['stageId'] < 4){
			$mediadata['title'] = 'Processing';	
$image = $this->db->select('image_video')->from('display_contents dc')->join('image_videos iv','dc.id=iv.display_content_id')->where('dc.stageId=3 or dc.stageId=4 or dc.stageId=5 or dc.stageId=6 or dc.stageId=7')->where('dc.content_type','stage')->get();
		$imagedata = $image->result_array('array');
		$dataimg = array();
		if(!empty($imagedata)){
			foreach($imagedata as $img){
			$fileUrl = base_url().'uploads/image_videos/'.$img['image_video'];
			$dataimg[] = $fileUrl;
			}
		}
		$mediadata['items'] = $dataimg;
		}
		if($stages['stageId']==7 || $stages['stageId']==4 || $stages['stageId']==5 || $stages['stageId']==6 || $stages['stageId']==11){
			$mediadata = [];
		}else{
			
				
				$stagesdata[] = $mediadata;
		}
				}
				
			$stagearr = array('0'=>$mediadata2,'1'=>$stagesdata[0],'2'=>$stagesdata[1],'3'=>$stagesdata[2],'4'=>$stagesdata[3],'5'=>$stagesdata[4],'6'=>$stagesdata[5],'7'=>$stagesdata[6]);
			//$stagearrdata = array_values($stagearr);
			
		$resArr ['result'] = 1;
		$resArr ['message'] = "Media List";
		$resArr ['data'] = $stagearr;
		
	  echo $this->response($resArr, 200); 
	  }
  function item_image_videos_post(){
	  $resArr['result'] = 0;
    	$resArr['message'] = "";	
		$header = $this->input->request_headers();
	    $dataarr = json_decode(file_get_contents("php://input"), true);
		$itemId = $dataarr['itemId'];
		$fileType = $dataarr['fileType'];
		if($itemId!=''){
		$image = $this->db->select('image_video')->from('display_contents dc')->join('image_videos iv','dc.id=iv.display_content_id')->where('dc.itemId',$itemId)->where('dc.file_type',$fileType)->where('dc.content_type','item')->get();
		$imagedata = $image->result_array('array');
		$dataimg = array();
		if(!empty($imagedata)){
			foreach($imagedata as $img){
			$data['fileUrl'] = base_url().'uploads/image_videos/'.$img['image_video'];
			$dataimg[] = $data;
			}
		$resArr ['result'] = 1;
			$resArr ['message'] = "Item ".ucfirst($fileType)." List";
			$resArr ['data'] = $dataimg;
		}else{
		$resArr ['message'] = "No ".$fileType." available.";	
		}
		}else{
		$resArr ['message'] = "Please pass the item id";	
			}
	  echo $this->response($resArr, 200); 
	  }
	  
   function customer_profile_post(){
	 $resArr['result'] = 0;
	$resArr['message'] = "";	
	$header = $this->input->request_headers();
	$dataarr = json_decode(file_get_contents("php://input"), true);
	$userId = $dataarr['userId'];
	$fullName = $dataarr['name'];
	$altMobile = $dataarr['altMobile'];
	$email = $dataarr['email'];
	$address = $dataarr['address'];
	$birthDate = $dataarr['birthDate'];
	$anniversaryDate = $dataarr['anniversaryDate'];
	$locationCoords = $dataarr['locationCoords'];
	$image = $dataarr['image'];
	if($header['Lang']=='ar'){
	$data['foreignName'] = $fullName;
	}else{
	$data['name'] = $fullName;
	}	
	$data['altMobile'] = $altMobile;
	$data['email'] = $email;
	if($header['Lang']=='ar'){
	$data['foreignAddress'] = $address;
	}else{
	$data['address'] = $address;
	}
	$data['birthDate'] = $birthDate;
	$data['anniversaryDate'] = $anniversaryDate;
	$data['locationCoords'] = json_encode($locationCoords);
	if($image!=''){
	$dataimg = str_replace('data:image/png;base64,', '', $image);
				$dataimg = str_replace(' ', '+', $dataimg);
				$dataimg = base64_decode($dataimg);
				$url = $_SERVER['DOCUMENT_ROOT'].'/uploads/profile';
				$filename = 'profile'.$userId.time().'.png';
								 
				$filetosave = $_SERVER['DOCUMENT_ROOT'].'/uploads/profile/'. $filename;
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
				$fileraw = curl_exec($ch);
				curl_close ($ch);
				$success = file_put_contents($filetosave, $dataimg);
	$data['image'] = $filename;
	}
	//print_r($data);exit;
	$this->db->where('id',$userId);			
	$this->db->update('customers',$data);
	
	$where = "id='".$userId."'";
	$customerdata = $this->Admin_model->getDataById('customers',$where);
		$resultdata['userId'] = $customerdata[0]['id'];
		if($header['Lang']=='ar'){
		$resultdata['name'] = $customerdata[0]['foreignName'];
		$resultdata['address'] = $customerdata[0]['foreignAddress'];
		}else{
			$resultdata['name'] = $customerdata[0]['name'];
			$resultdata['address'] = $customerdata[0]['address'];
			}
		$resultdata['callingCode'] = $customerdata[0]['callingCode'];
		$resultdata['mobile'] = $customerdata[0]['mobile'];
		$resultdata['altMobile'] = $customerdata[0]['altMobile'];
		$resultdata['email'] = $customerdata[0]['email'];
		$resultdata['birthDate'] = $customerdata[0]['birthDate'];
		$resultdata['anniversaryDate'] = $customerdata[0]['anniversaryDate'];
		//$data['locationCoords'] = $customerdata[0]['locationCoords'];
		if($customerdata[0]['image']!=''){
		$resultdata['image'] = base_url().'uploads/profile/'.$customerdata[0]['image'];
		}else{
		$resultdata['image'] = base_url().'assets/img/user.png';	
			}
	$resArr['result'] = 1;
    $resArr["message"] = "Customer profile updated successfully";
	$resArr["data"] = $resultdata;
	echo $this->response($resArr, 200);	
		}	
/*	FOR SAP ------MODE*/

   function syncCustomer_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $CardName = $this->input->post('CardName');
        $CardForeignName = $this->input->post('CardForeignName');
        $Cellular = $this->input->post('Cellular');
        $CardCode = $this->input->post('CardCode');
        if (! $CardName || ! $CardForeignName || !$Cellular || !$CardCode) {
            $resArr['message'] = "Please pass all parameters";
        } else {
			$where = "mobile='".$Cellular."'";
			$customerdata = $this->Admin_model->getDataById('customers',$where);
			$whereCardCode = "customerCode='".$CardCode."'";
			$customerdataCardCode = $this->Admin_model->getDataById('customers',$whereCardCode);
			if(empty($customerdata) && empty($customerdataCardCode)){
            $data = array('name'=>$CardName,
						'foreignName'=>$CardForeignName,
						'mobile'=>$Cellular,
						'oldMobile'=>$Cellular,
						'customerCode'=>$CardCode,
						'created_at'=>date('Y-m-d H:i:s')
                         );
            $savecustomer = $this->db->insert('customers',$data);
            if($savecustomer){
                $resArr['result'] = 1;
                $resArr["message"] = "Customer inserted successfully";
            }else{
                $resArr['result'] = 0;
                $resArr["message"] = "Something went wrong. Please try again";
            }
			}else{
				$resArr['result'] = 0;
                $resArr["message"] = "This customer is already exist.";
				}
        }
        echo $this->response($resArr, 200);
    }
	function changeCustomerCredential_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
       
        $Cellular = $this->input->post('Cellular');
        $CardCode = $this->input->post('CardCode');
        if (!$Cellular || !$CardCode) {
            $resArr['message'] = "Please pass all parameters";
        } else {
			$where = "mobile='".$Cellular."'";
			$customerdata = $this->Admin_model->getDataById('customers',$where);
			$whereCardCode = "customerCode='".$CardCode."'";
			$customerdataCardCode = $this->Admin_model->getDataById('customers',$whereCardCode);
			if(!empty($customerdata)){
			$resArr['result'] = 0;
            $resArr["message"] = "This Cellular is already exist.";	
			}else if(!empty($customerdataCardCode)){
            $data = array('mobile'=>$Cellular,
						'oldMobile'=>$customerdataCardCode[0]['mobile']
                         );
			$this->db->where('id',$customerdataCardCode[0]['id']);	
            $savecustomer = $this->db->update('customers',$data);
            if($savecustomer){
                $resArr['result'] = 1;
                $resArr["message"] = "Customer credential updated successfully";
            }else{
                $resArr['result'] = 0;
                $resArr["message"] = "Something went wrong. Please try again";
            }
			}else{
				$resArr['result'] = 0;
                $resArr["message"] = "This customer is not avalilable.";
				}
        }
        echo $this->response($resArr, 200);
    }
   function syncSalesOrder_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $DocEntry = $this->input->post('DocEntry');
        $DocNum = $this->input->post('DocNum');
        $DocDate = $this->input->post('DocDate');
        $DocDueDate = $this->input->post('DocDueDate');
		$CardCode = $this->input->post('CardCode');
		$CardName = $this->input->post('CardName');
		$U_POTyp = $this->input->post('U_POTyp');
		$U_TechRev = $this->input->post('U_TechRev');
		$DocCurrency = $this->input->post('DocCurrency');
		$VatSum = $this->input->post('VatSum');
		$DocTotal = $this->input->post('DocTotal');
		
        if (! $DocEntry || ! $DocNum || !$DocDate || !$DocDueDate || !$CardCode || !$CardName || !$DocCurrency || !$U_POTyp || !$U_TechRev || !$VatSum || !$DocTotal) {
            $resArr['message'] = "Please pass all parameters";
        } else {
			$wherecust = "customerCode='".$CardCode."'";
			$customerdata = $this->Admin_model->getDataById('customers',$wherecust);
			$where = "salesOrderId='".$DocEntry."'";
			$orderdata = $this->Admin_model->getDataById('sales_orders',$where);
			
			if(empty($orderdata)){
            $data = array('customer_id'=>$customerdata[0]['id'],
			            'salesOrderId'=>$DocEntry,
						'orderNumber'=>$DocNum,
						'orderDate'=>$DocDate,
						'orderDueDate'=>$DocDueDate,
						'customerCode'=>$CardCode,
						'customerName'=>$CardName,
						'orderTypeId'=>$U_POTyp,
						'stageId'=>$U_TechRev,
						'currency'=>$DocCurrency,
						'vatAmount'=>$VatSum,
						'orderTotal'=>$DocTotal,
						'created_at'=>date('Y-m-d H:i:s')
                         );
            $saveorder = $this->db->insert('sales_orders',$data);
			$order_id = $this->db->insert_id();
			
            if($saveorder){
				if($U_TechRev==8){
				$wheredeliver = "order_id='".$order_id."'";
			    $deliverydata = $this->Admin_model->getDataById('order_delivery',$wheredeliver);
			if(empty($deliverydata)){
				$deliverytab = array('order_id'=>$order_id,'received_at'=>date('Y-m-d H:i:s'),'created_at'=>date('Y-m-d H:i:s'));
				$savedelivery = $this->db->insert('order_delivery',$deliverytab);
			}
				}
				
				$datalog = array('stageId'=>$U_TechRev,'order_id'=>$order_id,'creationDateTime'=>date('Y-m-d H:i:s'));
		        $savelog = $this->db->insert('stages_log',$datalog);
                $resArr['result'] = 1;
                $resArr["message"] = "Sales Order inserted successfully";
            }else{
                $resArr['result'] = 0;
                $resArr["message"] = "Something went wrong. Please try again";
            }
			}else{
				$resArr['result'] = 0;
                $resArr["message"] = "This Order is already exist.";
				}
        }
        echo $this->response($resArr, 200);
    }
   function syncOrderItem_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $DocEntry = $this->input->post('DocEntry');
		$DocNum = $this->input->post('DocNum');
        $ItemCode = $this->input->post('ItemCode');
        $ItemDescription = $this->input->post('ItemDescription');
		 $foreignDescription = $this->input->post('foreignDescription');
        $Quantity = $this->input->post('Quantity');
		
        if (!$DocEntry || !$DocNum || !$ItemCode || !$ItemDescription || !$foreignDescription || !$Quantity) {
            $resArr['message'] = "Please pass all parameters";
        } else {
			$whereorder = "salesOrderId='".$DocEntry."'";
			$orderdata = $this->Admin_model->getDataById('sales_orders',$whereorder);
			$where = "salesOrderId='".$DocEntry."' and itemCode='".$ItemCode."'";
			$orderitemdata = $this->Admin_model->getDataById('order_items',$where);
			$wherenum = "orderNumber='".$DocNum."' and itemCode='".$ItemCode."'";
			$orderitemdatanum = $this->Admin_model->getDataById('order_items',$wherenum);
			
			if(empty($orderitemdata) && empty($orderitemdatanum)){
            $data = array('order_id'=>$orderdata[0]['id'],
			            'salesOrderId'=>$DocEntry,
			            'orderNumber'=>$DocNum,
						'itemCode'=>$ItemCode,
						'description'=>$ItemDescription,
						'foreignDescription'=>$foreignDescription,
						'quantity'=>$Quantity,						
						'created_at'=>date('Y-m-d H:i:s')
                         );
            $saveorderitem = $this->db->insert('order_items',$data);
            if($saveorderitem){
                $resArr['result'] = 1;
                $resArr["message"] = "Sales Order Item inserted successfully";
            }else{
                $resArr['result'] = 0;
                $resArr["message"] = "Something went wrong. Please try again";
            }
			}else{
				$resArr['result'] = 0;
                $resArr["message"] = "This Order Item is already exist.";
				}
        }
        echo $this->response($resArr, 200);
    }
function syncSaleOrderUpdate_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
       
        $DocEntry = $this->input->post('DocEntry');
        $U_TechRev = $this->input->post('U_TechRev');
        if (!$DocEntry || !$U_TechRev) {
            $resArr['message'] = "Please pass all parameters";
        } else {
			$where = "salesOrderId='".$DocEntry."'";
			$orderdata = $this->Admin_model->getDataById('sales_orders',$where);
		
			 if(!empty($orderdata)){
            $data = array('stageId'=>$U_TechRev
                         );
			$this->db->where('id',$orderdata[0]['id']);	
            $saveorder = $this->db->update('sales_orders',$data);
			
            if($saveorder){
				if($U_TechRev==8){
				$wheredeliver = "order_id='".$order_id."'";
			    $deliverydata = $this->Admin_model->getDataById('order_delivery',$wheredeliver);
			if(empty($deliverydata)){
				$deliverytab = array('order_id'=>$order_id,'received_at'=>date('Y-m-d H:i:s'),'created_at'=>date('Y-m-d H:i:s'));
				$savedelivery = $this->db->insert('order_delivery',$deliverytab);
			}
				}
				$datalog = array('stageId'=>$U_TechRev,'order_id'=>$orderdata[0]['id'],'creationDateTime'=>date('Y-m-d H:i:s'));
		        $savelog = $this->db->insert('stages_log',$datalog);
                $resArr['result'] = 1;
                $resArr["message"] = "Order status updated successfully";
            }else{
                $resArr['result'] = 0;
                $resArr["message"] = "Something went wrong. Please try again";
            }
			}else{
				$resArr['result'] = 0;
                $resArr["message"] = "This Order is not avalilable.";
				}
        }
        echo $this->response($resArr, 200);
    }
}
