<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper("mode");
		$this->load->model("admin_model");
		$this->load->model("dashboard_model");
		$this->load->helper("url");
		$this->load->helper('form');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
		$this->load->library('upload');
		session_check();
		getusername();
		date_default_timezone_set('Asia/Riyadh');
	}

	public function employees()
	{
		$table = 'users';
		
		$whereemployees = "userType='3' order by id desc";
        $data['employees'] = $this->admin_model->getDataById($table,$whereemployees);
		$country = $this->db->select('name,callingCode')->from('country_codes')->get();
		$data['countrydata'] = $country->result_array('array');
		$this->load->view('employees', $data);
	}
   public function empchangestatus()
	{
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$savecustomer[$i] = $this->db->update('users',$data[$i]);
		}
		if ($savecustomer) {
            echo 1;
        } else {
            echo 0;
        }
		
	}
	public function deleteemployee(){
		$id = explode(',',$_POST['id']);
		
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('users', $id[$i]);
		}
		
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
   public function saveemployee(){
	   $name = $_POST['name'];
	   $callingCode = $_POST['callingCode'];
	   $mobile = $_POST['mobile'];
	   $password = $_POST['password'];
	   $email = $_POST['email'];
	   $user_code = $_POST['user_code'];
	   $designation = $_POST['designation'];
	   $data = array('name'=>$name,'callingCode'=>$callingCode,'mobile'=>$mobile,'password'=>md5($password),'text_password'=>$password,'email'=>$email,'user_code'=>$user_code,'designation'=>$designation,'userType'=>3);
	   $saveemployee = $this->db->insert('users',$data);
	  if ($saveemployee) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	public function ajaxeditemployee(){
		$id = $_POST['id'];	
	   $table = 'users';
	   $where = "id='".$id."'";
       $data['emps'] = $this->admin_model->getDataById($table,$where);
	   $country = $this->db->select('name,callingCode')->from('country_codes')->get();
		$data['countrydata'] = $country->result_array('array');
		$this->load->view('ajaxeditemployee', $data);
	   }
	public function updateemployee(){
	   $id = $_POST['id'];
	   $name = $_POST['name'];
	   $callingCode = $_POST['callingCode'];
	   $mobile = $_POST['mobile'];
	   $password = $_POST['password'];
	   $email = $_POST['email'];
	   $user_code = $_POST['user_code'];
	   $designation = $_POST['designation'];
	   $data = array('name'=>$name,'callingCode'=>$callingCode,'mobile'=>$mobile,'password'=>md5($password),'text_password'=>$password,'email'=>$email,'user_code'=>$user_code,'designation'=>$designation);
	   $this->db->where('id',$id);	
	   $saveemployee = $this->db->update('users',$data);
	  if ($saveemployee) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	   
	   
	   
   public function deliveryman()
	{
		$table = 'users';
		
		$wheredeliveryman = "userType='1' order by id desc";
        $data['deliveryman'] = $this->admin_model->getDataById($table,$wheredeliveryman);
		 $country = $this->db->select('name,callingCode')->from('country_codes')->get();
		$data['countrydata'] = $country->result_array('array');
		$this->load->view('deliveryman', $data);
	}
   public function deliverymanchangestatus()
	{
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$savecustomer[$i] = $this->db->update('users',$data[$i]);
		}
		if ($savecustomer) {
            echo 1;
        } else {
            echo 0;
        }
		
	}
	public function deletedeliveryman(){
		$id = explode(',',$_POST['id']);
		
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('users', $id[$i]);
		}
		
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
   public function savedeliveryman(){
	   $name = $_POST['name'];
	    $callingCode = $_POST['callingCode'];
	   $mobile = $_POST['mobile'];
	   $password = $_POST['password'];
	   $email = $_POST['email'];
	   $user_code = $_POST['user_code'];
	   $designation = $_POST['designation'];
	   $data = array('name'=>$name,'callingCode'=>$callingCode,'mobile'=>$mobile,'password'=>md5($password),'text_password'=>$password,'email'=>$email,'user_code'=>$user_code,'designation'=>$designation,'userType'=>1);
	   $savedeliveryman = $this->db->insert('users',$data);
	  if ($savedeliveryman) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	public function ajaxeditdeliveryman(){
		$id = $_POST['id'];	
	   $table = 'users';
	   $where = "id='".$id."'";
       $data['emps'] = $this->admin_model->getDataById($table,$where);
	    $country = $this->db->select('name,callingCode')->from('country_codes')->get();
		$data['countrydata'] = $country->result_array('array');
		$this->load->view('ajaxeditdeliveryman', $data);
	   }
	public function updatedeliveryman(){
	   $id = $_POST['id'];
	   $name = $_POST['name'];
	    $callingCode = $_POST['callingCode'];
	   $mobile = $_POST['mobile'];
	   $password = $_POST['password'];
	   $email = $_POST['email'];
	   $user_code = $_POST['user_code'];
	   $designation = $_POST['designation'];
	   $data = array('name'=>$name,'callingCode'=>$callingCode,'mobile'=>$mobile,'password'=>md5($password),'text_password'=>$password,'email'=>$email,'user_code'=>$user_code,'designation'=>$designation);
	   $this->db->where('id',$id);	
	   $savedeliveryman = $this->db->update('users',$data);
	  if ($savedeliveryman) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	 public function fitter()
	{
		$table = 'users';
		
		$wherefitter = "userType='2' order by id desc";
        $data['fitter'] = $this->admin_model->getDataById($table,$wherefitter);
		 $country = $this->db->select('name,callingCode')->from('country_codes')->get();
		$data['countrydata'] = $country->result_array('array');
		$this->load->view('fitter', $data);
	}
   public function fitterchangestatus()
	{
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$savecustomer[$i] = $this->db->update('users',$data[$i]);
		}
		if ($savecustomer) {
            echo 1;
        } else {
            echo 0;
        }
		
	}
	public function deletefitter(){
		$id = explode(',',$_POST['id']);
		
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('users', $id[$i]);
		}
		
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
   public function savefitter(){
	   $name = $_POST['name'];
	    $callingCode = $_POST['callingCode'];
	   $mobile = $_POST['mobile'];
	   $password = $_POST['password'];
	   $email = $_POST['email'];
	   $user_code = $_POST['user_code'];
	   $designation = $_POST['designation'];
	   $data = array('name'=>$name,'callingCode'=>$callingCode,'mobile'=>$mobile,'password'=>md5($password),'text_password'=>$password,'email'=>$email,'user_code'=>$user_code,'designation'=>$designation,'userType'=>2);
	   $savefitter = $this->db->insert('users',$data);
	  if ($savefitter) {
            echo 1;
        } else {
            echo 0;
        }
	   }
	public function ajaxeditfitter(){
		$id = $_POST['id'];	
	   $table = 'users';
	   $where = "id='".$id."'";
       $data['emps'] = $this->admin_model->getDataById($table,$where);
	    $country = $this->db->select('name,callingCode')->from('country_codes')->get();
		$data['countrydata'] = $country->result_array('array');
		$this->load->view('ajaxeditfitter', $data);
	   }
	public function updatefitter(){
	   $id = $_POST['id'];
	   $name = $_POST['name'];
	    $callingCode = $_POST['callingCode'];
	   $mobile = $_POST['mobile'];
	   $password = $_POST['password'];
	   $email = $_POST['email'];
	   $user_code = $_POST['user_code'];
	   $designation = $_POST['designation'];
	   $data = array('name'=>$name,'callingCode'=>$callingCode,'mobile'=>$mobile,'password'=>md5($password),'text_password'=>$password,'email'=>$email,'user_code'=>$user_code,'designation'=>$designation);
	   $this->db->where('id',$id);	
	   $savefitter = $this->db->update('users',$data);
	  if ($savefitter) {
            echo 1;
        } else {
            echo 0;
        }
	   }
}
