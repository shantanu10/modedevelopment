<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper("mode");
		$this->load->model("admin_model");
		$this->load->model("dashboard_model");
		$this->load->helper("url");
		$this->load->helper('form');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
		$this->load->library('upload');
		session_check();
		getusername();
		date_default_timezone_set('Asia/Riyadh');
	}

	public function index()
	{
		$table = 'customers';
		
		$wherecustomer = "1 order by id desc";
        $data['customers'] = $this->admin_model->getDataById($table,$wherecustomer);
		
		$this->load->view('customers', $data);
	}
	public function changestatus()
	{
		$table = 'customers';
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$data[$i] = array('status'=>$status);
		$this->db->where('id',$id[$i]);	
		$savecustomer[$i] = $this->db->update('customers',$data[$i]);
		}
		if ($savecustomer) {
            echo 1;
        } else {
            echo 0;
        }
		
	}
	public function deletecustomer(){
		$id = explode(',',$_POST['id']);
		$where = "customer_id IN ('".$_POST['id']."')";
		$existdata = $this->admin_model->getDataById('sales_orders',$where);
		if(!empty($existdata)){
			echo 2;
		}else{
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('customers', $id[$i]);
		}
		
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
		}
   public function ajaxviewcustomer(){
	   $id = $_POST['id'];
	   $table = 'customers';
	   $wherecustomer = "id='".$id."'";
       $data['customer'] = $this->admin_model->getDataById($table,$wherecustomer);
		$this->load->view('ajaxviewcustomer', $data);
	   }

}
