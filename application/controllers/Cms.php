<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct()
	{
		parent::__construct();
		$this->load->model("admin_model");
		
	}
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function aboutus()
	{
		$id = 1;
		$table = 'contents';
		$data['content'] = $this->admin_model->getdetails($table, $id);
		$this->load->view('cmspage', $data);
	}
	public function termscondition()
	{
		$id = 2;
		$table = 'contents';
		$data['content'] = $this->admin_model->getdetails($table, $id);
		$this->load->view('cmspage', $data);
	}
	public function privacy()
	{
		$id = 3;
		$table = 'contents';
		$data['content'] = $this->admin_model->getdetails($table, $id);
		$this->load->view('cmspage', $data);
	}
	public function vendoraboutus()
	{
		$id = 4;
		$table = 'contents';
		$data['content'] = $this->admin_model->getdetails($table, $id);
		$this->load->view('cmspage', $data);
	}
	public function vendortermscondition()
	{
		$id = 5;
		$table = 'contents';
		$data['content'] = $this->admin_model->getdetails($table, $id);
		$this->load->view('cmspage', $data);
	}
	public function vendorprivacy()
	{
		$id = 6;
		$table = 'contents';
		$data['content'] = $this->admin_model->getdetails($table, $id);
		$this->load->view('cmspage', $data);
	}
	public function notify(){
		$date = date('Y-m-d');
		$table = 'bookings';
		$where = "booking_date = '".$date."' and status='Confirmed'";
		$bookingdada = $this->admin_model->getDataById($table, $where);
		if(!empty($bookingdada)){
			foreach($bookingdada as $data){
		    $wherecustomer = "id='".$data['customer_id']."'";
			$customerdata = $this->admin_model->getDataById('customers',$wherecustomer);
				$token = $customerdata[0]['fcm_token'];
				$message = "Booking Reminder : You have a booking today at ".$data['booking_time']."";
				$this->admin_model->sendpush($token,$message);
			}
		}
		}
}
