<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper("mode");
		$this->load->model("admin_model");
		$this->load->model("dashboard_model");
		$this->load->helper("url");
		$this->load->helper('form');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
		$this->load->library('upload');
		session_check();
		getusername();
		date_default_timezone_set('Asia/Riyadh');
	}

	public function index()
	{
		$table = 'sales_orders';		
		$where = "1 order by id desc";
        $data['orders'] = $this->admin_model->getDataById($table,$where);
		$where2 = "stageId IN (2,3,4,5,6,7)";
        $data['sales_order_stages'] = $this->admin_model->getDataById('sales_order_stages',$where2);
		$this->load->view('orders', $data);
	}
   public function view_order($id=''){
	   $table = 'sales_orders';
	   $where = "id='".$id."'";
       $data['order'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('view_order', $data);
	   }
   public function changestatus()
	{
		$table = 'sales_orders';
		$id = explode(',',$_POST['id']);
		$status = $_POST['status'];
		for($i=0;$i<count($id);$i++){
		$where[$i] = "id='".$id[$i]."'";
       $orderdata[$i] = $this->admin_model->getDataById('sales_orders',$where[$i]);	
		if($status > $orderdata[$i][0]['stageId'] && $status == $orderdata[$i][0]['stageId']+1){
			$data[$i] = array('stageId'=>$status);
		$this->db->where('id',$id[$i]);	
		$saveorder[$i] = $this->db->update('sales_orders',$data[$i]);
		$where[$i] = "id='".$id[$i]."'";
       $orderdata[$i] = $this->admin_model->getDataById('sales_orders',$where[$i]);
		$wherecust[$i] = "customer_id='".$orderdata[$i][0]['customer_id']."'";
       $customerdata[$i] = $this->admin_model->getDataById('customer_tokens',$wherecust[$i]);
	  // $customer_id[$i] = $customerdata[$i][0]['id'];
	   $token[$i] = $customerdata[$i][0]['fcm_token'];
	   $wherestage = "stageId='".$status."'";
       $stagedata = $this->admin_model->getDataById('sales_order_stages',$wherestage);
	   if($status==7){
		  $message[$i] = "Your order(Number:".$orderdata[$i][0]['orderNumber'].") has been received at warehouse.";
		  $this->admin_model->sendpush($token[$i],$message[$i],$id[$i],'ORDER_DETAILS_SCREEN'); 
	   }		
		
		$data[$i] = array('stageId'=>$status,'order_id'=>$id[$i],'creationDateTime'=>date('Y-m-d H:i:s'));
		$savelog[$i] = $this->db->insert('stages_log',$data[$i]);
		if($status==7){
			$data[$i] = array('order_id'=>$id[$i],'stageId'=>$status,'received_at'=>date('Y-m-d H:i:s'),'created_at'=>date('Y-m-d H:i:s'));
		$savedelivery[$i] = $this->db->insert('order_delivery',$data[$i]);
		$savefitting[$i] = $this->db->insert('order_fitting',$data[$i]);
		 }
			
			}else{
				echo 2;
				exit;
				}	
		
		}
		if ($saveorder) {
            echo 1;
        } else {
            echo 0;
        }
		
	}
	public function deleteorder(){
		$id = explode(',',$_POST['id']);
		$where = "order_id IN ('".$_POST['id']."')";
		$existdata = $this->admin_model->getDataById('order_items',$where);
		if(!empty($existdata)){
			echo 2;
		}else{
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('sales_orders', $id[$i]);
		}
		
        if ($res) {
            echo 1;
        } else {
            echo 0;
		}
		}
		}
 public function items($id=''){
	 $data['orderNumber'] = $id;
	   $table = 'order_items';
	   $where = "orderNumber='".$id."'";
       $data['items'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('items', $data);
	   }
	public function ajaxitems(){
		$id = $_POST['id'];
	 $data['orderNumber'] = $id;
	   $table = 'order_items';
	   $where = "orderNumber='".$id."'";
       $data['items'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('ajaxitems', $data);
	   }
	public function deleteorderitems(){
		$id = explode(',',$_POST['id']);		
		for($i=0;$i<count($id);$i++){
		
        $res[$i] = $this->admin_model->delete('order_items', $id[$i]);
		}
        if ($res) {
            echo 1;
        } else {
            echo 0;
        }
		}
		
	public function receipt()
	{
		$table = 'item_receipts';		
		$where = "1 order by id desc";
        $data['receipts'] = $this->admin_model->getDataById($table,$where);
		
		$this->load->view('receipts', $data);
	}
	public function ajaxpaymentTransaction(){
		$id = $_POST['id'];
		$order = $this->db->select('orderNumber')->from('sales_orders')->where('id',$id)->get();
		$orderdata = $order->result_array('array');
		$data['orderNumber'] = $orderdata[0]['orderNumber'];
		$table = 'payments';
		$where = "order_id='".$id."'";
		$data['transactions'] = $this->admin_model->getDataById($table,$where);
		$this->load->view('ajaxpaymentTransaction', $data);
	   }
}
